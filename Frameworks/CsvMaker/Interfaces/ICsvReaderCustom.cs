﻿using CsvMaker.CsvString;

namespace CsvMaker.Interfaces
{
    interface ICsvReaderCustom
    {
        T ValidateCsvHeaderCustom<T>(CsvStringReader sr);
        T ReadCsvRowCustom<T>(CsvStringReader sr);
    }
}
