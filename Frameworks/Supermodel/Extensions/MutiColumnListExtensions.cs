﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.Metadata;

namespace Supermodel.Extensions
{
    public static class MutiColumnListExtensions
    {
        public static StringBuilder ToHtmlTableHeader(this object me, HtmlHelper html, StringBuilder sb = null, MvcHtmlString sortedHtml = null, MvcHtmlString sortedHtmlDesc = null)
        {
            sb = sb ?? new StringBuilder();

            var proprties = me.GetType().GetProperteisInOrder().ToList();
            if (!proprties.Any()) throw new SupermodelSystemErrorException("ListMvcModelT must have at least one property marked with [ListColumn] attribute");
            foreach (var property in proprties)
            {
                var listColumnAttr = property.GetCustomAttribute<ListColumnAttribute>();
                var header = listColumnAttr.Header ?? property.Name.InsertSpacesBetweenWords();
                var orderBy = listColumnAttr.OrderBy;
                var orderByDesc = listColumnAttr.OrderByDesc;
                sb.Append(html.Supermodel().SortableColumnHeader(header, orderBy, orderByDesc));
            }
            return sb;
        }
        public static StringBuilder ToHtmlTableRow(this object me, HtmlHelper html, StringBuilder sb = null)
        {
            sb = sb ?? new StringBuilder();

            var proprties = me.GetType().GetProperteisInOrder().ToList();
            if (!proprties.Any()) throw new SupermodelSystemErrorException("ListMvcModelT must have at least one property marked with [ListColumn] attribute");
            foreach (var property in proprties)
            {
                sb.Append("<td>");
                var propertyObj = me.PropertyGet(property.Name);
                if (propertyObj != null)
                {
                    //We do special handling for MvcHtmlString type to allow for HTML context in columns while still being secure
                    if (propertyObj is MvcHtmlString)
                    {
                        sb.Append(propertyObj);
                    }
                    else
                    {
                        if (propertyObj is ISupermodelDisplayTemplate) sb.Append((propertyObj as ISupermodelDisplayTemplate).DisplayTemplate(html));
                        else sb.Append(html.Encode(propertyObj.GetDescription()));
                    }
                }
                sb.Append("</td>");
            }
            return sb;
        }

        public static IEnumerable<PropertyInfo> GetProperteisInOrder(this Type myType)
        {
            return myType.GetProperties().Where(x => x.GetCustomAttribute<ListColumnAttribute>() != null).OrderBy(x => x.GetCustomAttribute<ListColumnAttribute>().HeaderOrder);
        }
    }
}
