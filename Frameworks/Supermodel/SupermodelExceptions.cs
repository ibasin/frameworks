﻿using System;
using ReflectionMapper;

namespace Supermodel 
{
    public class SupermodelException : Exception
    {
        public SupermodelException() : this("N/A") { }
        public SupermodelException(string msg) : base(msg) { }
    }

    public class SupermodelSystemErrorException : SupermodelException 
    {
		public SupermodelSystemErrorException() : this("N/A") {} 
        public SupermodelSystemErrorException(string msg) : base(ReflectionHelper.GetThrowingContext() + ": " + msg) {}
	}

    public class SettingParentIdOnRootException : SupermodelSystemErrorException {}

    public class ChangingParentIdOnChildtoInvalidValueException : SupermodelSystemErrorException {}

    public class ModelStateInvalidException : SupermodelSystemErrorException
    {
        public ModelStateInvalidException(object model)
        {
            Model = model;
        }

        public object Model { get; protected set; }
    }

    public class UnableToDeleteException : SupermodelException
    {
        public UnableToDeleteException(string errorMessageToDisplay) : base(errorMessageToDisplay) {}
    }
}
