﻿using System;
using System.Collections;
using System.Collections.Generic;
using ReflectionMapper;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers
{
    public class ListWithCriteria<ListItemT, CriteriaT> : List<ListItemT>, IRMapperCustom
    {
        [NotRMapped] public CriteriaT Criteria { get; set; }
        
        public object MapFromObjectCustom(object obj, Type objType)
        {
            //Check if we are mapping from a list with Criteria
            var propertyInfo = obj.GetType().GetProperty("Criteria");
            if (propertyInfo != null && propertyInfo.PropertyType == typeof(CriteriaT)) obj.PropertySet("Criteria", Criteria);

            var objIEnumerable = (IEnumerable)obj;
            var myIEnumerableGenericArg = GetType().GetInterface(typeof(IEnumerable<>).Name).GetGenericArguments()[0];
            foreach (var objItemObj in objIEnumerable)
            {
                this.ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
            }
            
            return this;
        }
        public object MapToObjectCustom(object obj, Type objType)
        {
            //Check if we are mapping to a list with Criteria
            var propertyInfo = obj.GetType().GetProperty("Criteria");
            if (propertyInfo != null && propertyInfo.PropertyType == typeof(CriteriaT)) Criteria = (CriteriaT)obj.PropertyGet("Criteria");

            var myICollection = (ICollection)this;
            var objICollectionGenericArg = obj.GetType().GetInterface(typeof(IEnumerable<>).Name).GetGenericArguments()[0];
            foreach (var myItemObj in myICollection)
            {
                obj.ExecuteMethod("Add", myItemObj != null ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objICollectionGenericArg), objICollectionGenericArg) : null);
            }
            
            return obj;
        }
    }
}
