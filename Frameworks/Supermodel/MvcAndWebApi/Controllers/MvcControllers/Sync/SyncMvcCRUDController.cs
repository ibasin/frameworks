﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync
{
    public abstract class SyncMvcCRUDController<EntityT, MvcModelT, DbContextT> : SyncMvcCRUDController<EntityT, MvcModelT, MvcModelT, DbContextT>
        where EntityT : class, IEntity, new()
        where MvcModelT : MvcModelForEntity<EntityT>, new()
        where DbContextT : class, IDbContext, new()
    { }

    public abstract class SyncMvcCRUDController<EntityT, DetailMvcModelT, ListMvcModelT, DbContextT> : SupermodelMvcControllerCore
        where EntityT : class, IEntity, new()
        where DetailMvcModelT : MvcModelForEntity<EntityT>, new()
        where ListMvcModelT : MvcModelForEntity<EntityT>, new()
        where DbContextT : class, IDbContext, new()
    {
        #region Action Methods
        public virtual ActionResult List()
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var entities = GetItems().ToList();
                var mvcModels = new List<ListMvcModelT>();
                mvcModels = (List<ListMvcModelT>)mvcModels.MapFromObject(entities);
                mvcModels = mvcModels.OrderBy(p => p.Label).ToList();
                return View(mvcModels);
            }
        }
        
        [HttpDelete]
        public virtual ActionResult Detail(long id, HttpDelete ignore)
        {
            EntityT entityItem = null;
            using (new UnitOfWork<DbContextT>())
            {
                try
                {
                    entityItem = GetItem(id);
                    entityItem.Delete();
                }
                catch (UnableToDeleteException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = ex.Message;
                }
                catch (Exception)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = "PROBLEM!!!\\n\\nUnable to delete. Most likely reason: references from other entities.";
                }
                return AfterDelete(id, entityItem);
            }
        }

        [HttpGet]
        public virtual ActionResult Detail(long id, HttpGet ignore)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                if (id == 0) return View(new DetailMvcModelT().MapFromObject(new EntityT().ConstructVirtualProperties()));

                var entityItem = GetItem(id);
                var mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(entityItem);

                return View(mvcModelItem);
            }
        }

        [HttpPut]
        public virtual ActionResult Detail(long id, HttpPut ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id == 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Put]: id == 0");

                var entityItem = GetItem(id);
                DetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }

                return AfterUpdate(id, entityItem, mvcModelItem);
            }
        }

        [HttpPost]
        public virtual ActionResult Detail(long id, HttpPost ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id != 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Post]: id != 0");

                var entityItem = (EntityT)new EntityT().ConstructVirtualProperties();
                DetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }
                entityItem.Add();

                return AfterCreate(id, entityItem, mvcModelItem);
            }
        }

        [HttpGet]
        public virtual ActionResult GetBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(GetItem(id));
                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);
                if (file == null || file.IsEmpty) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                const string mimeType = "application/octet-stream";
                //var mimeType = Common.GetContentTypeHeader(file.Name);
                return File(file.BinaryContent, mimeType, file.Name);
            }
        }

        [HttpGet]
        public virtual ActionResult DeleteBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>())
            {
                var entityItem = GetItem(id);
                var mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(entityItem);

                //see if pn is a required property
                if (Attribute.GetCustomAttribute(typeof(DetailMvcModelT).GetProperty(pn), typeof(RequiredAttribute), true) != null)
                {
                    TempData.Supermodel().NextPageModalMessage = "Cannot delete required field";
                    var routeValues = new RouteValueDictionary(ControllerContext.RouteData.Values);
                    routeValues["Action"] = "Detail";
                    return RedirectToRoute(routeValues);
                }

                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);

                file.Empty();
                entityItem = (EntityT)mvcModelItem.MapToObject(entityItem);
                return AfterBinaryDelete(id, entityItem, mvcModelItem);
            }
        }
        #endregion

        #region Protected Methods
        protected virtual ActionResult AfterUpdate(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            return GoToListScreen();
        }
        protected virtual ActionResult AfterCreate(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            return GoToListScreen();
        }
        protected virtual ActionResult AfterDelete(long id, EntityT entityItem)
        {
            return GoToListScreen();
        }
        protected virtual ActionResult AfterBinaryDelete(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            return StayOnDetailScreen(entityItem);
        }

        protected virtual EntityT GetItem(long id)
        {
            return GetItems().Single(x => x.Id == id);
        }
        protected virtual IQueryable<EntityT> GetItems()
        {
            return ControllerCommon.GetItems<EntityT>();
        }

        //this methods will catch validation exceptions that happen during mapping from mvc to domain (when it runs validation for mvc model by creating a domain object)
        protected virtual EntityT TryUpdateEntity(EntityT entityItem, out DetailMvcModelT mvcModelItem)
        {
            return ControllerCommon.TryUpdateEntity(this, entityItem, out mvcModelItem);
        }

        protected virtual ActionResult GoToListScreen()
        {
            return ControllerCommon.GoToListScreen(this);
        }
        protected virtual ActionResult StayOnDetailScreen(EntityT entityItem)
        {
            UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChanges();
            return ControllerCommon.StayOnDetailScreen(this, entityItem.Id);
        }
        #endregion
    }
}
