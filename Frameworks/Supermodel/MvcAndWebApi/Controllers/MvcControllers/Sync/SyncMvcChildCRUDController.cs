﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync
{
    public abstract class SyncMvcChildCRUDController<ChildEntityT, ParentEntityT, ChildDetailMvcModelT, DbContextT> : SupermodelMvcControllerCore
        where ChildEntityT : class, IEntity, new()
        where ParentEntityT : class, IEntity, new()
        where ChildDetailMvcModelT : ChildMvcModelForEntity<ChildEntityT, ParentEntityT>, new()
        where DbContextT : class, IDbContext, new()
    {
        #region Action Methods
        public abstract ActionResult List(long? parentId);

        [HttpDelete]
        public virtual ActionResult Detail(long id, HttpDelete ignore)
        {
            ChildEntityT entityItem = null;
            using (new UnitOfWork<DbContextT>())
            {
                long? parentId = null;
                try
                {
                    entityItem = GetItem(id);
                    parentId = new ChildDetailMvcModelT().GetParentEntity(entityItem).Id;
                    entityItem.Delete();
                }
                catch (UnableToDeleteException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = ex.Message;
                }
                catch (Exception)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = "PROBLEM!!!\\n\\nUnable to delete. Most likely reason: references from other entities.";
                }
                if (parentId == null) throw new SupermodelSystemErrorException("Unknown parentId");
                return AfterDelete(id, parentId.Value, entityItem);
            }
        }

        [HttpGet]
        public virtual ActionResult Detail(long id, long? parentId, HttpGet ignore)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                ChildDetailMvcModelT mvcModelItem;
                if (id == 0)
                {
                    if (parentId == null) throw new SupermodelSystemErrorException("parentId == null when id == 0");
                    mvcModelItem = new ChildDetailMvcModelT { ParentId = parentId }; //We set parentID twice, in case we may need it during MapFromObject
                    mvcModelItem = (ChildDetailMvcModelT)mvcModelItem.MapFromObject(new ChildEntityT().ConstructVirtualProperties());
                    mvcModelItem.ParentId = parentId;
                    return View(mvcModelItem);
                }

                var entityItem = GetItem(id);
                mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(entityItem);
                return View(mvcModelItem);
            }
        }

        [HttpPut]
        public virtual ActionResult Detail(long id, HttpPut ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id == 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Post]: id == 0");

                var entityItem = GetItem(id);
                ChildDetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, null, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }
                return AfterUpdate(id, entityItem, mvcModelItem);
            }
        }

        [HttpPost]
        public virtual ActionResult Detail(long id, long parentId, HttpPost ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id != 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Put]: id != 0");

                var entityItem = (ChildEntityT)new ChildEntityT().ConstructVirtualProperties();
                ChildDetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, parentId, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }
                entityItem.Add();
                return AfterCreate(id, parentId, entityItem, mvcModelItem);
            }
        }

        [HttpGet]
        public virtual ActionResult GetBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(GetItem(id));
                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);
                const string mimeType = "application/octet-stream";
                //var mimeType = Common.GetContentTypeHeader(file.Name);
                if (file == null || file.IsEmpty) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                return File(file.BinaryContent, mimeType, file.Name);
            }
        }

        [HttpGet]
        public virtual ActionResult DeleteBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>())
            {
                var entityItem = GetItem(id);
                var mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(entityItem);

                //see if pn is a required property
                if (Attribute.GetCustomAttribute(typeof(ChildDetailMvcModelT).GetProperty(pn), typeof(RequiredAttribute), true) != null)
                {
                    TempData.Supermodel().NextPageModalMessage = "Cannot delete required field";
                    var routeValues = new RouteValueDictionary(ControllerContext.RouteData.Values);
                    routeValues["Action"] = "Detail";
                    return RedirectToRoute(routeValues);
                }

                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);

                file.Empty();
                entityItem = (ChildEntityT)mvcModelItem.MapToObject(entityItem);
                return AfterBinaryDelete(id, entityItem, mvcModelItem);
            }
        }
        #endregion

        #region Protected Methods & Properties
        protected virtual ActionResult AfterUpdate(long id, ChildEntityT entityItem, ChildDetailMvcModelT mvcModelItem)
        {
            return GoToListScreen(mvcModelItem.GetParentEntity(entityItem).Id);
        }
        protected virtual ActionResult AfterCreate(long id, long parentId, ChildEntityT entityItem, ChildDetailMvcModelT mvcModelItem)
        {
            return GoToListScreen(mvcModelItem.GetParentEntity(entityItem).Id);
        }
        protected virtual ActionResult AfterDelete(long id, long parentId, ChildEntityT entityItem)
        {
            return GoToListScreen(parentId);
        }
        protected virtual ActionResult AfterBinaryDelete(long id, ChildEntityT entityItem, ChildDetailMvcModelT mvcModelItem)
        {
            return StayOnDetailScreen(entityItem);
        }

        protected virtual IQueryable<ChildEntityT> GetItems()
        {
            return ControllerCommon.GetItems<ChildEntityT>();
        }
        protected virtual ChildEntityT GetItem(long id)
        {
            return GetItems().Single(x => x.Id == id);
        }

        //this methods will catch validation exceptions that happen during mapping from mvc to domain (when it runs validation for mvc model by creating a domain object)
        protected virtual ChildEntityT TryUpdateEntity(ChildEntityT entityItem, long? parentId, out ChildDetailMvcModelT mvcModelItem)
        {
            return ControllerCommon.TryUpdateEntity<ChildEntityT, ParentEntityT, ChildDetailMvcModelT>(this, entityItem, parentId, out mvcModelItem);
        }

        protected ActionResult GoToListScreen(long parentId)
        {
            return ControllerCommon.GoToListScreen(this, parentId);
        }
        protected ActionResult StayOnDetailScreen(ChildEntityT entityItem)
        {
            UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChanges();
            return ControllerCommon.StayOnDetailScreen(this, entityItem.Id);
        }
        #endregion
    }
}
