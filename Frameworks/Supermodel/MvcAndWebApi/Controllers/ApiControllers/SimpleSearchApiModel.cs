﻿using Supermodel.DDD.Models.View.WebApi;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers
{
    public class SimpleSearchApiModel : SearchApiModel
    {
        public string SearchTerm { get; set; }
    }
}