﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.WebApi;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    public abstract class SyncEnhancedApiCRUDController<EntityT, ApiModelT, SearchApiModelT, DbContextT> : SyncEnhancedApiCRUDController<EntityT, ApiModelT, ApiModelT, SearchApiModelT, DbContextT>
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where ApiModelT : ApiModelForEntity<EntityT>, new()
        where SearchApiModelT : SearchApiModel, new()
    { }

    public abstract class SyncEnhancedApiCRUDController<EntityT, DetailApiModelT, ListApiModelT, SearchApiModelT, DbContextT> : SyncApiCRUDController<EntityT, DetailApiModelT, ListApiModelT, DbContextT>, ISyncEnhancedApiCRUDController<DetailApiModelT, ListApiModelT, SearchApiModelT> 
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where DetailApiModelT : ApiModelForEntity<EntityT>, new()
        where ListApiModelT : ApiModelForEntity<EntityT>, new()
        where SearchApiModelT : SearchApiModel, new()
    {
        #region Action Methods
        [HttpGet]
        public virtual HttpResponseMessage Where(SearchApiModelT smSearchBy, int? smSkip = null, int? smTake = null, string smSortBy = null)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                var entities = GetPagedSortedAndSearchedItems(smSkip, smTake, smSortBy, smSearchBy).ToList();
                var apiModels = new List<ListApiModelT>();
                apiModels = (List<ListApiModelT>)apiModels.MapFromObject(entities);
                return Request.CreateResponse(HttpStatusCode.OK, apiModels);
            }
        }

        [HttpGet]
        public virtual HttpResponseMessage CountWhere(SearchApiModelT smSearchBy, int? smSkip = null, int? smTake = null, string smSortBy = null)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                var count = GetPagedSortedAndSearchedItems(smSkip, smTake, smSortBy, smSearchBy).Count();
                return Request.CreateResponse(HttpStatusCode.OK, count);
            }
        }
        #endregion

        #region Protected Helpers
        protected virtual IQueryable<EntityT> GetPagedSortedAndSearchedItems(int? skip, int? take, string sortBy, SearchApiModelT searchBy)
        {
            var items = GetItems();
            items = ApplySearchBy(items, searchBy);
            items = ApplySortBy(items, sortBy);
            items = ApplySkipAndTake((IOrderedQueryable<EntityT>)items, skip, take);

            return items;
        }
        protected virtual IQueryable<EntityT> ApplySearchBy(IQueryable<EntityT> items, SearchApiModelT searchBy)
        {
            return items;
        }

        protected virtual IOrderedQueryable<EntityT> ApplySortBy(IQueryable<EntityT> items, string sortBy)
        {
            return ControllerCommon.ApplySortBy(items, sortBy);
        }
        #endregion
    }
}
