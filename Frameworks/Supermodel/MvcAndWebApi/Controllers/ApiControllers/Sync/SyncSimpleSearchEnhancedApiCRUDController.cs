﻿using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.WebApi;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    public abstract class SyncSimpleSearchEnhancedApiCRUDController<EntityT, ApiModelT, DbContextT> : SyncSimpleSearchEnhancedApiCRUDController<EntityT, ApiModelT, ApiModelT, DbContextT>
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where ApiModelT : ApiModelForEntity<EntityT>, new()
    {}

    public abstract class SyncSimpleSearchEnhancedApiCRUDController<EntityT, DetailApiModelT, ListApiModelT, DbContextT> : SyncEnhancedApiCRUDController<EntityT, DetailApiModelT, ListApiModelT, SimpleSearchApiModel, DbContextT>
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where DetailApiModelT : ApiModelForEntity<EntityT>, new()
        where ListApiModelT : ApiModelForEntity<EntityT>, new()
    {}
}
