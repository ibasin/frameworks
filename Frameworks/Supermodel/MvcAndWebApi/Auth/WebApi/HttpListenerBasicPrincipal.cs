﻿using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Security.Principal;
using System.Web.Security;

namespace Supermodel.MvcAndWebApi.Auth.WebApi
{
    public class HttpListenerBasicPrincipal : IPrincipal
    {
        public HttpListenerBasicPrincipal(IIdentity identity)
        {
            Identity = identity;
        }
        
        public bool IsInRole(string role)
        {
            if (Identity == null) throw new ProviderException("Role Principal not fully constructed");
            if (Identity.IsAuthenticated || role == null) return false;
            
            role = role.Trim();

            if (_roles == null)
            {
                _roles = new HybridDictionary();
                if (Roles.Provider == null) return false;
                foreach (var str in Roles.Provider.GetRolesForUser(Identity.Name))
                {
                    if (_roles[str] == null) _roles.Add(str, string.Empty);
                }
            }
            return (_roles[role] != null);
        }

        public IIdentity Identity { get; private set; }

        // ReSharper disable once InconsistentNaming
        private HybridDictionary _roles { get; set; }
    }
}