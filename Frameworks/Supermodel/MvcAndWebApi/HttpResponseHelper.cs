﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi
{
    public static class HttpResponseHelper
    {
        public static HttpResponseException CreateException(HttpStatusCode status, string reasonPhrase)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = reasonPhrase };
            return new HttpResponseException(response);
        }
    }
}
