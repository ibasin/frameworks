﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;
using ReflectionMapper;

namespace Supermodel.MvcAndWebApi.WebApiValidation
{
    public class ValidationErrorsApiModel : List<ValidationErrorsApiModel.Error>, IRMapperCustom
    {
        #region Embedded Classes
        public class Error
        {
            #region Constructors
            public Error() { ErrorMessages = new List<string>(); }
            #endregion

            #region Properties
            public string Name { get; set; }
            public List<string> ErrorMessages { get; set; }
            #endregion
        }
        #endregion

        #region Constructors
        public ValidationErrorsApiModel()
        {
            Message = "Validation Error(s)";
        }
        #endregion

        #region IRMapperCustom implemetation
        public object MapFromObjectCustom(object obj, Type objType)
        {
            var modelState = (ModelStateDictionary) obj;
            foreach (var keyValuePair in modelState)
            {
                var error = new Error {Name = keyValuePair.Key};
                foreach (var errorMessage in keyValuePair.Value.Errors.Select(x => x.ErrorMessage)) error.ErrorMessages.Add(errorMessage);
                Add(error);
            }
            return this;
        }
        public object MapToObjectCustom(object obj, Type objType)
        {
            throw new InvalidOperationException();
        }
        #endregion

        #region Properties
        public string Message { get; set; }
        #endregion
    }
}
