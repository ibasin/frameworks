﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync;

namespace Supermodel.Mobile.Xamarin
{
    public class ModelGenerator
    {
        #region Constructors
        public ModelGenerator(Assembly webAssembly, string nameSpace = "Supermodel.Mobile.Runtime.Models"):this(new List<Assembly> { webAssembly }, nameSpace ){}
        public ModelGenerator(IEnumerable<Assembly> assemblies, string nameSpace = "Supermodel.Mobile.Runtime.Models")
        {
            Assemblies = assemblies;
            NameSpace = nameSpace;
        }
        #endregion

        #region Methods
        public CSOutStringBuilder GenerateModelsAndSaveToDisk(string filename = @"..\..\" + "Supermodel.Mobile.ModelsAndRuntime.cs", CSOutStringBuilder sb = null)
        {
            if (sb == null) sb = new CSOutStringBuilder();
            sb = GenerateModels(sb);
            File.WriteAllText(filename, sb.ToString());
            return sb;
        }
        public CSOutStringBuilder GenerateModels(CSOutStringBuilder sb = null)
        {
            _globalCustomTypesDefined.Clear();
            
            if (sb == null) sb = new CSOutStringBuilder();
            var customTypesDefined = new List<Type>();
            
            sb.AppendLine("//   DO NOT MAKE CHANGES TO THIS FILE. THEY MIGHT GET OVERWRITTEN!!!");
            sb.AppendLine("//   Autogenrated by Supermodel.Mobile on " + DateTime.Now);
            sb.AppendLine("//");
            sb.AppendLine(EmbeddedResource.ReadTextFile(GetType().Assembly, "Supermodel.Mobile.Xamarin.Header.cs"));
            sb.AppendLine("#region Models");
            sb.AppendLineFormat("namespace {0}", NameSpace);
            sb.AppendLineIndentPlus("{");
            sb.AppendLine("using System;");
            sb.AppendLine("using Mobile.Models;");
            sb.AppendLine("using Attributes;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine("using System.ComponentModel;");
            
            foreach (var controllerType in FindAllSupermodelControllers())
            {                
                if (!controllerType.Name.EndsWith("Controller")) throw new SupermodelSystemErrorException("Controller " +  controllerType.Name + ": names by convention must end with 'Controller'");
                var modelTypes = GetModelTypesForController(controllerType);
                var detailAndListAreSameModel = (modelTypes[0] == modelTypes[1]);
                var restUrlAttribute = string.Format("[RestUrl(\"{0}\")]", controllerType.Name.Replace("Controller", ""));

                sb.AppendLine("");
                sb.AppendLineFormat("#region {0}", controllerType.Name);
                if (detailAndListAreSameModel)
                {
                    if (!modelTypes[0].Name.EndsWith("ApiModel")) throw new SupermodelSystemErrorException("Model " +  modelTypes[0].Name + ": names by convention must end with 'ApiModel'");
                }
                else
                {
                    if (!modelTypes[0].Name.EndsWith("DetailApiModel")) throw new SupermodelSystemErrorException("Detail Model " +  modelTypes[0].Name + ": names by convention must end with 'DetailApiModel'");
                }
                
                if (!_globalCustomTypesDefined.Contains(modelTypes[0]))
                {
                    _globalCustomTypesDefined.Add(modelTypes[0]);
                    sb.AppendLine(restUrlAttribute);
                    sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                    sb.AppendLineFormat("public partial class {0} : Model", modelTypes[0].Name.Replace("DetailApiModel", "").Replace("ApiModel", ""));
                    sb = GeneratePropertiesForModel(modelTypes[0], customTypesDefined, false, sb);
                }
                else
                {
                    sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[0].Name.Replace("DetailApiModel", "").Replace("ApiModel", ""));
                }

                //if List and Detail types are diffeent, do a separate one here
                if (!detailAndListAreSameModel)
                {
                    sb.AppendLine("");
                    sb.AppendLine(restUrlAttribute);
                    if (!modelTypes[1].Name.EndsWith("ListApiModel")) throw new SupermodelSystemErrorException("List Model " +  modelTypes[0].Name + ": names by convention must end with 'ListApiModel'");
                    
                    if (!_globalCustomTypesDefined.Contains(modelTypes[1]))
                    {
                        _globalCustomTypesDefined.Add(modelTypes[1]);
                        sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                        sb.AppendLineFormat("public partial class {0} : Model", modelTypes[1].Name.Replace("ApiModel", ""));
                        sb = GeneratePropertiesForModel(modelTypes[1], customTypesDefined, false, sb);
                    }
                    else
                    {
                        sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[1].Name.Replace("ApiModel", ""));
                    }
                }

                //if we are dealing with an enhanced controller, do the search model
                if (modelTypes.Count == 3)
                {
                    sb.AppendLine("");
                    if (!modelTypes[2].Name.EndsWith("SearchApiModel")) throw new SupermodelSystemErrorException("Search Model " +  modelTypes[0].Name + ": names by convention must end with 'SearchApiModel'");
                    
                    if (!_globalCustomTypesDefined.Contains(modelTypes[2]))
                    {
                        _globalCustomTypesDefined.Add(modelTypes[2]);
                        sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                        sb.AppendLineFormat("public partial class {0}", modelTypes[2].Name.Replace("ApiModel", ""));
                        sb = GeneratePropertiesForModel(modelTypes[2], customTypesDefined, true, sb);
                    }
                    else
                    {
                        sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[2].Name.Replace("ApiModel", ""));
                    }
                }
                sb.AppendLine("#endregion");
            }

            //Add types marked by an InlcueInMobileRuntime attribute
            foreach (var assembly in Assemblies)
            {
                var types = assembly.GetTypes().Where(x => Attribute.IsDefined(x, typeof(IncludeInMobileRuntimeAttribute))).ToList();
                foreach (var type in types)
                {
                    if (!_globalCustomTypesDefined.Contains(type))
                    {
                        customTypesDefined.Add(type);
                        _globalCustomTypesDefined.Add(type);
                    }
                }
            }
            
            sb.AppendLine("");
            sb.AppendLine("#region Types models depend on and types that were specifically marked with [IncludeInMobileRuntime]");
            sb = GenerateCustomTypes(customTypesDefined, sb);
            sb.AppendLine("#endregion");

            sb.AppendLineIndentMinus("}");
            sb.AppendLine("#endregion");
            
            sb.AppendLine("");
            
            //sb = GenerateRuntimeLibFromDir(@"..\..\Mobile\Xamarin\Supermodel.Mobile.Runtime", sb);
            var runtimeLib = EmbeddedResource.ReadTextFile(GetType().Assembly, "Supermodel.Mobile.Xamarin.Supermodel.Mobile.Runtime.cs");
            sb.AppendLine(runtimeLib);
            return sb;
        }
        #endregion

        #region Private Helper Methods
        private CSOutStringBuilder GeneratePropertiesForModel(Type modelType, List<Type> customTypesDefined, bool generateIdProp, CSOutStringBuilder sb)
        {
            if (sb == null) sb = new CSOutStringBuilder();

            sb.AppendLineIndentPlus("{");
            sb.AppendLine("#region Properties");
            foreach (var property in GetPublicProperties(modelType))
            {
                //skip if property is marked with [JsonIgnore]
                if (property.GetCustomAttributes().SingleOrDefault(x => x is JsonIgnoreAttribute) != null) continue;
                
                var customModelAttributeAttribute = (MobileModelAttributeAttribute)property.GetCustomAttributes().SingleOrDefault(x => x is MobileModelAttributeAttribute);
                if (customModelAttributeAttribute != null) sb.Append(customModelAttributeAttribute.AttrContent + " ");
                
                if (property.PropertyType.IsPrimitive)
                {
                    if (generateIdProp || property.Name != "Id")
                    {
                        sb.AppendFormat("public {0} {1} ", property.PropertyType.Name, property.Name);
                        sb.AppendLine("{ get; set; }");
                    }
                }
                else if (property.PropertyType != typeof(string) && property.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                {
                    var genericArgType = property.PropertyType.GetInterfaces().Single(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];
                    if (!_globalCustomTypesDefined.Contains(genericArgType))
                    {
                        customTypesDefined.Add(genericArgType);
                        _globalCustomTypesDefined.Add(genericArgType);
                    }
                    sb.AppendFormat("public List<{0}> {1} ", genericArgType.Name.Replace("ApiModel", ""), property.Name);
                    sb.AppendLine("{ get; set; }");
                }  
                else if (Nullable.GetUnderlyingType(property.PropertyType) != null)
                {
                    var underlyingPropType = Nullable.GetUnderlyingType(property.PropertyType);
                    sb.AppendFormat("public {0}? {1} ", underlyingPropType.Name, property.Name);
                    if (underlyingPropType.IsEnum)
                    {
                        if (!_globalCustomTypesDefined.Contains(underlyingPropType))
                        {
                            customTypesDefined.Add(underlyingPropType);
                            _globalCustomTypesDefined.Add(underlyingPropType);
                        }
                    }
                    sb.AppendLine("{ get; set; }");

                }
                else if (property.PropertyType == typeof(string) || property.PropertyType == typeof(DateTime) || property.PropertyType.Name == "BinaryFileApiModel")
                {
                    sb.AppendFormat("public {0} {1} ", property.PropertyType.Name.Replace("ApiModel", ""), property.Name);
                    sb.AppendLine("{ get; set; }");
                }
                else if (property.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                {
                    sb.AppendFormat("public {0} {1} ", property.PropertyType.Name, property.Name);
                    var underlyingPropType = property.PropertyType.GetInterfaces().Single(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GenericTypeArguments[0];
                    if (!underlyingPropType.IsPrimitive && !_globalCustomTypesDefined.Contains(underlyingPropType))
                    {
                        customTypesDefined.Add(underlyingPropType);
                        _globalCustomTypesDefined.Add(underlyingPropType);
                    }
                    sb.AppendLine("{ get; set; }");
                }
                else if (property.PropertyType.IsEnum || property.PropertyType.IsClass || property.PropertyType.IsValueType)
                {
                    sb.AppendFormat("public {0} {1} ", property.PropertyType.Name.Replace("ApiModel", ""), property.Name);
                    if (!_globalCustomTypesDefined.Contains(property.PropertyType))
                    {
                        customTypesDefined.Add(property.PropertyType);
                        _globalCustomTypesDefined.Add(property.PropertyType);
                    }
                    sb.AppendLine("{ get; set; }");
                }
                else
                {
                    throw new SupermodelSystemErrorException("Unexpected property type '" + property.PropertyType.Name + "'");
                }
            }
            sb.AppendLine("#endregion");
            sb.AppendLineIndentMinus("}");
            return sb;
        }
        private CSOutStringBuilder GenerateCustomTypes(List<Type> customTypesDefined, CSOutStringBuilder sb)
        {
            var additionalCustomTypes = new List<Type>();
            var firstcustomType = true;
            foreach (var customType in customTypesDefined)
            {
                if (firstcustomType) firstcustomType = false;
                else sb.AppendLine("");

                if (customType.IsEnum)
                {
                    sb.AppendLineFormat("public enum {0}", customType.Name);
                    sb.AppendLineIndentPlus("{");
                    var first = true;
                    foreach (var enumValue in Enum.GetValues(customType))
                    {
                        var descriptionAttribute = "";
                        var descriptionStr = enumValue.GetDescription();
                        if (descriptionStr != enumValue.ToString()) descriptionAttribute = string.Format("[Description(\"{0}\")] ", descriptionStr);

                        var disabledAttribute = "";
                        var isDisabled = enumValue.IsDisabled();
                        if (isDisabled) disabledAttribute = "[Disabled] ";
                        
                        if (first)
                        {
                            sb.AppendFormat("{0}{1}{2} = {3}", descriptionAttribute, disabledAttribute, enumValue.ToString(), Convert.ChangeType(enumValue, Enum.GetUnderlyingType(customType)));
                            first = false;
                        }
                        else
                        {
                            sb.AppendLine(",");
                            sb.AppendFormat("{0}{1}{2} = {3}", descriptionAttribute, disabledAttribute, enumValue.ToString(), Convert.ChangeType(enumValue, Enum.GetUnderlyingType(customType)));
                        }
                    }
                    sb.AppendLine("");
                    sb.AppendLineIndentMinus("}");
                }
                else if (customType.IsClass || customType.IsValueType)
                {
                    sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                    sb.AppendLineFormat("public partial class {0}", customType.Name.Replace("ApiModel", ""));
                    sb = GeneratePropertiesForModel(customType, additionalCustomTypes, true, sb);
                }
                else
                {
                    throw new SupermodelSystemErrorException("Unexpected Custom Type '" + customType.Name + "'");
                }
            }
            
            if (additionalCustomTypes.Any()) GenerateCustomTypes(additionalCustomTypes, sb);
            return sb;
        }
        private List<PropertyInfo> GetPublicProperties(Type modelType)
        {
            return modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.CanRead && x.CanWrite).ToList();
        }
        private List<Type> GetModelTypesForController(Type controllerType)
        {
            var modelTypes = new List<Type>();
            var controllerInterfaceType = controllerType.GetInterfaces().Single(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _synchInterfaceType || i.GetGenericTypeDefinition() == _asynchInterfaceType));
            var genericArguments = controllerInterfaceType.GetGenericArguments();
            modelTypes.Add(genericArguments[0]);
            modelTypes.Add(genericArguments[1]);
            
            //if this is an enhanced CRUD controller, grab the third argument (SearchType)
            var controllerEnhancedInterfaceType = controllerType.GetInterfaces().SingleOrDefault(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _synchEnhancedInterfaceType || i.GetGenericTypeDefinition() == _asynchEnhancedInterfaceType));
            if (controllerEnhancedInterfaceType != null) modelTypes.Add(controllerEnhancedInterfaceType.GetGenericArguments()[2]);
            
            return modelTypes;
        }
        private List<Type> FindAllSupermodelControllers()
        {
            var result = new List<Type>();

            foreach (var assembly in Assemblies)
            {
                var assemblyResult = assembly.GetTypes().Where(t => !t.IsAbstract && t.GetInterfaces()
                    .Any(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _synchInterfaceType || i.GetGenericTypeDefinition() == _asynchInterfaceType)))
                    .ToList();
                result.AddRange(assemblyResult);
            }

            return result;
        }
        private static CSOutStringBuilder GenerateRuntimeLibFromDir(string initialDir, CSOutStringBuilder sb = null)
        {
            try
            {
                if (sb == null) sb = new CSOutStringBuilder();
                // ReSharper disable once PossibleNullReferenceException
                sb.AppendLine("");
                var dirName = Path.GetFileName(initialDir);
                sb.AppendLineIndentPlus("#region " + dirName);
                //we only need these three directories for Android and iOS
                //if (dirName == "Multipart" || dirName == "ReflectionMapper" || dirName == "Encryptor") sb.AppendLine("#if __ANDROID__ || __IOS__");

                foreach (var dir in Directory.GetDirectories(initialDir))
                {
                    GenerateRuntimeLibFromDir(dir, sb);
                }
                
                foreach (var file in Directory.GetFiles(initialDir))
                {
                    sb.AppendLine("");
                    sb.AppendLine("#region " + Path.GetFileNameWithoutExtension(file));

                    using (var fileStream = new StreamReader(file))
                    {
                        while (true)
                        {
                            var line = fileStream.ReadLine();
                            if (line == null) break;
                            sb.AppendLine(line);
                        }
                    }
                    
                    sb.AppendLine("#endregion");
                }

                sb.AppendLine("");
                //if (dirName == "Multipart" || dirName == "ReflectionMapper" || dirName == "Encryptor") sb.AppendLine("#endif");
                sb.AppendLineIndentMinus("#endregion");
            }
            catch (Exception ex)
            {
                Console.WriteLine("FATAL ERROR: " + ex.Message);
                Console.WriteLine("Press enter");
                Console.ReadLine();
                Environment.Exit(-1);
            }
            return sb;
        }
        #endregion

        #region Proprties & Attributes
        public IEnumerable<Assembly> Assemblies { get; set;}
        public string NameSpace { get; set; }

        private readonly HashSet<Type> _globalCustomTypesDefined = new HashSet<Type>();
        private readonly Type _synchEnhancedInterfaceType = typeof(ISyncEnhancedApiCRUDController<,,>);
        private readonly Type _asynchEnhancedInterfaceType = typeof(IAsyncEnhancedApiCRUDController<,,>);
        private readonly Type _synchInterfaceType = typeof(ISyncApiCRUDController<,>);
        private readonly Type _asynchInterfaceType = typeof(IAsyncApiCRUDController<,>);

        #endregion
    }
}
