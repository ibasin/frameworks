﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Supermodel.DDD.Models.Domain
{
    public interface IEntity : IValidatableObject
    {
        long Id { get; }
        IEntity ConstructVirtualProperties();
        void Add();
        void Delete();
        bool IsNewModel();
        void BeforeSave(EntityState entityState);
    }
}