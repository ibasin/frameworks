﻿using System.Web.Mvc;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TweeterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TweeterBS
    {
        public class AccordeonPanel
        {
            public AccordeonPanel(){}
            public AccordeonPanel(string elementId, string title, int screenOrderFrom, int screenOrderTo)
            {
                ElementId = elementId;
                Title = title;
                ScreenOrderFrom = screenOrderFrom;
                ScreenOrderTo = screenOrderTo;
            }
            
            public string ElementId { get; set; }
            public string Title { get; set; }
            public int ScreenOrderFrom { get; set; }
            public int ScreenOrderTo { get; set; }
        }
    }
}
