﻿using System;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TweeterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TweeterBS
    {
        public class DropdownMvcModelUsingEnum<EnumT> : UIComponents.DropdownMvcModelUsingEnum<EnumT> where EnumT : struct, IConvertible {}
    }
}

