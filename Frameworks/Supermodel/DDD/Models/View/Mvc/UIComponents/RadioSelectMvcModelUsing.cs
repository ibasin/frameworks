﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Repository;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class RadioSelectMvcModelUsing<MvcModelT> : SingleSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
    {
        #region Constructors
        public RadioSelectMvcModelUsing()
        {
            var mvcModelForEntitybaseType = ReflectionHelper.IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(typeof (MvcModelT), typeof (MvcModelForEntity<>));
            if (mvcModelForEntitybaseType == null) throw new SupermodelSystemErrorException("RadioSelectMvcModelUsing<MvcModelT> has invalid type parameter");
            var entityType = mvcModelForEntitybaseType.GetGenericArguments()[0];

            Options = ((IDataRepoGenericTypeIgnorant) RepoFactory.CreateForRuntimeType(entityType)).GetDropdownOptions<MvcModelT>();
            SelectedValue = "";
        }
        public RadioSelectMvcModelUsing(long selectedId): this()
        {
            SelectedValue = selectedId.ToString(CultureInfo.InvariantCulture);
        }
        #endregion

        #region ISupermodelEditorTemplate implementation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return DropdownMvcModel.DropdownCommonEditorTemplate(html, HtmlAttributesAsDict);
        }
        #endregion

        #region Properties
        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}
