﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public abstract class TextBoxForNumberMvcModelBase<NumT> : IRMapperCustom, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate, ISupermodelMvcModelBinder, IComparable
    {
        #region Constructors
        protected TextBoxForNumberMvcModelBase()
        {
            Pattern = null;
        }
        #endregion

        #region ICustomMapper implemtation
        public object MapFromObjectCustom(object obj, Type objType)
        {
            var domainObj = (NumT)obj;
            Value = domainObj;

            return this;
        }
        public object MapToObjectCustom(object obj, Type objType)
        {
            return Value;
        }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public virtual MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
            htmlAttributes.Add("type", "number");
            //htmlAttributes.Add("data-clear-btn", true);
            if (Pattern != null) htmlAttributes.Add("pattern", Pattern);
            //if (Step != null) htmlAttributes.Add("step", Step);
            return html.TextBox("", GetStringValue(), htmlAttributes);
        }
        #endregion

        #region ISupermodelDisplayTemplate
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return MvcHtmlString.Create(GetStringValue());
        }
        #endregion

        #region ISupermodelHiddenTemplate implemtation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return html.Hidden("", GetStringValue());
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public abstract object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext);
        #endregion

        #region IComparable implemetation
        public abstract int CompareTo(object obj);
        #endregion

        #region Protected Helpers
        protected string GetStringValue()
        {
            // ReSharper disable CompareNonConstrainedGenericWithNull
            return (IsNulableValue() && Value == null) ? "" : Value.ToString();
            // ReSharper restore CompareNonConstrainedGenericWithNull
        }
        protected bool IsNulableValue()
        {
            var type = typeof(NumT);
            return (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
        #endregion

        #region ToString override
        public override string ToString()
        {
            return Value.ToString();
        }
        #endregion

        #region Properties
        public NumT Value { get; set; }
        public string Pattern { get; set; }
        //public NumT Step { get; set; }
        public const string IntegerPattern = "[0-9]*";

        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }

}
