﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class SliderForIntMvcModel : TextBoxForIntMvcModel
    {
        #region Contructors
        public SliderForIntMvcModel()
        {
            Min = 0;
            Max = 100;
            Step = 1;
        }
        #endregion

        #region ISupermodelEditorTemplate implemetation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                
            htmlAttributes.Add("type", "range");
            htmlAttributes.Add("Min", Min);
            htmlAttributes.Add("Max", Max);
            htmlAttributes.Add("Step", Step);

            return html.TextBox("", GetStringValue(), htmlAttributes);
        }
        #endregion

        #region ISupermodelDisplayTemplate implementation
        public override MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute).Supermodel().DisableAllControls();
        }
        #endregion

        #region Properties
        public int Min { get; set; }
        public int Max { get; set; }
        public int Step { get; set; }
        #endregion
    }
}
