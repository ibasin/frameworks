﻿using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Models.View.Mvc
{
    public interface ILoginMvcModel
    {
        TextBoxForStringMvcModel Username { get; set; }
        TextBoxForPasswordMvcModel Password { get; set; }
    }
}
