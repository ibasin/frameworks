﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class CheckboxesListHorizontalMvcModelUsing<MvcModelT> : MultiSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
        {
            #region ISupermodel EditorTemplate implementation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
                if (!(html.ViewData.Model is MultiSelectMvcModelCore)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from CheckboxesListFormModel.");

                var multiSelect = (MultiSelectMvcModelCore)html.ViewData.Model;
                var fieldsetHtmlAttributesAsDict = FieldsetHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                fieldsetHtmlAttributesAsDict.Add("data-type", "horizontal");
                return CheckboxesListVerticalMvcModelUsing<MvcModelT>.RenderCheckBoxesList(html, "", multiSelect.GetSelectListItemList(), fieldsetHtmlAttributesAsDict, InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            #endregion

            #region Properties
            public object FieldsetHtmlAttributesAsObj { set { FieldsetHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> FieldsetHtmlAttributesAsDict { get; set; }

            public object InputHtmlAttributesAsObj { set { InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set { LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}