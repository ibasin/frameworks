﻿namespace Supermodel.DDD.Models.View.Mvc.JQMobile
{
    public abstract partial class JQMobile
    {
        public class LoginMvcModel : MvcModel, ILoginMvcModel
        {
            public LoginMvcModel()
            {
                Username = new TextBoxForStringMvcModel();
                Password = new TextBoxForPasswordMvcModel();
            }
            
            public UIComponents.TextBoxForStringMvcModel Username { get; set; }
            public UIComponents.TextBoxForPasswordMvcModel Password { get; set; }
        }
    }
}