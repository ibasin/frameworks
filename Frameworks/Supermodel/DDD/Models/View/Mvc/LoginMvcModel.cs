﻿using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Models.View.Mvc
{
    public class LoginMvcModel : MvcModel, ILoginMvcModel
    {
        public TextBoxForStringMvcModel Username { get; set; }
        public TextBoxForPasswordMvcModel Password { get; set; }
    }
}