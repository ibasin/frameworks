﻿using System.Collections.Generic;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Repository 
{
    public interface IDataRepo<EntityT> : IDataRepoGenericTypeIgnorant, IDataRepoReadOnly<EntityT>, IDataRepoWriteOnly<EntityT> where EntityT : class, IEntity, new()
    {
        //EntityT GetOrCreate(Expression<Func<EntityT, bool>> predicatesLambda);
        //Task<EntityT> GetOrCreateAsync(Expression<Func<EntityT, bool>> predicatesLambda);

        List<SingleSelectMvcModel.Option> GetDropdownOptions<MvcModelT>(IEnumerable<EntityT> entities) where MvcModelT : MvcModelForEntityCore;
        List<MultiSelectMvcModelCore.Option> GetMultiSelectOptions<MvcModelT>(IEnumerable<EntityT> entities) where MvcModelT : MvcModelForEntityCore;
    }
}
