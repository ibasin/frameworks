﻿using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Repository 
{
	public interface IRepoFactory
	{
	    IDataRepo<EntityT> CreateRepo<EntityT>() where EntityT : class, IEntity, new();
	}
}
