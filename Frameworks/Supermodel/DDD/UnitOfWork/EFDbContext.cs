﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ReflectionMapper;
using Supermodel.DDD.Models.Data;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Repository;

namespace Supermodel.DDD.UnitOfWork
{
    public abstract class EFDbContext : DbContext, IDbContext
    {
        #region Constructors
        protected EFDbContext(string databaseNameOrConnectionString) : base(databaseNameOrConnectionString)
        {
            CommitOnDispose = true;
            IsReadOnly = false;
            IsCompletedAndFinalized = false;
            CustomValues = new Dictionary<string, object>();
        }
        #endregion

        #region Methods
        //Returns the assemblies containing entities and entity config classes for this unit of work. Default returns AppDomain.CurrentDomain.GetAssemblies(). Override it for finer control 
        protected virtual Assembly[] GetDomainEntitiesAssemblies() { return AppDomain.CurrentDomain.GetAssemblies(); }

        public void InitStorageIfApplies()
        {
            //Do nothing. EF takes care of this when db is first accessed
        }

        public int FinalSaveChanges()
        {
            int result;
            try
            {
                result = SaveChanges();
            }
            finally
            {
                MakeCompletedAndFinalized();
            }
            return result;
        }
        public async Task<int> FinalSaveChangesAsync()
        {
            int result;
            try
            {
                result = await SaveChangesAsync();
            }
            finally
            {
                MakeCompletedAndFinalized();
            }
            return result;
        }

        public override int SaveChanges()
        {
            try
            {
                if (!CommitOnDispose || IsReadOnly || IsCompletedAndFinalized) return 0;
                
                //Force detect changes
                ChangeTracker.DetectChanges();
                
                //find modified iteam and call OnSave for each of them
                var modifiedItems = ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged);
                foreach (var item in modifiedItems)
                {
                    var entity = item.Entity as IEntity;

                    if (entity != null) entity.BeforeSave(item.State);
                    else throw new SupermodelSystemErrorException("In Supermodel all Entities must derive from an Entity class");
                }
                
                //find modified relations, based on solution: http://stackoverflow.com/a/9833201/39396
                //var objectContext = ((IObjectContextAdapter)this).ObjectContext;
                //var addedRelations = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added).Where(x => x.IsRelationship);
                //var removedRelations = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted).Where(x => x.IsRelationship);
                //var modifiedRelations = addedRelations.Concat(removedRelations);

                return base.SaveChanges();
            }
            // ReSharper disable RedundantCatchClause
            #pragma warning disable 168
            catch (Exception ex)
            {
                throw; //we need this for debugging
            }
            #pragma warning restore 168
            // ReSharper restore RedundantCatchClause
        }
        public override async Task<int> SaveChangesAsync()
        {
            try
            {
                if (!CommitOnDispose || IsReadOnly || IsCompletedAndFinalized) return 0;
                
                //Force detect changes
                ChangeTracker.DetectChanges();
                
                //find modified iteam and call OnSave for each of them
                var modifiedItems = ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged);
                foreach (var item in modifiedItems)
                {
                    var entity = item.Entity as IEntity;
                    if (entity != null) entity.BeforeSave(item.State);
                    else throw new SupermodelSystemErrorException("In Supermodel all Entities must derive from an Entity class");
                }
                
                //find modified relations, based on solution: http://stackoverflow.com/a/9833201/39396
                //var objectContext = ((IObjectContextAdapter)this).ObjectContext;
                //var addedRelations = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added).Where(x => x.IsRelationship);
                //var removedRelations = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted).Where(x => x.IsRelationship);
                //var modifiedRelations = addedRelations.Concat(removedRelations);

                return await base.SaveChangesAsync(CancellationToken.None);
            }
            // ReSharper disable RedundantCatchClause
            #pragma warning disable 168
            catch (Exception ex)
            {
                throw; //we need this for debugging
            }
            #pragma warning restore 168
            // ReSharper restore RedundantCatchClause
        }

        public IQueryable<EntityT> Items<EntityT>() where EntityT : class, IEntity, new()
        {
            return Set<EntityT>();
        }

        public virtual IDataRepo<EntityT> CreateRepo<EntityT>() where EntityT : class, IEntity, new()
        {
            if (CustomRepoFactoryList != null)
            {
                foreach (var customFactory in CustomRepoFactoryList)
                {
                    var repo = customFactory.CreateRepo<EntityT>();
                    if (repo != null) return repo;
                }
            }
            return new SqlLinqEFSimpleRepo<EntityT>();
        }
        protected virtual List<IRepoFactory> CustomRepoFactoryList => null;

        public IDbContextTransaction BeginTransactionIfApplies()
        {
            return new EFDbContextTransaction(Database.BeginTransaction());
        }

        public new void Dispose()
        {
            if (CommitOnDispose && !IsReadOnly && !IsCompletedAndFinalized) SaveChanges();
            base.Dispose();
        }

        protected IEntity CreateEntityWithValues(Type entityType, DbPropertyValues originalValues)
        {
            var entity = (IEntity) ReflectionHelper.CreateType(entityType);
            foreach (var propertyName in originalValues.PropertyNames) entity.PropertySet(propertyName, originalValues[propertyName]);
            return entity;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            IsReadOnly = false; //If we are creating a new model, we must set ReadOnly to false even it was created as readOnly. Otherwise our Seed will not get saved
            
            base.OnModelCreating(modelBuilder);

            //Set up custom supermodel datatypes
            modelBuilder.Configurations.Add(new BinaryFileTypeMapping());//.Add(new EmailTypeMapping());//.Add(new EFEnumTypeMapping())

            //Set up configurations that are derived from EFDataModelConfigForEntity and EFDataModelConfigForValueType
            foreach (var assembly in GetDomainEntitiesAssemblies())
            {
                Type[] assemblyTypes;
                try { assemblyTypes = assembly.GetTypes(); }
                catch (ReflectionTypeLoadException) { continue; }
                
                foreach (var assemblyType in assemblyTypes)
                {
                    if (assemblyType.IsAbstract || assemblyType.IsGenericType) continue;

                    var baseTypeOfEFDataModelConfigForEntity = ReflectionHelper.IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(assemblyType, typeof(EFDataModelConfigForEntity<>));
                    var baseTypeOfEFDataModelConfigForValueType = ReflectionHelper.IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(assemblyType, typeof(EFDataModelConfigForValueType<>));

                    if (baseTypeOfEFDataModelConfigForEntity != null)
                    {
                        var instanceOfEFDataModelConfigForEntityInstance = ReflectionHelper.CreateType(assemblyType);
                        //modelBuilder.Configurations.ExecuteGenericMethod("Add", baseTypeOfEFDataModelConfigForEntity.GetGenericArguments(), instanceOfEFDataModelConfigForEntityInstance);
                        this.ExecuteNonPublicGenericMethod("AddToConfiguration", baseTypeOfEFDataModelConfigForEntity.GetGenericArguments(), modelBuilder, instanceOfEFDataModelConfigForEntityInstance);
                    }
                    else if (baseTypeOfEFDataModelConfigForValueType != null)
                    {
                        var instanceOfEFDataModelConfigForValueType = ReflectionHelper.CreateType(assemblyType);
                        //modelBuilder.Configurations.ExecuteGenericMethod("Add", baseTypeOfEFDataModelConfigForValueType.GetGenericArguments(), instanceOfEFDataModelConfigForValueType);
                        this.ExecuteNonPublicGenericMethod("AddToConfiguration", baseTypeOfEFDataModelConfigForValueType.GetGenericArguments(), modelBuilder, instanceOfEFDataModelConfigForValueType);
                    }
                }
            }

            //Set up configurations that implememnt IEntity
            foreach (var assembly in GetDomainEntitiesAssemblies())
            {
                Type[] assemblyTypes;
                try { assemblyTypes = assembly.GetTypes(); }
                catch (ReflectionTypeLoadException) { continue; }

                foreach (var assemblyType in assemblyTypes)
                {
                    if (assemblyType.IsAbstract) continue;
                    if (typeof(IEntity).IsAssignableFrom(assemblyType)) modelBuilder.ExecuteGenericMethod("Entity", new[] { assemblyType });
                }
            }
        }

        // We call this method through reflection to avoid dealing with Configurations.Add() overloads
        protected ConfigurationRegistrar AddToConfiguration<EntityT>(DbModelBuilder modelBuilder, EntityTypeConfiguration<EntityT> entityTypeConfiguration) where EntityT : class
        {
            return modelBuilder.Configurations.Add(entityTypeConfiguration);
        }
        #endregion

        #region Properties
        public bool CommitOnDispose { get; set; }
        public bool IsReadOnly { get; protected set; }
        public void MakeReadOnly()
        {
            IsReadOnly = true;
            
            //this should improve performance for read only operations
            Configuration.AutoDetectChangesEnabled = false; 
            Configuration.ValidateOnSaveEnabled = false;
        }
        public bool IsCompletedAndFinalized { get; protected set; }
        public void MakeCompletedAndFinalized()
        {
            IsCompletedAndFinalized = true;
        }

        public Dictionary<string, object> CustomValues { get; protected set; } 
        #endregion
    }
}
