﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.Model;
using Linq2DynamoDb.DataContext;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.DynamoDb
{
    public abstract class DDbDataModelConfigForEntity<EntityT> where EntityT : class, IEntity, new()
    {
        public bool CreateTableIfNotExists(DDbContext currentContext)
        {
            //Create a separatae context becasue we access this without cache
            var separateContext = (DDbContext)ReflectionHelper.CreateType(currentContext.GetType());
            
            //Check if entity table already exists
            var tableName = separateContext.GetTableNameForType<EntityT>();
            if (separateContext.TableExists<EntityT>()) return false;

            //Create identity counter table if does not exists
            if (!separateContext.TableExists(separateContext.MaxHashIdTableNameWithPrefix))
            {
                var createTableRequest = new CreateTableRequest
                {
                    AttributeDefinitions = new List<AttributeDefinition>
                    {
                        new AttributeDefinition { AttributeName = "Table", AttributeType = "S" }
                    },
                    KeySchema = new List<KeySchemaElement>
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Table",
                            KeyType = "HASH"
                        }
                    },
                    ProvisionedThroughput = currentContext.MaxHashIdTableDefaultThroughput,
                    TableName = separateContext.MaxHashIdTableNameWithPrefix
                };

                separateContext.Client.CreateTable(createTableRequest);
                separateContext.WaitUntilTableReady(separateContext.MaxHashIdTableNameWithPrefix);
            }

            //Insert identity counter for the entity table
            var createCounterRequest = new PutItemRequest
            {
                TableName = separateContext.MaxHashIdTableNameWithPrefix,
                Item = new Dictionary<string, AttributeValue>
                {
                    { "Table", new AttributeValue { S = tableName } },
                    { "CurrentHashId", new AttributeValue { N = "0" } }
                }
            };
            separateContext.Client.PutItem(createCounterRequest);

            //Create the entity table
            CreateTableIfNotExistsInternal(currentContext);

            //Wait for table to be created
            currentContext.WaitUntilTableReady<EntityT>();

            return true;
        }
        protected virtual void CreateTableIfNotExistsInternal(DDbContext context)
        {
            context.CreateTableIfNotExists
            (
                new CreateTableArgs<EntityT>
                (
                    5, 5,       // read and write capacities
                    m => m.Id,  // hash key
                    null        // range key
                )
            );
        }
    }
}
