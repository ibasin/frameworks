//DO NOT MAKE CHANGES TO THIS FILE. THEY MIGHT GET OVERWRITTEN!!!
//Autogenrated by Supermodel.Mobile on 6/2/2015 5:57:09 PM
//
//PROJECT SETUP INSTRUCTIONS:
//
//1) Please add the follwoing refernces:
//   a. System.Net.Http
//   b. System.Data.Sqlite (if using Visual Studio) or Mono.Data.Sqlite (if using Xamarin Studio). If programming for server, add it as a Mono.Data.Sqlite NuGet
//   c. System.Data
//   d. System.Web
//   e. System.ComponentModel.DataAnnotations
//   f. System.Runtime.Serialization
//   g. System.Web.Services
//   h. Json.NET (NuGet package)
//   i. ModernHttpClient (NuGet package) -- this one is only for Mobile
//   j. Xamarin.Forms (NuGet package) -- this one is only for Mobile
//
//2) For iOS only: under build options please add --nolinkaway to Additional mtouch arguments
//   For more info on this, pleasee see: 
//   https://spouliot.wordpress.com/2011/11/01/linker-vs-linked-away-exceptions/

#region Models
namespace Supermodel.Mobile.Runtime.Models
{
	using System;
	using Mobile.Models;
	using Attributes;
	using System.Collections.Generic;
	
	#region DemoPersonController
	[RestUrl("DemoPerson")]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class DemoPerson : Model
	{
		public String Name { get; set; }
		public Double? Height { get; set; }
	}
	
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class DemoPersonSearch
	{
		public String Name { get; set; }
		public Double? FromHeight { get; set; }
		public Double? ToHeight { get; set; }
	}
	#endregion
	
	#region TestPersonController
	[RestUrl("TestPerson")]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class TestPerson : Model
	{
		public String Name { get; set; }
		public DateTime? DOB { get; set; }
		public Int32? ApprovalRating { get; set; }
		public Double? Height { get; set; }
		public Int32? FamilySize { get; set; }
		public String Phone { get; set; }
		public String Email { get; set; }
		public String Url { get; set; }
		public String PIN { get; set; }
		public SexEnum? Sex { get; set; }
		public RaceEnum? Race { get; set; }
		public PoliticalAffiliationEnum? PoliticalAffiliation { get; set; }
		public Boolean? RegisteredToVote { get; set; }
		public Boolean? SecurityClearance { get; set; }
	}
	
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class TestPersonSearch
	{
		public String Name { get; set; }
		public DateTime? DOB { get; set; }
	}
	#endregion
	
	#region Types models depend on and types that were specifically marked with [IncludeInMobileRuntime]
	public enum SexEnum
	{
		Man = 0,
		Woman = 1
	}
	
	public enum RaceEnum
	{
		White = 0,
		Black = 1,
		Asian = 2
	}
	
	public enum PoliticalAffiliationEnum
	{
		Democrat = 0,
		Republican = 1
	}
	#endregion
}
#endregion

#region Supermodel.Mobile.Runtime
	
	#region Async
		
		#region AsyncHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Runtime.Async
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class AsyncHelper
		    {
		        public static void RunSync(Func<Task> task)
		        {
		            var oldContext = SynchronizationContext.Current;
		            var synch = new ExclusiveSynchronizationContext();
		            SynchronizationContext.SetSynchronizationContext(synch);
		            synch.Post(async _ =>
		            {
		                try
		                {
		                    await task();
		                }
		                catch (Exception e)
		                {
		                    synch.InnerException = e;
		                    throw;
		                }
		                finally
		                {
		                    synch.EndMessageLoop();
		                }
		            }, null);
		            synch.BeginMessageLoop();
		
		            SynchronizationContext.SetSynchronizationContext(oldContext);
		        }
		
		        public static T RunSync<T>(Func<Task<T>> task)
		        {
		            var oldContext = SynchronizationContext.Current;
		            var synch = new ExclusiveSynchronizationContext();
		            SynchronizationContext.SetSynchronizationContext(synch);
		            var ret = default(T);
		            synch.Post(async _ =>
		            {
		                try
		                {
		                    ret = await task();
		                }
		                catch (Exception e)
		                {
		                    synch.InnerException = e;
		                    throw;
		                }
		                finally
		                {
		                    synch.EndMessageLoop();
		                }
		            }, null);
		            synch.BeginMessageLoop();
		            SynchronizationContext.SetSynchronizationContext(oldContext);
		            return ret;
		        }
		
		        private class ExclusiveSynchronizationContext : SynchronizationContext
		        {
		            private bool _done;
		            public Exception InnerException { get; set; }
		            readonly AutoResetEvent _workItemsWaiting = new AutoResetEvent(false);
		            readonly Queue<Tuple<SendOrPostCallback, object>> _items = new Queue<Tuple<SendOrPostCallback, object>>();
		
		            public override void Send(SendOrPostCallback d, object state)
		            {
		                throw new NotSupportedException("We cannot send to our same thread");
		            }
		
		            public override void Post(SendOrPostCallback d, object state)
		            {
		                lock (_items)
		                {
		                    _items.Enqueue(Tuple.Create(d, state));
		                }
		                _workItemsWaiting.Set();
		            }
		
		            public void EndMessageLoop()
		            {
		                Post(_ => _done = true, null);
		            }
		
		            public void BeginMessageLoop()
		            {
		                while (!_done)
		                {
		                    Tuple<SendOrPostCallback, object> task = null;
		                    lock (_items)
		                    {
		                        if (_items.Count > 0) task = _items.Dequeue();
		                    }
		                    if (task != null)
		                    {
		                        task.Item1(task.Item2);
		                        // the method threw an exception
		                        if (InnerException != null) throw new AggregateException("AsyncHelper.Run method threw an exception.", InnerException);
		                    }
		                    else
		                    {
		                        _workItemsWaiting.WaitOne();
		                    }
		                }
		            }
		
		            public override SynchronizationContext CreateCopy()
		            {
		                return this;
		            }
		        }
		    }
		}
		#endregion
		
		#region AsyncLock
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Runtime.Async
		{
		    using System;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public class AsyncLock
		    {
		        #region Embedded Types
		        public struct Releaser : IDisposable
		        {
		            #region Contructors
		            internal Releaser(AsyncLock toRelease) { _toRelease = toRelease; }
		            #endregion
		
		            #region IDisposable implemetation
		            public void Dispose()
		            {
		                if (_toRelease != null) _toRelease._semaphore.Release();
		            }
		            #endregion
		
		            #region Attributes
		            private readonly AsyncLock _toRelease;
		            #endregion
		        }
		        #endregion
		
		        #region Constructors
		        public AsyncLock()
		        {
		            _semaphore = new AsyncSemaphore(1);
		            _releaser = Task.FromResult(new Releaser(this));
		        }
		        #endregion
		
		        #region Methods
		        public Task<Releaser> LockAsync()
		        {
		            var wait = _semaphore.WaitAsync();
		            return wait.IsCompleted ?
		                _releaser :
		                wait.ContinueWith((_, state) => new Releaser((AsyncLock)state), this, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		        }
		        #endregion
		
		        #region Attributes
		        private readonly AsyncSemaphore _semaphore;
		        private readonly Task<Releaser> _releaser;
		        #endregion
		    }
		}
		#endregion
		
		#region AsyncSemaphore
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Runtime.Async
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Threading.Tasks;
		
		    public class AsyncSemaphore
		    {
		        #region Methods
		        public AsyncSemaphore(int initialCount)
		        {
		            if (initialCount < 0) throw new ArgumentOutOfRangeException("initialCount");
		            _currentCount = initialCount;
		        }
		        public Task WaitAsync()
		        {
		            lock (_waiters)
		            {
		                if (_currentCount > 0)
		                {
		                    --_currentCount;
		                    return _completed;
		                }
		                else
		                {
		                    var waiter = new TaskCompletionSource<bool>();
		                    _waiters.Enqueue(waiter);
		                    return waiter.Task;
		                }
		            }
		        }
		        public void Release()
		        {
		            TaskCompletionSource<bool> toRelease = null;
		            lock (_waiters)
		            {
		                if (_waiters.Count > 0) toRelease = _waiters.Dequeue();
		                else ++_currentCount;
		            }
		            if (toRelease != null) toRelease.SetResult(true);
		        }
		        #endregion
		
		        #region Attributes
		        private readonly static Task _completed = Task.FromResult(true);
		        private readonly Queue<TaskCompletionSource<bool>> _waiters = new Queue<TaskCompletionSource<bool>>();
		        private int _currentCount;
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Attributes
		
		#region RestUrlAttribute
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Attributes
		{
		    using System;
		
		    public class RestUrlAttribute : Attribute
		    {
		        public RestUrlAttribute(string url)
		        {
		            Url = url;
		        }
		        
		        public string Url { get; set; }
		    }
		}
		#endregion
		
	#endregion
	
	#region DataContext
		
		#region CachedWebApi
			
			#region CachedWebApiDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.CachedWebApi
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Linq;
			    using Encryptor;
			    using System.Text;
			    using System.Threading.Tasks;
			    using Mono.Data.Sqlite;
			    using Core;
			    using Sqlite;
			    using WebApi;
			    using Exceptions;
			    using Models;
			    using Repository;
			    // ReSharper disable RedundantNameQualifier
			    using Supermodel.Mobile.ReflectionMapper;
			    // ReSharper restore RedundantNameQualifier
			
			    public class CachedWebApiDataContext<WebApiDataContextT, SqlliteDataContextT> : DataContextBase, IWebApiAuthorizationContext, ICachedDataContext
			        where WebApiDataContextT : WebApiDataContext, new()
			        where SqlliteDataContextT : SqliteDataContext, new()
			    {
			        #region Constructors
			        public CachedWebApiDataContext()
			        {
			            CacheAgeToleranceInSeconds = 5 * 60; // 5 min
			        }
			        #endregion
			
			        #region Methods
			        public async Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null)
			        {
			            var sqlliteContext = new SqlliteDataContextT();
			
			            await sqlliteContext.InitDbAsync();
			            using (var connection = new SqliteConnection("Data Source=" + sqlliteContext.DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    var sb = new StringBuilder();
			                    sb.AppendFormat(@"DELETE FROM {0}", sqlliteContext.DataTableName);
			                    var first = true;
			                    
			                    if (cacheExpirationAgeInSeconds != null)
			                    {
			                        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
			                        if (first)
			                        {
			                            sb.Append(" WHERE ");
			                            first = false;
			                        }
			                        else
			                        // ReSharper disable HeuristicUnreachableCode
			                        {
			                            sb.Append(" AND ");
			                        }
			                        // ReSharper restore HeuristicUnreachableCode
			                        sb.AppendFormat("BroughtFromMasterDbOnUtcTicks < {0}", DateTime.Now.AddSeconds(cacheExpirationAgeInSeconds.Value).Ticks);
			                    }
			
			                    if (modelType != null)
			                    {
			                        if (first)
			                        {
			                            sb.Append(" WHERE ");
			                            first = false;
			                        }
			                        else
			                        {
			                            sb.Append(" AND ");
			                        }
			                        sb.AppendFormat("ModelTypeLogicalName == '{0}'", GetModelTypeLogicalName(modelType));
			                    }
			
			                    if (first)
			                    {
			                        sb.Append(" WHERE ");
			                        // ReSharper disable once RedundantAssignment
			                        first = false;
			                    }
			                    else
			                    {
			                        sb.Append(" AND ");
			                    }
			                    sb.AppendFormat("ModelTypeLogicalName != '{0}'", sqlliteContext.SchemaVersionModelType);
			
			                    command.CommandText = sb.ToString();
			                    await command.ExecuteNonQueryAsync();
			                }
			            }
			        }
			        #endregion
			
			        #region ValidateLogin
			        public virtual async Task<bool> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
			        {
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.ValidateLoginAsync<ModelT>();
			            }
			        }
			        #endregion
			
			        #region DataContext Reads
			        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(int id)
			        {
			            //First check local cache
			            using (new UnitOfWork<SqlliteDataContextT>())
			            {
			                var cachedModel = await RepoFactory.Create<ModelT>().GetByIdOrDefaultAsync(id);
			                if (cachedModel != null)
			                {
			                    if (cachedModel.BroughtFromMasterDbOn != null && cachedModel.BroughtFromMasterDbOn.Value.AddSeconds(CacheAgeToleranceInSeconds) > DateTime.Now)
			                    {
			                        ManagedModels.Add(new ManagedModel(cachedModel));
			                        return cachedModel;
			                    }
			                    else
			                    {
			                        cachedModel.Delete();
			                    }
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			                else
			                {
			                    //Mark done for performance reasons
			                    UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.MakeCompletedAndFinalized();
			                }
			            }
			
			            //if we get here, we need to get the data from web api service
			            ModelT masterModel;
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			                
			                masterModel = await RepoFactory.Create<ModelT>().GetByIdOrDefaultAsync(id);
			            }
			
			            //Now save master model to cache and add it to managed models if we have something to save
			            if (masterModel != null)
			            {
			                using (new UnitOfWork<SqlliteDataContextT>())
			                {
			                    ManagedModels.Add(new ManagedModel(masterModel)); 
			                    UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			                
			            }
			
			            return masterModel;
			        }
			        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            //first delete local cache, we will be refreshing it with new data
			            var sqlliteContext = new SqlliteDataContextT();
			            await sqlliteContext.InitDbAsync();
			            using (var connection = new SqliteConnection("Data Source=" + sqlliteContext.DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    command.CommandText = string.Format(@"DELETE FROM {0} WHERE ModelTypeLogicalName == '{1}'", sqlliteContext.DataTableName, GetModelTypeLogicalName(typeof(ModelT)));
			                    await command.ExecuteNonQueryAsync();
			                }
			            }
			            
			            //get the data from web api service
			            List<ModelT> masterModels;
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                masterModels = await RepoFactory.Create<ModelT>().GetAllAsync(skip, take);
			            }
			
			            //Now save master models to cache if we have something to save
			            if (masterModels.Any())
			            {
			                using (new UnitOfWork<SqlliteDataContextT>())
			                {
			                    foreach (var masterModel in masterModels)
			                    {
			                        ManagedModels.Add(new ManagedModel(masterModel)); 
			                        UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
			                    }
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			            }
			
			            return masterModels;
			        }
			        public override async Task<int> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.GetCountAllAsync<ModelT>(skip, take);
			            }
			        }
			        #endregion
			
			        #region DataContext Queries
			        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
			        {
			            //if we get here, we need to get the data from web api service
			            List<ModelT> masterModels;
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                masterModels = await RepoFactory.Create<ModelT>().GetWhereAsync(searchBy, sortBy, skip, take);
			            }
			
			            //Now save master models to cache if we have something to save
			            if (masterModels.Any())
			            {
			                using (new UnitOfWork<SqlliteDataContextT>())
			                {
			                    foreach (var masterModel in masterModels)
			                    {
			                        ManagedModels.Add(new ManagedModel(masterModel));
			                        UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
			                    }
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			            }
			
			            return masterModels;
			        }
			        public override async Task<int> GetCountWhereAsync<ModelT>(object searchBy)
			        {
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.GetCountWhereAsync<ModelT>(searchBy);
			            }
			        }
			        #endregion
			
			        #region DataContext Save Changes
			        public async override Task SaveChnagesInternalAsync(List<PendingAction> pendingActions)
			        {
			            //First we delete all objects in cache that are about to be updated -- we do this for transcational integrity
			            var actionsToLoopThrough = IsReadOnly ? PendingActions.Where(x => x.IsReadOnlyAction) : PendingActions;
			            using (new UnitOfWork<SqlliteDataContextT>())
			            {
			                // ReSharper disable once PossibleMultipleEnumeration
			                foreach (var pendingAction in actionsToLoopThrough)
			                {
			                    switch (pendingAction.Operation)
			                    {
			                        case PendingAction.OperationEnum.AddWithExistingId:
			                        case PendingAction.OperationEnum.Update:
			                        case PendingAction.OperationEnum.Delete:
			                        case PendingAction.OperationEnum.AddOrUpdate:
			                        case PendingAction.OperationEnum.DelayedGetById:
			                        case PendingAction.OperationEnum.DelayedGetByIdOrDefault:
			                        {
			                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("Delete", new[] { pendingAction.ModelType }, pendingAction.ModelId);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetAll:
			                        {
			                            var sqlliteContext = new SqlliteDataContextT();
			                            await sqlliteContext.InitDbAsync();
			                            using (var connection = new SqliteConnection("Data Source=" + sqlliteContext.DatabaseFilePath))
			                            {
			                                await connection.OpenAsync();
			                                using (var command = connection.CreateCommand())
			                                {
			                                    command.CommandText = string.Format(@"DELETE FROM {0} WHERE ModelTypeLogicalName == '{1}'", sqlliteContext.DataTableName, GetModelTypeLogicalName(pendingAction.ModelType));
			                                    await command.ExecuteNonQueryAsync();
			                                }
			                            }
			                            break;
			                        }
			                        case PendingAction.OperationEnum.GenerateIdAndAdd:
			                        case PendingAction.OperationEnum.DelayedGetWhere:
			                        case PendingAction.OperationEnum.DelayedGetCountAll:
			                        case PendingAction.OperationEnum.DelayedGetCountWhere:
			                        {
			                            //for these we do nothing
			                            break;
			                        }
			                        default:
			                        {
			                            throw new SupermodelException("Unsupported Operation");
			                        }
			                    }
			                }
			                await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			            }
			
			            //Then we attempt to save to web api service
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                if (IsReadOnly) UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.SaveChnagesInternalAsync(PendingActions);
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeCompletedAndFinalized();
			            }
			
			            //Them if we were successful, we update local db
			            using (new UnitOfWork<SqlliteDataContextT>())
			            {
			                // ReSharper disable once PossibleMultipleEnumeration
			                foreach (var pendingAction in actionsToLoopThrough)
			                {
			                    switch (pendingAction.Operation)
			                    {
			                        case PendingAction.OperationEnum.AddWithExistingId:
			                        case PendingAction.OperationEnum.GenerateIdAndAdd:
			                        case PendingAction.OperationEnum.Update:
			                        case PendingAction.OperationEnum.AddOrUpdate:
			                        {
			                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, pendingAction.Model);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.Delete: //we need to delete again becasue we could have read data with delayed read
			                        {
			                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("Delete", new[] { pendingAction.ModelType }, pendingAction.ModelId);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetById:
			                        case PendingAction.OperationEnum.DelayedGetByIdOrDefault:
			                        {
			                            var model = (IModel)pendingAction.DelayedValue.GetValue();
			                            if (model != null)
			                            {
			                                UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, model);
			                            }
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetAll:
			                        case PendingAction.OperationEnum.DelayedGetWhere:
			                        {
			                            var models = (IEnumerable<IModel>)pendingAction.DelayedValue.GetValue();
			                            foreach (var model in models)
			                            {
			                                UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, model);
			                            }
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetCountAll:
			                        case PendingAction.OperationEnum.DelayedGetCountWhere:
			                        {
			                            //for these we do nothing
			                            break;
			                        }
			                        default:
			                        {
			                            throw new SupermodelException("Unsupported Operation");
			                        }
			                    }
			                }
			                await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			            }
			        }
			        #endregion
			
			        #region Configuration Properties
			        public AuthHeader AuthHeader { get; set; }
			        public int CacheAgeToleranceInSeconds { get; set; }
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region Core
			
			#region DataContextBase
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.Collections.Generic;
			    using System.ComponentModel.DataAnnotations;
			    using System.Linq;
			    using System.Threading.Tasks;
			    using Exceptions;
			    using Models;
			    using Repository;
			    using Attributes;
			    
			    public abstract class DataContextBase : IQuerableReadableDataContext, IWriteableDataContext
			    {
			        #region Contructors
			        protected DataContextBase()
			        {
			            CommitOnDispose = true;
			            IsReadOnly = false;
			            IsCompletedAndFinalized = false;
			            
			            ManagedModels = new List<ManagedModel>();
			            PendingActions = new List<PendingAction>();
			        }
			        #endregion
			
			        #region Methods
			        public static string GetModelTypeLogicalName<ModelT>() where ModelT : class, IModel
			        {
			            return GetModelTypeLogicalName(typeof(ModelT));
			        }
			        public static string GetModelTypeLogicalName(Type type)
			        {
			            if (!typeof(IModel).IsAssignableFrom(type)) throw new SupermodelException("GetModelTypeLogicalName can only be called for types that implement IModel");
			            var restUrlAttribute = type.GetCustomAttributes(typeof(RestUrlAttribute), true).FirstOrDefault() as RestUrlAttribute;
			            return restUrlAttribute == null ? type.Name : restUrlAttribute.Url;
			        }
			        public void DetectUpdates()
			        {
			            //remove all update that are already in PendingActions
			            PendingActions.RemoveAll(x => x.Operation == PendingAction.OperationEnum.Update);
			
			            DetectUpdatesInternal();
			        }
			        protected void DetectUpdatesInternal()
			        {
			            //detect new updates and put them in PendingActions
			            foreach (var managedModel in ManagedModels.Where(managedModel => managedModel.NeedsUpdating()))
			            {
			                PendingActions.Add(new PendingAction
			                {
			                    Operation = PendingAction.OperationEnum.Update,
			                    ModelType = managedModel.Model.GetType(),
			                    ModelId = managedModel.Model.Id,
			                    Model = managedModel.Model,
			                    DelayedValue = null,
			                    SearchBy = null,
			                    Skip = null,
			                    Take = null,
			                    SortBy = null
			                }.Validate());
			            }
			        }
			        #endregion
			
			        #region DataContext Reads
			        public virtual async Task<ModelT> GetByIdAsync<ModelT>(int id) where ModelT : class, IModel, new()
			        {
			            var model = await GetByIdOrDefaultAsync<ModelT>(id);
			            if (model == null) throw new SupermodelException("GetByIdAsync(id): no object exists with id = " + id);
			            return model;
			        }
			        public abstract Task<ModelT> GetByIdOrDefaultAsync<ModelT>(int id) where ModelT : class, IModel, new();
			        public abstract Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        public abstract Task<int> GetCountAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        #endregion
			
			        #region DataContext Delayed Reads
			        public void DelayedGetById<ModelT>(out DelayedModel<ModelT> model, int id) where ModelT : class, IModel, new()
			        {
			            model = new DelayedModel<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetById,
			                ModelType = typeof(ModelT),
			                ModelId = id,
			                Model = null,
			                DelayedValue = model,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void DelayedGetByIdOrDefault<ModelT>(out DelayedModel<ModelT> model, int id) where ModelT : class, IModel, new()
			        {
			            model = new DelayedModel<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetByIdOrDefault,
			                ModelType = typeof(ModelT),
			                ModelId = id,
			                Model = null,
			                DelayedValue = model,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void DelayedGetAll<ModelT>(out DelayedModels<ModelT> models) where ModelT : class, IModel, new()
			        {
			            models = new DelayedModels<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetAll,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                Model = null,
			                DelayedValue = models,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void DelayedGetCountAll<ModelT>(out DelayedCount count) where ModelT : class, IModel, new()
			        {
			            count = new DelayedCount();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetCountAll,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                Model = null,
			                DelayedValue = count,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        #endregion
			
			        #region DataContext Queries
			        public abstract Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        public abstract Task<int> GetCountWhereAsync<ModelT>(object searchBy) where ModelT : class, IModel, new();
			        #endregion
			
			        #region DataContext Delayed Queries
			        public void DelayedGetWhere<ModelT>(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new()
			        {
			            models = new DelayedModels<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetWhere,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                Model = null,
			                DelayedValue = models,
			                SearchBy = searchBy,
			                Skip = skip,
			                Take = take,
			                SortBy = sortBy
			            }.Validate());
			        }
			        public void DelayedGetCountWhere<ModelT>(out DelayedCount count, object searchBy) where ModelT : class, IModel, new()
			        {
			            count = new DelayedCount();
			
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetCountWhere,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                Model = null,
			                DelayedValue = count,
			                SearchBy = searchBy,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        #endregion
			
			        #region DataContext Writes
			        public void Add<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            var operation = model.Id == 0 ? PendingAction.OperationEnum.GenerateIdAndAdd : PendingAction.OperationEnum.AddWithExistingId;
			            PendingActions.Add(new PendingAction
			            {
			                Operation = operation,
			                ModelType = typeof(ModelT),
			                ModelId = model.Id,
			                Model = model,
			                DelayedValue = null,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void Delete<ModelT>(int modelId) where ModelT : class, IModel, new()
			        {
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.Delete,
			                ModelType = typeof(ModelT),
			                ModelId = modelId,
			                Model = null,
			                DelayedValue = null,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void ForceUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            var managedModel = ManagedModels.SingleOrDefault(x => x.Model == model);
			            if (managedModel == null) throw new Exception("Unable to ForceUpdate for the model not tracked by the context");
			            managedModel.ForceUpdate = true;
			        }
			        #endregion
			
			        #region IDisposable implemetation
			        public void Dispose()
			        {
			            if (IsCompletedAndFinalized) return;
			
			            if (IsReadOnly || !CommitOnDispose) PendingActions.RemoveAll(x => !x.IsReadOnlyAction);
			            else DetectUpdatesInternal();
			
			            //OptimizePendingActions();
			
			            // ReSharper disable once SimplifyLinqExpression
			            if (!PendingActions.Any()) return;
			
			            throw new SupermodelException("Must run FinalSaveChangesAsync or Finalize otherwise before you are done with the unit of work");
			        }
			        #endregion
			        
			        #region DataContext Configuration
			        public bool CommitOnDispose { get; set; }
			        public bool IsReadOnly { get; protected set; }
			        public void MakeReadOnly()
			        {
			            IsReadOnly = true;
			        }
			        public bool IsCompletedAndFinalized { get; protected set; }
			        public void MakeCompletedAndFinalized()
			        {
			            IsCompletedAndFinalized = true;
			        }
			        #endregion
			
			        #region Context RepoFactory
			        public virtual IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new()
			        {
			            if (CustomRepoFactoryList != null)
			            {
			                foreach (var customFactory in CustomRepoFactoryList)
			                {
			                    var repo = customFactory.CreateRepo<ModelT>();
			                    if (repo != null) return repo;
			                }
			            }
			            return new DataRepo<ModelT>();
			        }
			        protected List<IRepoFactory> CustomRepoFactoryList { get { return null; } }
			        #endregion
			
			        #region DataContext Save Changes
			        public async Task FinalSaveChangesAsync()
			        {
			            try
			            {
			                await SaveChangesAsync(true);
			            }
			            finally
			            {
			                MakeCompletedAndFinalized();
			            }
			        }
			        public async Task SaveChangesAsync()
			        {
			            await SaveChangesAsync(false);
			        }
			        protected virtual async Task SaveChangesAsync(bool isFinal)
			        {
			            if (IsCompletedAndFinalized) return;
			
			            if (IsReadOnly || !CommitOnDispose) PendingActions.RemoveAll(x => !x.IsReadOnlyAction);
			            else DetectUpdatesInternal();
			
			            // ReSharper disable once SimplifyLinqExpression
			            if (!PendingActions.Any(x => !x.Disabled)) return;
			
			            OptimizePendingActions();            
			            ValidatePendingActions();
			
			            await SaveChnagesInternalAsync(PendingActions);
			
			            if (!isFinal)
			            {
			                //Update all managed models with the new hash and clear PendingAction, so that we can save multiple times in the same unit of work
			                foreach (var managedModel in ManagedModels) managedModel.UpdateHash();
			                PendingActions.Clear();
			            }
			        }
			        public abstract Task SaveChnagesInternalAsync(List<PendingAction> pendingActions);
			        #endregion
			
			        #region Private Helpers
			        protected void PrepareForThrowingException()
			        {
			            foreach (var pendingAction in PendingActions.Where(x => x.Operation == PendingAction.OperationEnum.GenerateIdAndAdd)) pendingAction.Model.Id = 0;
			            MakeCompletedAndFinalized();
			        }
			        protected void ThrowSupermodelValidationException(SupermodelValidationException.ValidationError validationError)
			        {
			            PrepareForThrowingException();
			            throw new SupermodelValidationException(validationError);
			        }
			        protected void ThrowSupermodelValidationException(List<SupermodelValidationException.ValidationError> validationErrors)
			        {
			            PrepareForThrowingException();
			            throw new SupermodelValidationException(validationErrors);
			        }
			        protected virtual void ValidatePendingActions()
			        {
			            //Validate that all disbaled PendingActions have been cleared
			            if (PendingActions.Any(x => x.Disabled)) throw new SupermodelException("ValidatePendingActions: should not have any Disbaled pending actions");
			
			            //Validate that every Pending Action is valid
			            if (PendingActions.Any(x => !x.IsValid())) throw new SupermodelException("One of the PendingActions is invalid. This should never happen");
			            
			            //Validate no duplicate addorupdate, updates, and deletes. For every Id and type there should only be one Update or Delete or AddOrUpdate
			            if (PendingActions.Where(x => x.Operation == PendingAction.OperationEnum.Update || x.Operation == PendingAction.OperationEnum.Delete || x.Operation == PendingAction.OperationEnum.AddOrUpdate).GroupBy(x => new { x.ModelType, x.ModelId }).Any(x => x.Count() > 1))
			            {
			                throw new SupermodelException("Duplicate addorupdates/updates/deletes in PendingActions");
			            }
			
			            //Validate objects themselves
			            var validationErrors = new List<SupermodelValidationException.ValidationError>();
			            foreach (var pendingAction in PendingActions.Where(x => x.Model != null))
			            {
			                var vr = pendingAction.Model.Validate(new ValidationContext(pendingAction.Model, null));
			                if (vr.Any())
			                {
			                    var validationError = new SupermodelValidationException.ValidationError(vr, pendingAction, "There are some Model Validation Errors");
			                    validationErrors.Add(validationError);
			                }
			            }
			            if (validationErrors.Any()) throw new SupermodelValidationException(validationErrors);
			        }
			        protected virtual void OptimizePendingActions()
			        {
			            //Run the optimization, marking redundand operations Disabled
			            for (var i = PendingActions.Count - 1; i >= 0; i--)
			            {
			                var pendingAction = PendingActions[i];
			                if (pendingAction.Disabled) continue;
			                switch (pendingAction.Operation)
			                {
			                    case PendingAction.OperationEnum.Delete:
			                    {
			                        for (var j = i - 1; j >= 0; j--)
			                        {
			                            var pendingAction2 = PendingActions[j];
			                            if (pendingAction2.Disabled || pendingAction.ModelId != pendingAction2.ModelId || pendingAction.ModelType != pendingAction2.ModelType) continue;
			                            switch (pendingAction2.Operation)
			                            {
			                                case PendingAction.OperationEnum.Delete: pendingAction.Disabled = true; break;
			                                case PendingAction.OperationEnum.AddOrUpdate: pendingAction2.Disabled = true; break;
			                                case PendingAction.OperationEnum.Update: pendingAction2.Disabled = true; break;
			                                //do nothing for all other operations
			                            }
			                        }
			                        break;
			                    }
			                    case PendingAction.OperationEnum.AddOrUpdate:
			                    {
			                        for (var j = i - 1; j >= 0; j--)
			                        {
			                            var pendingAction2 = PendingActions[j];
			                            if (pendingAction2.Disabled || pendingAction.ModelId != pendingAction2.ModelId || pendingAction.ModelType != pendingAction2.ModelType) continue;
			                            switch (pendingAction2.Operation)
			                            {
			                                case PendingAction.OperationEnum.Delete: pendingAction.Disabled = true; break;
			                                case PendingAction.OperationEnum.AddOrUpdate: pendingAction2.Disabled = true; break;
			                                case PendingAction.OperationEnum.Update: pendingAction2.Disabled = true; break;
			                                //do nothing for all other operations
			                            }
			                        }
			                        break;
			                    }
			                    case PendingAction.OperationEnum.Update:
			                    {
			                        for (var j = i - 1; j >= 0; j--)
			                        {
			                            var pendingAction2 = PendingActions[j];
			                            if (pendingAction2.Disabled || pendingAction.ModelId != pendingAction2.ModelId || pendingAction.ModelType != pendingAction2.ModelType) continue;
			                            switch (pendingAction2.Operation)
			                            {
			                                case PendingAction.OperationEnum.Delete: pendingAction.Disabled = true; break;
			                                case PendingAction.OperationEnum.AddOrUpdate: pendingAction2.Disabled = true; pendingAction.Operation = PendingAction.OperationEnum.AddOrUpdate; break;
			                                case PendingAction.OperationEnum.Update: pendingAction2.Disabled = true; break;
			                                //do nothing for all other operations
			                            }
			                        }
			                        break;
			                    }
			                    //do nothing for all other operations
			                }
			            }
			            
			            //Remove disabled operations
			            PendingActions.RemoveAll(x => x.Disabled);
			        }
			        #endregion
			
			        #region Properties & Constants
			        protected List<ManagedModel> ManagedModels { get; set; }
			        protected List<PendingAction> PendingActions { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region DelayedValues
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Collections.Generic;
			    using Models;
			    
			    public abstract class DelayedValue
			    {
			        public abstract void SetValue(object value);
			        public abstract object GetValue();
			    }
			    
			    public class DelayedModel<ModelT> : DelayedValue where ModelT : class, IModel, new() 
			    {
			        #region Overrides
			        public override void SetValue(object value)
			        {
			            Value = (ModelT)value;
			        }
			
			        public override object GetValue()
			        {
			            return Value;
			        }
			        #endregion
			
			        #region Properties
			        public ModelT Value { get; set; }
			        #endregion
			    }
			
			    public class DelayedModels<ModelT> : DelayedValue where ModelT : class, IModel, new()
			    {
			        #region Overrides
			        public override void SetValue(object value)
			        {
			            Values = (List<ModelT>)value;
			        }
			        public override object GetValue()
			        {
			            return Values;
			        }
			        #endregion
			
			        #region Properties
			        public List<ModelT> Values { get; set; }
			        #endregion
			    }
			
			    public class DelayedCount : DelayedValue
			    {
			        #region Overrides
			        public override void SetValue(object value)
			        {
			            Value = (int?) value;
			        }
			        public override object GetValue()
			        {
			            return Value;
			        }
			        #endregion
			
			        #region Properties
			        public int? Value { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region HashExtensions
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.IO;
			    using System.Runtime.Serialization;
			    using System.Security.Cryptography;
			    
			    public static class HashExtensions
			    {
			        public static string GetHash<T>(this object instance) where T : HashAlgorithm, new()
			        {
			            T cryptoServiceProvider = new T();
			            return ComputeHash(instance, cryptoServiceProvider);
			        }
			        public static string GetKeyedHash<T>(this object instance, byte[] key) where T : KeyedHashAlgorithm, new()
			        {
			            T cryptoServiceProvider = new T { Key = key };
			            return ComputeHash(instance, cryptoServiceProvider);
			        }
			        public static string GetMD5Hash(this object instance)
			        {
			            return instance.GetHash<MD5CryptoServiceProvider>();
			        }
			        public static string GetSHA1Hash(this object instance)
			        {
			            return instance.GetHash<SHA1CryptoServiceProvider>();
			        }
			        private static string ComputeHash<T>(object instance, T cryptoServiceProvider) where T : HashAlgorithm, new()
			        {
			            var serializer = new DataContractSerializer(instance.GetType());
			            using (var memoryStream = new MemoryStream())
			            {
			                serializer.WriteObject(memoryStream, instance);
			                cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
			                return Convert.ToBase64String(cryptoServiceProvider.Hash);
			            }
			        }
			    }
			}
			#endregion
			
			#region ICachedDataContext
			 // ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.Threading.Tasks;
			    
			    public interface ICachedDataContext
			    {
			        int CacheAgeToleranceInSeconds { get; set; }
			        Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null);
			    }
			}
			#endregion
			
			#region IDataContext
			
			
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using Models;
			    using Repository;
			    
			    public interface IDataContext : IDisposable
			    {
			        #region Configuration
			        bool CommitOnDispose { get; set; }
			        bool IsReadOnly { get; }
			        void MakeReadOnly();
			        #endregion
			
			        #region Context RepoFactory
			        IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new();
			        #endregion
			    }
			}
			
			#endregion
			
			#region IQuerableReadableDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Collections.Generic;
			    using System.Threading.Tasks;
			    using Models;
			    
			    public interface IQuerableReadableDataContext : IReadableDataContext
			    {
			        #region Queries
			        Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        Task<int> GetCountWhereAsync<ModelT>(object searchBy) where ModelT : class, IModel, new();
			        #endregion
			
			        #region Delayed Queries
			        void DelayedGetWhere<ModelT>(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        void DelayedGetCountWhere<ModelT>(out DelayedCount count, object searchBy) where ModelT : class, IModel, new();
			        #endregion
			    }
			}
			#endregion
			
			#region IReadableDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Collections.Generic;
			    using System.Threading.Tasks;
			    using Models;
			    
			    public interface IReadableDataContext : IDataContext
			    {
			        #region Reads
			        Task<ModelT> GetByIdAsync<ModelT>(int id) where ModelT : class, IModel, new();
			        Task<ModelT> GetByIdOrDefaultAsync<ModelT>(int id) where ModelT : class, IModel, new();
			        Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        Task<int> GetCountAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        #endregion
			
			        #region Batch Reads
			        void DelayedGetById<ModelT>(out DelayedModel<ModelT> model, int id) where ModelT : class, IModel, new();
			        void DelayedGetByIdOrDefault<ModelT>(out DelayedModel<ModelT> model, int id) where ModelT : class, IModel, new();
			        void DelayedGetAll<ModelT>(out DelayedModels<ModelT> models) where ModelT : class, IModel, new();
			        void DelayedGetCountAll<ModelT>(out DelayedCount count) where ModelT : class, IModel, new();
			        #endregion
			    }
			}
			#endregion
			
			#region IWebApiAuthorizationContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using Encryptor;
			    using System.Threading.Tasks;
			    using Models;
			
			    public interface IWebApiAuthorizationContext
			    {
			        AuthHeader AuthHeader { get; set; }
			        Task<bool> ValidateLoginAsync<ModelT>() where ModelT : class, IModel;
			    }
			}
			#endregion
			
			#region IWriteableDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Threading.Tasks;
			    using Models;
			    
			    public interface IWriteableDataContext : IDataContext
			    {
			        #region Writes
			        void Add<ModelT>(ModelT model) where ModelT : class, IModel, new();
			        void Delete<ModelT>(int modelId) where ModelT : class, IModel, new();
			        void ForceUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new();
			        void DetectUpdates();
			        #endregion
			
			        #region Save Changes
			        Task SaveChangesAsync();
			        Task FinalSaveChangesAsync();
			        #endregion
			    }
			}
			#endregion
			
			#region ManagedModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using Models;
			    
			    public class ManagedModel
			    {
			        #region Constructors
			        public ManagedModel(IModel originalModel)
			        {
			            Model = originalModel;
			            UpdateHash();
			        }
			        #endregion
			
			        #region Methods
			        public void UpdateHash()
			        {
			            OriginalModelHash = Model.GetMD5Hash();
			        }
			        public bool NeedsUpdating()
			        {
			            return ForceUpdate || Model.GetMD5Hash() != OriginalModelHash;
			        }
			        #endregion
			
			        #region Properties
			        public IModel Model { get; set; }
			        public bool ForceUpdate { get; set; }
			        protected string OriginalModelHash { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region PendingAction
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Data.Common;
			    using System.Net.Http;
			    using System.Text;
			    using System.Threading.Tasks;
			    using Newtonsoft.Json;
			    using Sqlite;
			    using WebApi;
			    using Exceptions;
			    using Models;
			    // ReSharper disable RedundantNameQualifier
			    using Supermodel.Mobile.ReflectionMapper;
			    // ReSharper restore RedundantNameQualifier
			
			    
			    public class PendingAction 
			    {
			        #region Embedded Enums
			        public enum OperationEnum { AddWithExistingId, GenerateIdAndAdd, Update, Delete, DelayedGetById, DelayedGetByIdOrDefault, DelayedGetAll, DelayedGetCountAll, DelayedGetWhere, DelayedGetCountWhere, AddOrUpdate }
			        public enum OperationSqlType { SingleResultQuery, ListResultQuery, ScalarResultQuery, NoQueryResult }
			        #endregion
			
			        #region Constructors
			        public PendingAction()
			        {
			            Disabled = false;
			        }
			        public PendingAction(PendingAction other)
			        {
			            Disabled = other.Disabled;
			            Operation = other.Operation;
			            ModelType = other.ModelType;
			            ModelId = other.ModelId;
			            Model = other.Model;
			            DelayedValue = other.DelayedValue;
			            SearchBy = other.SearchBy;
			            Skip = other.Skip;
			            Take = other.Take;
			            SortBy = other.SortBy;
			        }
			        #endregion
			
			        #region Methods
			        public PendingAction Validate()
			        {
			            if (!IsValid()) throw new SupermodelException("Invalid PendingAction");
			            return this;
			        }
			        public bool IsValid()
			        {
			            if (ModelType == null) return false;
			
			            switch (Operation)
			            {
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    if (ModelId != 0) return false;
			                    if (Model == null) return false;
			                    break;
			                }
			                case OperationEnum.AddWithExistingId:
			                case OperationEnum.AddOrUpdate:
			                case OperationEnum.Update:
			                {
			                    if (ModelId == 0) return false;
			                    if (Model == null) return false;
			                    break;
			                }
			                case OperationEnum.Delete:
			                {
			                    if (ModelId == 0) return false;
			                    break;
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    if (ModelId == 0) return false;
			                    if (DelayedValue == null) return false;
			                    break;
			                }
			                case OperationEnum.DelayedGetAll:
			                case OperationEnum.DelayedGetWhere:
			                case OperationEnum.DelayedGetCountAll:
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    if (DelayedValue == null) return false;
			                    break;
			                }
			                default:
			                {
			                    throw new SupermodelException("Unsupported Operation");
			                }
			            }
			            return true;
			        }
			        public bool IsReadOnlyAction 
			        {
			            get
			            {
			                switch (Operation)
			                {
			                    case OperationEnum.AddWithExistingId:
			                    case OperationEnum.GenerateIdAndAdd:
			                    case OperationEnum.Update:
			                    case OperationEnum.Delete:
			                    case OperationEnum.AddOrUpdate:
			                        return false;
			
			                    case OperationEnum.DelayedGetById:
			                    case OperationEnum.DelayedGetByIdOrDefault:
			                    case OperationEnum.DelayedGetAll:
			                    case OperationEnum.DelayedGetWhere:
			                    case OperationEnum.DelayedGetCountAll:
			                    case OperationEnum.DelayedGetCountWhere:
			                        return true;
			
			                    default:
			                        throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        public OperationSqlType SqlType
			        {
			            get
			            {
			                switch (Operation)
			                {
			                    case OperationEnum.AddWithExistingId:
			                    case OperationEnum.GenerateIdAndAdd:
			                    case OperationEnum.Update:
			                    case OperationEnum.Delete:
			                    case OperationEnum.AddOrUpdate:
			                        return OperationSqlType.NoQueryResult;
			
			                    case OperationEnum.DelayedGetById:
			                    case OperationEnum.DelayedGetByIdOrDefault:
			                        return OperationSqlType.SingleResultQuery;
			
			                    case OperationEnum.DelayedGetAll:
			                    case OperationEnum.DelayedGetWhere:
			                        return OperationSqlType.ListResultQuery;
			
			                    case OperationEnum.DelayedGetCountAll:
			                    case OperationEnum.DelayedGetCountWhere:
			                        return OperationSqlType.ScalarResultQuery;
			
			                    default:
			                        throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        #endregion
			
			        #region WebApi Methods
			        public HttpRequestMessage GenerateHttpRequest(string baseUrl, IQueryStringProvider queryStringProvider)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                {
			                    throw new SupermodelException("AddWithExistingId is not supported by WebApiContext -- only GenerateIdAndAdd");
			                }
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    if (ModelId != 0 || Model.Id != 0) throw new SupermodelException("When adding a new Model Id must be equal 0.");
			                    return new HttpRequestMessage(HttpMethod.Post, string.Format("{0}{1}", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType)))
			                    {
			                        Content = new StringContent(JsonConvert.SerializeObject(Model.PerpareForSerializingForMasterDb()), Encoding.UTF8, ContentType)
			                    };
			                }
			                case OperationEnum.Update:
			                {
			                    if (ModelId == 0 || Model.Id == 0 || ModelId != Model.Id) throw new SupermodelException("When updating a ModelIds must be not equal 0 and equal to each other");
			                    return new HttpRequestMessage(HttpMethod.Put, string.Format("{0}{1}/{2}", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType), ModelId))
			                    {
			                        Content = new StringContent(JsonConvert.SerializeObject(Model.PerpareForSerializingForMasterDb()), Encoding.UTF8, ContentType)
			                    };
			                }
			                case OperationEnum.Delete:
			                {
			                    return new HttpRequestMessage(HttpMethod.Delete, string.Format("{0}{1}/{2}", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType), ModelId));
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}/{2}", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType), ModelId));
			                }
			                case OperationEnum.DelayedGetAll:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}/All", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType)));
			                }
			                case OperationEnum.DelayedGetCountAll:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}/CountAll", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType)));
			                }
			                case OperationEnum.DelayedGetWhere:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}/Where?{2}", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType), queryStringProvider.GetQueryString(SearchBy, Skip, Take, SortBy)));
			                }
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}/CountWhere?{2}", baseUrl, DataContextBase.GetModelTypeLogicalName(ModelType), queryStringProvider.GetQueryString(SearchBy, Skip, Take, SortBy)));
			                }
			
			                // ReSharper disable once RedundantCaseLabel
			                case OperationEnum.AddOrUpdate:
			                default:
			                    throw new SupermodelException("Unsupported Operation");
			            }
			        }
			        public void ProcessHttpResponse(string responseContentStr)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                {
			                    throw new SupermodelException("AddWithExistingId is not supported by WebApiContext -- only GenerateIdAndAdd");
			                }
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    Model.Id = int.Parse(responseContentStr);
			                    Model.BroughtFromMasterDbOn = DateTime.Now;
			                    break;
			                }
			                case OperationEnum.Update:
			                {
			                    Model.BroughtFromMasterDbOn = DateTime.Now;
			                    break;
			                }
			                case OperationEnum.Delete:
			                {
			                    //Do nothing
			                    break;
			                }
			                case OperationEnum.DelayedGetById:
			                {
			                    if (string.IsNullOrEmpty(responseContentStr))
			                    {
			                        throw new SupermodelException("DelayedGetById: no object exists with id = " + ModelId);
			                    }
			                    else
			                    {
			                        var model = (IModel)JsonConvert.DeserializeObject(responseContentStr, ModelType);
			                        model.BroughtFromMasterDbOn = DateTime.Now;
			                        DelayedValue.SetValue(model);
			                    }
			                    break;
			                }
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    if (string.IsNullOrEmpty(responseContentStr))
			                    {
			                        DelayedValue.SetValue(null);
			                    }
			                    else
			                    {
			                        var model = (IModel)JsonConvert.DeserializeObject(responseContentStr, ModelType);
			                        model.BroughtFromMasterDbOn = DateTime.Now;
			                        DelayedValue.SetValue(model);
			                    }
			                    break;
			                }
			                case OperationEnum.DelayedGetAll:
			                case OperationEnum.DelayedGetWhere:
			                {
			                    var models = (IEnumerable<IModel>)JsonConvert.DeserializeObject(responseContentStr, typeof(List<>).MakeGenericType(ModelType));
			                    foreach (var model in models) model.BroughtFromMasterDbOn = DateTime.Now;
			                    DelayedValue.SetValue(models);
			                    break;
			                }
			                case OperationEnum.DelayedGetCountAll:
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    var count = JsonConvert.DeserializeObject<int>(responseContentStr);
			                    DelayedValue.SetValue(count);
			                    break;
			                }
			                // ReSharper disable once RedundantCaseLabel
			                case OperationEnum.AddOrUpdate:
			                default:
			                {
			                    throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        #endregion
			
			        #region Sqlite Methods
			        public string GenerateSql(string dataTableName, ISqlQueryProvider sqlQueryProvider)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    return new DataRow(dataTableName, Model, sqlQueryProvider).GenerateSqlInsert();
			                }
			                case OperationEnum.AddOrUpdate:
			                {
			                    return new DataRow(dataTableName, Model, sqlQueryProvider).GenerateSqlInsertOrReplace();
			                }
			                case OperationEnum.Update:
			                {
			                    return new DataRow(dataTableName, Model, sqlQueryProvider).GenerateSqlUpdate();
			                }
			                case OperationEnum.Delete:
			                {
			                    return string.Format(@"DELETE FROM [{0}] WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}", dataTableName, DataContextBase.GetModelTypeLogicalName(ModelType), ModelId);
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    return string.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}", dataTableName, DataContextBase.GetModelTypeLogicalName(ModelType), ModelId);
			                }
			                case OperationEnum.DelayedGetAll:
			                {
			                    return string.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}'", dataTableName, DataContextBase.GetModelTypeLogicalName(ModelType));
			                }
			                case OperationEnum.DelayedGetWhere:
			                {
			                    var whereClause = sqlQueryProvider.GetWhereClause(SearchBy, Skip, Take, SortBy);
			                    if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			                    return string.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}' AND {2}", dataTableName, DataContextBase.GetModelTypeLogicalName(ModelType), whereClause);
			                }
			                case OperationEnum.DelayedGetCountAll:
			                {
			                    return string.Format("SELECT COUNT(*) FROM [{0}] WHERE ModelTypeLogicalName = '{1}'", dataTableName, DataContextBase.GetModelTypeLogicalName(ModelType));
			                }
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    var whereClause = sqlQueryProvider.GetWhereClause(SearchBy, Skip, Take, SortBy);
			                    if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			                    return string.Format("SELECT COUNT(*) FROM [{0}] WHERE ModelTypeLogicalName = '{1}' AND {2}", dataTableName, DataContextBase.GetModelTypeLogicalName(ModelType), whereClause);
			                }
			                default:
			                {
			                    throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        public async Task ProcessDatabaseResponseAsync(string dataTableName, object response, ISqlQueryProvider sqlQueryProvider)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                case OperationEnum.GenerateIdAndAdd:
			                case OperationEnum.AddOrUpdate:
			                case OperationEnum.Update:
			                case OperationEnum.Delete:
			                {
			                    //we have nothing to process
			                    return;
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    using (var dr = (DbDataReader)response)
			                    {
			                        if (!await dr.ReadAsync())
			                        {
			                            if (Operation == OperationEnum.DelayedGetById) throw new SupermodelException("DelayedGetById: no object exists with id = " + ModelId);
			                            DelayedValue.SetValue(null);
			                            return;
			                        }
			
			                        var model = new DataRow(dataTableName, dr).GetModel(ModelType);
			                        DelayedValue.SetValue(model);
			                        if (await dr.ReadAsync()) throw new Exception("GetByIdOrAsync or GetByIdOrDefaultAsync brought back more than one record");
			                    }
			                    return;
			
			                }
			                case OperationEnum.DelayedGetAll:
			                case OperationEnum.DelayedGetWhere:
			                {
			                    using (var dr = (DbDataReader) response)
			                    {
			                        var models = ReflectionHelper.CreateGenericType(typeof (List<>), ModelType);
			                        while (await dr.ReadAsync())
			                        {
			                            var model = new DataRow(dataTableName, dr).GetModel(ModelType);
			                            models.ExecuteMethod("Add", model);
			                        }
			                        DelayedValue.SetValue(models);
			                    }
			                }
			                return;
			                case OperationEnum.DelayedGetCountAll:
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    var responseLong = (long)response;
			                    DelayedValue.SetValue((int)responseLong);
			                    return;
			                }
			                default:
			                {
			                    throw new SupermodelException("Unrecognized pendingAction.SqlType");
			                }
			            }
			        }
			        #endregion
			
			        #region Properties and Constants
			        public bool Disabled { get; set; }
			        public OperationEnum Operation { get; set; }
			        public Type ModelType { get; set; }
			        public int ModelId { get; set; }
			        public IModel Model { get; set; }
			        public DelayedValue DelayedValue { get; set; }
			        public object SearchBy { get; set; }
			        public int? Skip { get; set; }
			        public int? Take { get; set; }
			        public string SortBy { get; set; }
			
			        public const string ContentType = "application/json";
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region Sqlite
			
			#region DataRow
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Sqlite
			{
			    using System;
			    using System.Data;
			    using Newtonsoft.Json;
			    using Core;
			    using Models;
			    
			    public class DataRow<ModelT> : DataRow where ModelT : class, IModel, new()
			    {
			        #region Constructors
			        public DataRow(string dataTableName, ModelT model, ISqlQueryProvider sqlQueryProvider) : base(dataTableName, model, sqlQueryProvider)
			        {
			            var modelTypeLogicalName1 = DataContextBase.GetModelTypeLogicalName<ModelT>();
			            var modelTypeLogicalName2 = DataContextBase.GetModelTypeLogicalName(model.GetType());
			            if (modelTypeLogicalName1 != modelTypeLogicalName2) throw new Exception("DataContextBase.GetModelTypeLogicalName<ModelT>() != DataContextBase.GetModelTypeLogicalName(model.GetType())");
			        }
			        public DataRow(string dataTableName, IDataRecord dr) : base(dataTableName, dr) { }
			        #endregion
			
			        #region Methods
			        public ModelT GetModel()
			        {
			            return (ModelT)GetModel(typeof(ModelT));
			        }
			        #endregion
			    }
			    
			    public class DataRow
			    {
			        #region Constructors
			        public DataRow(string dataTableName, IModel model, ISqlQueryProvider sqlQueryProvider)
			        {
			            DataTableName = dataTableName;
			
			            ModelTypeLogicalName = DataContextBase.GetModelTypeLogicalName(model.GetType());
			            ModelId = model.Id;
			            Json = JsonConvert.SerializeObject(model.PerpareForSerializingForLocalDb());
			            BroughtFromMasterDbOn = model.BroughtFromMasterDbOn;       
			            Index1 = sqlQueryProvider.GetIndex(1, model);
			            Index2 = sqlQueryProvider.GetIndex(2, model); 
			            Index3 = sqlQueryProvider.GetIndex(3, model); 
			            Index4 = sqlQueryProvider.GetIndex(4, model);
			            Index5 = sqlQueryProvider.GetIndex(5, model); 
			        }
			        public DataRow(string dataTableName, IDataRecord dr)
			        {
			            DataTableName = dataTableName;
			
			            ModelTypeLogicalName = (string)dr["ModelTypeLogicalName"];
			            ModelId = (long)dr["ModelId"];
			            Json = (string)dr["Json"];
			
			            if (!Convert.IsDBNull(dr["BroughtFromMasterDbOnUtcTicks"])) BroughtFromMasterDbOn = new DateTime((long)dr["BroughtFromMasterDbOnUtcTicks"]);
			            else BroughtFromMasterDbOn = null;
			            
			            if (!Convert.IsDBNull(dr["Index1"])) Index1 = (string) dr["Index1"];
			            else Index1 = null;
			
			            if (!Convert.IsDBNull(dr["Index2"])) Index1 = (string)dr["Index2"];
			            else Index2 = null;
			
			            if (!Convert.IsDBNull(dr["Index3"])) Index1 = (string)dr["Index3"];
			            else Index3 = null;
			
			            if (!Convert.IsDBNull(dr["Index4"])) Index1 = (string)dr["Index4"];
			            else Index4 = null;
			
			            if (!Convert.IsDBNull(dr["Index5"])) Index1 = (string)dr["Index5"];
			            else Index5 = null;
			        }
			        #endregion
			
			        #region Methods
			        public IModel GetModel(Type modelType)
			        {
			            var model = (IModel)JsonConvert.DeserializeObject(Json, modelType);
			            if (model.Id != ModelId || ModelTypeLogicalName != DataContextBase.GetModelTypeLogicalName(modelType)) throw new Exception("Database corruption: (THIS SHOULD NEVER HAPPEN): model.Id != DataRow.ModelId || ModelTypeLogicalName != DataContextBase.GetModelTypeLogicalName<ModelT>()");
			            return model;
			        }
			        public string GenerateSqlInsertOrReplace()
			        {
			            var sql = string.Format(@"INSERT OR REPLACE INTO [{0}] 
			                                      (ModelTypeLogicalName, ModelId, Json, BroughtFromMasterDbOnUtcTicks, Index1, Index2, Index3, Index4, Index5) 
			                                      VALUES ('{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9})",
			                                      DataTableName,
			                                      ModelTypeLogicalName,
			                                      ModelId,
			                                      Json.Replace("'", "''"),
			                                      BroughtFromMasterDbOn != null ? BroughtFromMasterDbOn.Value.Ticks.ToString() : "NULL",
			                                      Index1 != null ? "'" + Index1.Replace("'", "''") + "'" : "NULL",
			                                      Index2 != null ? "'" + Index2.Replace("'", "''") + "'" : "NULL",
			                                      Index3 != null ? "'" + Index3.Replace("'", "''") + "'" : "NULL",
			                                      Index4 != null ? "'" + Index4.Replace("'", "''") + "'" : "NULL",
			                                      Index5 != null ? "'" + Index5.Replace("'", "''") + "'" : "NULL");
			            return sql;
			        }
			        public string GenerateSqlInsert()
			        {
			            var sql = string.Format(@"INSERT INTO [{0}] 
			                                      (ModelTypeLogicalName, ModelId, Json, BroughtFromMasterDbOnUtcTicks, Index1, Index2, Index3, Index4, Index5) 
			                                      VALUES ('{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9})",
			                                      DataTableName,
			                                      ModelTypeLogicalName, 
			                                      ModelId, 
			                                      Json.Replace("'", "''"),
			                                      BroughtFromMasterDbOn != null ? BroughtFromMasterDbOn.Value.Ticks.ToString() : "NULL",
			                                      Index1 != null ? "'" + Index1.Replace("'", "''") + "'" : "NULL",
			                                      Index2 != null ? "'" + Index2.Replace("'", "''") + "'" : "NULL",
			                                      Index3 != null ? "'" + Index3.Replace("'", "''") + "'" : "NULL",
			                                      Index4 != null ? "'" + Index4.Replace("'", "''") + "'" : "NULL",
			                                      Index5 != null ? "'" + Index5.Replace("'", "''") + "'" : "NULL");
			            return sql;
			        }
			        public string GenerateSqlUpdate()
			        {
			            var sql = string.Format(@"UPDATE [{0}] 
			                                      SET 
			                                      Json = '{3}', 
			                                      BroughtFromMasterDbOnUtcTicks = {4},
			                                      Index1 = {5}, 
			                                      Index2 = {6}, 
			                                      Index3 = {7}, 
			                                      Index4 = {8}, 
			                                      Index5 = {9}
			                                      WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}",
			                                      DataTableName,
			                                      ModelTypeLogicalName, 
			                                      ModelId, 
			                                      Json.Replace("'", "''"),
			                                      BroughtFromMasterDbOn != null ? BroughtFromMasterDbOn.Value.Ticks.ToString() : "NULL",
			                                      Index1 != null ? "'" + Index1.Replace("'", "''") + "'" : "NULL",
			                                      Index2 != null ? "'" + Index2.Replace("'", "''") + "'" : "NULL",
			                                      Index3 != null ? "'" + Index3.Replace("'", "''") + "'" : "NULL",
			                                      Index4 != null ? "'" + Index4.Replace("'", "''") + "'" : "NULL",
			                                      Index5 != null ? "'" + Index5.Replace("'", "''") + "'" : "NULL");
			            return sql;
			        }
			        #endregion
			
			        #region Properties
			        public string ModelTypeLogicalName { get; set; }
			        public long ModelId { get; set; }
			        public string Json { get; set; }
			        public DateTime? BroughtFromMasterDbOn { get; set; }
			        public string Index1 { get; set; }
			        public string Index2 { get; set; }
			        public string Index3 { get; set; }
			        public string Index4 { get; set; }
			        public string Index5 { get; set; }
			
			        public string DataTableName { get; protected set; }
			        #endregion
			    }
			}
			#endregion
			
			#region ISqlQueryProvider
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Sqlite
			{
			    using Models;
			    
			    public interface ISqlQueryProvider
			    {
			        string GetIndex(int idxNum, IModel model);
			        string GetWhereClause(object searchBy, int? skip, int? take, string sortBy);
			    }
			}
			#endregion
			
			#region SqliteDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Sqlite
			{
			    using System;
			    using System.Collections.Generic;
			    using System.IO;
			    using System.Linq;
			    using System.Text;
			    using System.Threading;
			    using System.Threading.Tasks;
			    using Mono.Data.Sqlite;
			    using Core;
			    using Exceptions;
			    using Models;
			    using System.Runtime.Remoting.Messaging;
			    
			    public abstract class SqliteDataContext : DataContextBase, ISqlQueryProvider
			    {
			        #region ISqlQueryProvider implemetation
			        public virtual string GetIndex(int idxNum, IModel model)
			        {
			            if (idxNum < 1 || idxNum >5) throw new SupermodelException("Only Indexes 1-5 are allowed");
			            return null;
			        }
			        public virtual string GetWhereClause(object searchBy, int? skip, int? take, string sortBy)
			        {
			            return null;
			        }
			        #endregion
			
			        #region DataContext Reads
			        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(int id)
			        {
			            if (await InitDbAsync()) return null;
			
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			                    command.CommandText = String.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}", DataTableName, modelTypeLogicalName, id);
			                    using (var dr = await command.ExecuteReaderAsync())
			                    {
			                        if (!await dr.ReadAsync()) return null;
			
			                        var model = new DataRow<ModelT>(DataTableName, dr).GetModel();
			                        ManagedModels.Add(new ManagedModel(model));
			                        if (await dr.ReadAsync()) throw new Exception("GetByIdOrDefaultAsync brought back more than one record");
			                        return model;
			                    }
			                }
			            }
			        }
			        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return new List<ModelT>();
			
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			                    command.CommandText = String.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}'", DataTableName, modelTypeLogicalName);
			                    if (take != null) command.CommandText += " LIMIT " + take;
			                    if (skip != null) command.CommandText += " OFFSET " + skip;
			                    using (var dr = await command.ExecuteReaderAsync())
			                    {
			                        var models = new List<ModelT>();
			                        while (await dr.ReadAsync())
			                        {
			                            var model = new DataRow<ModelT>(DataTableName, dr).GetModel();
			                            ManagedModels.Add(new ManagedModel(model));
			                            models.Add(model);
			                        }
			                        return models;
			                    }
			                }
			            }
			        }
			        public override async Task<int> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return 0;
			
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			
			                    command.CommandText = String.Format("SELECT COUNT(*) FROM [{0}] WHERE ModelTypeLogicalName='{1}'", DataTableName, modelTypeLogicalName);
			                    if (take != null) command.CommandText += " LIMIT " + take;
			                    if (skip != null) command.CommandText += " OFFSET " + skip;
			                    var count = (int)await command.ExecuteScalarAsync();
			                    return count;
			                }
			            }
			        }
			        #endregion
			
			        #region DataContext Queries
			        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return new List<ModelT>();
			
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			
			                    var whereClause = GetWhereClause(searchBy, skip, take, sortBy);
			                    if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			                    command.CommandText = string.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}' AND {2}", DataTableName, modelTypeLogicalName, whereClause);
			                    using (var dr = await command.ExecuteReaderAsync())
			                    {
			                        var models = new List<ModelT>();
			                        while (await dr.ReadAsync())
			                        {
			                            var model = new DataRow<ModelT>(DataTableName, dr).GetModel();
			                            ManagedModels.Add(new ManagedModel(model));
			                            models.Add(model);
			                        }
			                        return models;
			                    }
			                }
			            }
			        }
			        public override async Task<int> GetCountWhereAsync<ModelT>(object searchBy)
			        {
			            if (await InitDbAsync()) return 0;
			
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			                    command.CommandText = string.Format("COUNT(*) FROM [{0}] WHERE ModelTypeLogicalName = '{1}'", DataTableName, modelTypeLogicalName);
			                    var count = (int)await command.ExecuteScalarAsync();
			                    return count;
			                }
			            }
			        }
			        #endregion
			
			        #region DataContext sqllite-specific writes
			        public void AddOrUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            if (model.Id == 0) throw new SupermodelException("AddOrUpdate operation does not allow model id to be 0");
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.AddOrUpdate,
			                ModelType = typeof(ModelT),
			                ModelId = model.Id,
			                Model = model,
			                DelayedValue = null,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        #endregion
			
			        #region DataContext Save Changes
			        protected override async Task SaveChangesAsync(bool isFinal)
			        {
			            await InitDbAsync();
			            await base.SaveChangesAsync(isFinal);
			        }
			        public override async Task SaveChnagesInternalAsync(List<PendingAction> pendingActions)
			        {
			            await InitDbAsync();
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                var transaction = connection.BeginTransaction();
			                try
			                {
			                    for (var i = 0; i < pendingActions.Count; i++)
			                    {
			                        var pendingAction = pendingActions[i];
			                        
			                        //if we have multiple deletes in a row
			                        if (pendingAction.Operation == PendingAction.OperationEnum.Delete && i + 1 < pendingActions.Count && pendingActions[i + 1].Operation == PendingAction.OperationEnum.Delete)
			                        {
			                            var sb = new StringBuilder();
			                            sb.Append(pendingAction.GenerateSql(DataTableName, this));
			                            
			                            int j;
			                            var brokeFromLoop = false;
			                            for (j = 1; i + j < pendingActions.Count; j++)
			                            {
			                                var pendingActionIPlusJ = pendingActions[i + j];
			                                if (pendingActionIPlusJ.Operation != PendingAction.OperationEnum.Delete)
			                                {
			                                    brokeFromLoop = true;
			                                    break;
			                                }
			                                sb.AppendFormat(@" OR ModelTypeLogicalName = '{0}' AND ModelId = {1}", GetModelTypeLogicalName(pendingActionIPlusJ.ModelType), pendingActionIPlusJ.ModelId);
			                            }
			
			                            using (var bulkDeleteCommand = connection.CreateCommand())
			                            {
			                                bulkDeleteCommand.Transaction = transaction;
			                                bulkDeleteCommand.CommandText = sb.ToString();
			                                await bulkDeleteCommand.ExecuteNonQueryAsync();
			                            }
			
			                            if (brokeFromLoop) i = i + j - 1;
			                            else i = i + j;
			                            
			                            continue;
			                        }
			
			                        //if we generate Id, we need to read the nextId first
			                        if (pendingAction.Operation == PendingAction.OperationEnum.GenerateIdAndAdd)
			                        {
			                            using (var nextIdCommand = connection.CreateCommand())
			                            {
			                                nextIdCommand.Transaction = transaction;
			                                nextIdCommand.CommandText = string.Format("SELECT IFNULL(MIN(ModelId), 0)-1 FROM {0} WHERE ModelTypeLogicalName='{1}'", DataTableName, GetModelTypeLogicalName(pendingAction.ModelType));
			                                var nextId = (long)await nextIdCommand.ExecuteScalarAsync();
			                                pendingAction.Model.Id = (int)nextId;
			                            }
			                        }
			
			                        using (var command = connection.CreateCommand())
			                        {
			                            command.Transaction = transaction;
			                            command.CommandText = pendingAction.GenerateSql(DataTableName, this);
			                            switch (pendingAction.SqlType)
			                            {
			                                case PendingAction.OperationSqlType.NoQueryResult:
			                                {
			                                    await command.ExecuteNonQueryAsync();
			                                    await pendingAction.ProcessDatabaseResponseAsync(DataTableName, null, this);
			                                    break;
			                                }
			                                case PendingAction.OperationSqlType.SingleResultQuery:
			                                case PendingAction.OperationSqlType.ListResultQuery:
			                                {
			                                    var dr = await command.ExecuteReaderAsync();
			                                    await pendingAction.ProcessDatabaseResponseAsync(DataTableName, dr, this);
			                                    break;
			                                }
			                                case PendingAction.OperationSqlType.ScalarResultQuery:
			                                {
			                                    var scalar = await command.ExecuteScalarAsync();
			                                    await pendingAction.ProcessDatabaseResponseAsync(DataTableName, scalar, this);
			                                    break;
			                                }
			                                default:
			                                {
			                                    throw new SupermodelException("Unrecognized pendingAction.SqlType");
			                                }
			                            }
			                        }
			                    }
			                    transaction.Commit();
			                }
			                catch (Exception)
			                {
			                    transaction.Rollback();
			                    foreach (var pendingAction in pendingActions.Where(x => x.Operation == PendingAction.OperationEnum.GenerateIdAndAdd)) pendingAction.Model.Id = 0;
			                    throw;
			                }
			            }
			        }
			        #endregion
			
			        #region Db Initialization and Migration
					public virtual int ContextSchemaVersion { get { return 1; } }
			        public async Task UpdateDbSchemaVersionAsync(int schemaVersion)
			        {
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    command.CommandText = string.Format(@"UPDATE [{0}]
			                                                            SET 
			                                                                Json = '{3}', 
			                                                                BroughtFromMasterDbOnUtcTicks = {4},
			                                                                Index1 = NULL, 
			                                                                Index2 = NULL, 
			                                                                Index3 = NULL, 
			                                                                Index4 = NULL, 
			                                                                Index5 = NULL
			                                                            WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}", 
			                                                            DataTableName, SchemaVersionModelType, 0, schemaVersion, DateTime.Now.Ticks);
			
			                    await command.ExecuteNonQueryAsync();
			                }
			            }
			        }
			        public async Task<int> GetDbSchemaVersionAsync()
			        {
			            using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			            {
			                await connection.OpenAsync();
			                using (var command = connection.CreateCommand())
			                {
			                    command.CommandText = String.Format("SELECT * FROM [{0}] WHERE ModelTypeLogicalName = '{1}'", DataTableName, SchemaVersionModelType);
			                    using (var dr = await command.ExecuteReaderAsync())
			                    {
			                        if (!await dr.ReadAsync()) throw new SupermodelException("Db Corruption: Schema Version is not in Db");
			
			                        var dbSchemaVersion = int.Parse((string)dr["Json"]);
			                        if (await dr.ReadAsync()) throw new Exception("GetDbSchemaVersionAsync brought back more than one record for the schema");
			                        return dbSchemaVersion;
			                    }
			                }
			            }
			        }
			        public async Task<bool> InitDbAsync()
			        {
			            //If this is called from inside of migration, don't initiaize or we get a deadlock
			            var supermodelMigrationInProgressOnThisThread = (bool?)CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread") ?? false;
			            if (supermodelMigrationInProgressOnThisThread) return false;
			            
			            //check if we already initialized this run, if we did, don't bother
			            if (_finishedInitializingSqlLiteContext.Contains(GetType())) return false;
			            if (_startedInitializingSqlLiteContext.Contains(GetType()))
			            {
			                const int checkEveryMilliseconds = 250;
			                var totalWait = 0;
			                while (!_finishedInitializingSqlLiteContext.Contains(GetType()))
			                {
			                    await Task.Delay(checkEveryMilliseconds);
			                    totalWait += checkEveryMilliseconds;
			                    //If this was not resolved in 12 seconds, we fail (this should be nearly instanteneouse)
			                    if (totalWait >= 12 * 1000) throw new SupermodelException("Deadlock in InitDbAsync");
			                }
			                return false;
			            }
			            _startedInitializingSqlLiteContext.Add(GetType());
			            try
			            {
			                var newDb = await CreateDatabaseIfNotExistsAsync();
			                
			                //If the database is not new, check for migrations
			                if (!newDb)
			                {
			                    var dbSchemaVersion = await GetDbSchemaVersionAsync();
			                    if (ContextSchemaVersion != dbSchemaVersion)
			                    {
			                        CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", true);
			                        await MigrateDbAsync(dbSchemaVersion, ContextSchemaVersion);
			                        await UpdateDbSchemaVersionAsync(ContextSchemaVersion);
			                        CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", false);
			                    }
			                }
			                _finishedInitializingSqlLiteContext.Add(GetType());
			                return newDb;
			
			            }
			            catch (Exception)
			            {
			                _startedInitializingSqlLiteContext.Remove(GetType());
			                throw;
			            }
			        }
			        public async Task<bool> CreateDatabaseIfNotExistsAsync()
			        {
			            if (File.Exists(DatabaseFilePath)) return false;
			            SqliteConnection.CreateFile(DatabaseFilePath);
			            try
			            {
			                using (var connection = new SqliteConnection("Data Source=" + DatabaseFilePath))
			                {
			                    await connection.OpenAsync();
			                    using (var c = connection.CreateCommand())
			                    {
			                        c.CommandText =
			                            string.Format(
			                            @"CREATE TABLE [{0}] (
			                          ModelTypeLogicalName text NOT NULL,
			                          ModelId integer NOT NULL,
			                          Json text NOT NULL,
			                          BroughtFromMasterDbOnUtcUtcTicks integer,
			                          Index1 text,
			                          Index2 text,
			                          Index3 text,
			                          Index4 text,
			                          Index5 text
			                        );
			
			                        CREATE UNIQUE INDEX UIX_Data_Data_ModelId ON Data (ModelTypeLogicalName ASC, ModelId ASC);
			                        CREATE INDEX IX_Data_BroughtFromMasterDbOnUtcTicks_ModelTypeLogicalName ON Data (BroughtFromMasterDbOnUtcTicks ASC, ModelTypeLogicalName ASC);
			                        CREATE INDEX IX_Data_Index1 ON Data (ModelTypeLogicalName ASC, Index1 ASC);
			                        CREATE INDEX IX_Data_Index2 ON Data (ModelTypeLogicalName ASC, Index2 ASC);
			                        CREATE INDEX IX_Data_Index3 ON Data (ModelTypeLogicalName ASC, Index3 ASC);
			                        CREATE INDEX IX_Data_Index4 ON Data (ModelTypeLogicalName ASC, Index4 ASC);
			                        CREATE INDEX IX_Data_Index5 ON Data (ModelTypeLogicalName ASC, Index5 ASC);
			
			                        INSERT INTO [{0}] (ModelTypeLogicalName, ModelId, Json, BroughtFromMasterDbOnUtcTicks, Index1, Index2, Index3, Index4, Index5) 
			                        VALUES ('{1}', {2}, '{3}', {4}, {5}, {6}, {7}, {8}, {9})",
			
			                            DataTableName,
			                            SchemaVersionModelType,
			                            0,
			                            ContextSchemaVersion,
			                            DateTime.Now.Ticks,
			                            "NULL",
			                            "NULL",
			                            "NULL",
			                            "NULL",
			                            "NULL");
			                        await c.ExecuteNonQueryAsync();
			                    }
			                }
			                return true;
			            }
			            catch (Exception)
			            {
			                //if we failed to set up schema, wait 2 seconds and then delete the file alltogether, so we can start over
			                Thread.Sleep(2000);
			                File.Delete(DatabaseFilePath);
			                throw;
			            }
			        }
					public virtual Task MigrateDbAsync(int fromVersion, int toVersion)
					{
						throw new SupermodelException("Must override MigrateDbAsync");
					}
			        #endregion
			
			        #region Properties and Contants
			        public string SchemaVersionModelType { get { return DataTableName + ".LocalDb.SchemaVersion"; } } 
			        public virtual string DbFileName { get { return "Supermodel.Mobile.db3"; } }
			        public virtual string DataTableName { get { return "Data"; } }
			        public string DatabaseFilePath
			        {
			            get
			            {
			                // ReSharper disable once RedundantAssignment
			                string libraryPath = null;
			                //Device.OnPlatform(
			                //    iOS: () =>
			                //    {
			                //        // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
			                //        // (they don't want non-user-generated data in Documents)
			                //        var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
			                //        libraryPath = Path.Combine(documentsPath, "../Library/"); // Library folder
			                //    }, 
			                //    Android: () =>
			                //    {
			                //        // Just use whatever directory SpecialFolder.Personal returns
			                //        libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			                //    }
			                //);
			
			                #if __IOS__
			                // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
			                // (they don't want non-user-generated data in Documents)
			                var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
			                libraryPath = Path.Combine(documentsPath, "../Library/"); // Library folder
			                #endif
			
			                #if __ANDROID__
			                // Just use whatever directory SpecialFolder.Personal returns
			                libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			                #endif
			
			                // ReSharper disable once AssignNullToNotNullAttribute
			                var path = Path.Combine(libraryPath, DbFileName);
			
			                return path;
			            }
			        }
			
			        private static readonly HashSet<Type> _finishedInitializingSqlLiteContext = new HashSet<Type>();
			        private static readonly HashSet<Type> _startedInitializingSqlLiteContext = new HashSet<Type>();
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region WebApi
			
			#region IQueryStringProvider
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
			    public interface IQueryStringProvider
			    {
			        string GetQueryString(object searchBy, int? skip, int? take, string sortBy);
			    }
			}
			#endregion
			
			#region SearchByHelper
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
			    using System.Text;
			    using System.Web;
			    
			    public static class SearchByHelper
			    {
			        public static string ToQueryString<T>(this T me)
			        {
			            var sb = new StringBuilder();
			
			            var firstColumn = true;
			            foreach (var property in me.GetType().GetProperties())
			            {
			                if (firstColumn) firstColumn = false;
			                else sb.Append("&");
			
			                var propertyObj = me.GetType().GetProperty(property.Name).GetValue(me);
			                var propertyValue = "";
			                if (propertyObj != null) propertyValue = propertyObj.ToString();
			
			                sb.Append(string.Format("{0}={1}", property.Name, HttpUtility.UrlEncode(propertyValue)));
			            }
			            return sb.ToString();
			        }
			    }
			}
			#endregion
			
			#region WebApiDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
				using System;
				using System.Collections.Generic;
				using System.Linq;
				using System.Net;
				using System.Net.Http;
				using System.Net.Http.Headers;
				using System.Text;
				using System.Threading.Tasks;
				using Newtonsoft.Json;
				using Core;
				using Exceptions;
				using UnitOfWork;
				using Models;
			    using Encryptor;
				#if __ANDROID__ || __IOS__
				using ModernHttpClient;
				using Multipart;
				using MultipartContent = Multipart.MultipartContent;
			    #endif
			
			    public abstract class WebApiDataContext : DataContextBase, IQueryStringProvider, IWebApiAuthorizationContext
			    {
			        #region Contructors
			        protected WebApiDataContext(AuthHeader authHeader = null)
			        {
			            AuthHeader = authHeader;
			        }
			        #endregion
			
			        #region IQueryStringProvider implemetation
			        public string GetQueryString(object searchBy, int? skip, int? take, string sortBy)
			        {
			            var qs = new StringBuilder();
			            qs.Append(searchBy.ToQueryString());
			            if (sortBy != null) qs.Append("&smSortBy=" + sortBy);
			            if (skip != null) qs.Append("&smSkip=" + skip);
			            if (take != null) qs.Append("&smTake=" + take);
			            return qs.ToString();
			        }
			        #endregion
			
			        #region ValidateLogin
			        public virtual async Task<bool> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
			        {
			            using (var httpClient = CreateHttpClient())
						{
			                var url = string.Format("{0}/ValidateLogin", GetModelTypeLogicalName<ModelT>());
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
						    var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
						    if (dataResponse.IsSuccessStatusCode) return true;
			                if (dataResponse.StatusCode == HttpStatusCode.Unauthorized) return false;
			                throw new SupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
						}
			        }
			        #endregion
			
			        #region DataContext Reads
			        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(int id)
			        {
			            var url = string.Format("{0}/{1}", GetModelTypeLogicalName<ModelT>(), id);
			            return await GetJsonObjectAsync<ModelT>(url);
			        }
			        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            var url = string.Format("{0}/All?", GetModelTypeLogicalName<ModelT>());
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetJsonObjectsAsync<ModelT>(url);
			        }
			        public override async Task<int> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            var url = string.Format("{0}/CountAll?", GetModelTypeLogicalName<ModelT>());
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetCountAsync(url);
			        }
			        #endregion
			
			        #region DataContext Queries
			        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
			        {
			            var url = string.Format("{0}/Where?{1}", GetModelTypeLogicalName<ModelT>(), searchBy.ToQueryString());
			            if (sortBy != null) url += "&smSortBy=" + sortBy;
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetJsonObjectsAsync<ModelT>(url);
			        }
			        public override async Task<int> GetCountWhereAsync<ModelT>(object searchBy)
			        {
			            var url = string.Format("{0}/CountWhere?{1}", GetModelTypeLogicalName<ModelT>(), searchBy.ToQueryString());
			            return await GetCountAsync(url);
			        }
			        #endregion
			
			        #region DataContext Save Changes
			        public override async Task SaveChnagesInternalAsync(List<PendingAction> pendingActions)
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                HttpRequestMessage request;
			                if (pendingActions.Count == 1)
			                {
			                    request = pendingActions.Single().GenerateHttpRequest(BaseUrl, this);
			                }
			                else
			                {
			                    request = new HttpRequestMessage(HttpMethod.Post, BaseUrl + "$batch") { Content = new MultipartContent("mixed") };
			                    foreach (var pendingAction in pendingActions) ((MultipartContent)request.Content).Add(new HttpMessageContent(pendingAction.GenerateHttpRequest(BaseUrl, this)));
			                }
			
			                var response = await httpClient.SendAsyncAndPreserveContext(request);
			                var responseContentStr = response.Content != null ? await response.Content.ReadAsStringAsync() : "";
			                if (!response.IsSuccessStatusCode)
			                {
			                    //this is a special case for when an object does not exists and it's ok
			                    if (response.StatusCode == HttpStatusCode.NotFound && pendingActions.Count == 1 && pendingActions.Single().Operation == PendingAction.OperationEnum.DelayedGetByIdOrDefault)
			                    {
			                        //by this we indicate that this is not an transport error - it's an indication that object with id does not exists
			                        responseContentStr = null;
			                    }
			                    else if (response.StatusCode == HttpStatusCode.ExpectationFailed) //If validation error(s)
			                    {
			                        var validationError = JsonConvert.DeserializeObject<SupermodelValidationException.ValidationError>(responseContentStr);
			                        validationError.FailedAction = pendingActions.Single();
			                        ThrowSupermodelValidationException(validationError);
			                    }
			                    else
			                    {
			                        ThrowSupermodelWebApiException(response.StatusCode, responseContentStr);
			                    }
			                }
			
			                if (pendingActions.Count == 1)
			                {
			                    pendingActions.Single().ProcessHttpResponse(responseContentStr);
			                }
			                else
			                {
			                    var streamProvider = await response.Content.ReadAsMultipartAsync();
			                    if (pendingActions.Count != streamProvider.Contents.Count) throw new SupermodelException("Response does not match the request");
			                    var validationErrors = new List<SupermodelValidationException.ValidationError>();
			                    for (var i = 0; i < pendingActions.Count; i++)
			                    {
			                        var dataResponse = await streamProvider.Contents[i].ReadAsHttpResponseMessageAsync();
			                        var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync() : "";
			                        if (!dataResponse.IsSuccessStatusCode)
			                        {
			                            //this is a special case for when an object does not exists and it's ok
			                            if (dataResponse.StatusCode == HttpStatusCode.NotFound && pendingActions[i].Operation == PendingAction.OperationEnum.DelayedGetByIdOrDefault)
			                            {
			                                //by this we indicate that this is not an transport error - it's an indication that object with id does not exists
			                                pendingActions[i].ProcessHttpResponse(null);
			                            }
			                            else if (dataResponse.StatusCode == HttpStatusCode.ExpectationFailed) //If validation error(s)
			                            {
			                                var validationError = JsonConvert.DeserializeObject<SupermodelValidationException.ValidationError>(dataResponseContentStr);
			                                validationErrors.Add(validationError);
			                            }
			                            else
			                            {
			                                ThrowSupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
			                            }
			                        }
			                        else
			                        {
			                            pendingActions[i].ProcessHttpResponse(dataResponseContentStr);
			                        }
			                    }
			                    if (validationErrors.Any()) ThrowSupermodelValidationException(validationErrors);
			                }
			            }
			        }
			        #endregion
			
			        #region Private Helpers
			        protected HttpClient CreateHttpClient()
			        {
			            #if __ANDROID__ || __IOS__
			            var httpClient = new HttpClient(new NativeMessageHandler()) { BaseAddress = new Uri(BaseUrl) };
			            #else
			            var httpClient = new HttpClient() { BaseAddress = new Uri(BaseUrl) };
			            #endif
			
						httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(AcceptHeader));
			            if (AuthHeader != null) httpClient.DefaultRequestHeaders.Add(AuthHeader.HeaderName, AuthHeader.AuthToken);
			            return httpClient;
			        }
					protected void ThrowSupermodelWebApiException(HttpStatusCode statusCode, string content)
					{
						PrepareForThrowingException();
						throw new SupermodelWebApiException(statusCode, content);
					}
			        protected async Task<List<ModelT>> GetJsonObjectsAsync<ModelT>(string url) where ModelT : class, IModel, new()
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
							var responseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
							if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException(dataResponse.StatusCode, responseContentStr);
			                var models = JsonConvert.DeserializeObject<List<ModelT>>(responseContentStr);
			                foreach (var model in models)
			                {
			                    model.BroughtFromMasterDbOn = DateTime.Now;
			                    ManagedModels.Add(new ManagedModel(model));
			                }
			                return models;
			            }
			        }
			        protected async Task<ModelT> GetJsonObjectAsync<ModelT>(string url) where ModelT : class, IModel, new()
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
			                if (dataResponse.StatusCode == HttpStatusCode.NotFound) return null;
							var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
							if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException (dataResponse.StatusCode, dataResponseContentStr);
			                var model = JsonConvert.DeserializeObject<ModelT>(dataResponseContentStr);
			                model.BroughtFromMasterDbOn = DateTime.Now;
			                ManagedModels.Add(new ManagedModel(model));
			                return model;
			            }
			        }
			        protected async Task<int> GetCountAsync(string url)
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
							var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
							if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
			                var count = JsonConvert.DeserializeObject<int>(dataResponseContentStr);
			                return count;
			            }
			        }
			        protected async Task<ScalarValueT> GetScalarValueAsync<ScalarValueT>(string url)
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
							var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
							if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
			                var count = JsonConvert.DeserializeObject<ScalarValueT>(dataResponseContentStr);
			                return count;
			            }
			        }
			        #endregion
			
			        #region Properties & Constants
			        public const string AcceptHeader = "application/json";
			        public AuthHeader AuthHeader { get; set; }
			        public abstract string BaseUrl { get; }
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
	#endregion
	
	#region Encryptor
		
		#region AuthHeader
		
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    public class AuthHeader
		    {
		        #region Constructors
		        public AuthHeader(string headerName, string authToken)
		        {
		            HeaderName = headerName;
		            AuthToken = authToken;
		        }
		        public AuthHeader(string authToken) : this("Authorization", authToken){}
		        #endregion
		
		        #region Properties
		        public string HeaderName { get; set; }
		        public string AuthToken { get; set; }
		        #endregion
		    }
		}
		#endregion
		
		#region Converter
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.Text;
		
		    public static class Converter
		    {
		        public static byte[] StringToByteArr(string str)
		        {
		            return Encoding.Unicode.GetBytes(str);
		        }
		        public static string ByteArrToString(byte[] bytes)
		        {
		            return Encoding.Unicode.GetString(bytes);
		        }
		        
		        public static string ByteArrToBase64String(byte[] bytes)
		        {
		            return Convert.ToBase64String(bytes);
		        }
		        public static byte[] Base64StringToByteArr(string str)
		        {
		            return Convert.FromBase64String(str);
		        }
		        
		        public static string BinaryToHex(byte[] data)
		        {
		            if (data == null) return null;
		            var hex = new char[checked(data.Length * 2)];
		            for (var i = 0; i < data.Length; i++)
		            {
		                var thisByte = data[i];
		                hex[2 * i] = NibbleToHex((byte)(thisByte >> 4));        // high nibble
		                hex[2 * i + 1] = NibbleToHex((byte)(thisByte & 0xf));   // low nibble
		            }
		            return new string(hex);
		        }
		        public static char NibbleToHex(byte nibble)
		        {
		            return (char)((nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A'));
		        }
		    }
		}
		#endregion
		
		#region EncryptorAgent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System.Collections;
		    using System.IO;
		    using System.Security.Cryptography;
		    using System.Text;
		    
		    public static class EncryptorAgent
		    {
		        //Sample key
		        //private readonly static byte[] _key = { 0xA6, 0x46, 0x10, 0xF1, 0xEA, 0x16, 0x51, 0xA0, 0xB2, 0x41, 0x27, 0x5C, 0x23, 0x9C, 0xF0, 0xDD };
		
		        public static byte[] Lock(byte[] key, string str, out byte[] iv)
		        {
		            var aes = new RijndaelManaged();
		            aes.GenerateIV();
		            iv = aes.IV;
		            var memstrm = new MemoryStream();
		            var csw = new CryptoStream(memstrm, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write);
		            csw.Write(Encoding.ASCII.GetBytes(str), 0, str.Length); //This breaks old code
		            csw.FlushFinalBlock();
		            var cryptdata = memstrm.ToArray();
		            csw.Close();
		            memstrm.Close();
		            return cryptdata;
		        }
		        public static byte[] LockUnicode(byte[] key, string str, out byte[] iv)
		        {
		            var aes = new RijndaelManaged();
		            aes.GenerateIV();
		            iv = aes.IV;
		            var memstrm = new MemoryStream();
		            var csw = new CryptoStream(memstrm, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write);
		            csw.Write(Converter.StringToByteArr(str), 0, str.Length);
		            csw.FlushFinalBlock();
		            var cryptdata = memstrm.ToArray();
		            csw.Close();
		            memstrm.Close();
		            return cryptdata;
		        }
		        
		        public static string Unlock(byte[] key, byte[] code, byte[] iv)
		        {
		            var aes = new RijndaelManaged();
		            var memstrm = new MemoryStream(code) {Position = 0};
		
		            var csr = new CryptoStream(memstrm, aes.CreateDecryptor(key, iv), CryptoStreamMode.Read);
		
		            var dataFragments = new ArrayList();
		            var recv = 0;
		            while (true)
		            {
		                var dataFragment = new byte[1024];
		                var recvdThisFragment = csr.Read(dataFragment, 0, dataFragment.Length);
		                if (recvdThisFragment == 0) break;
		                dataFragments.Add(dataFragment);
		                recv = recv + recvdThisFragment;
		            }
		
		            var data = new byte[dataFragments.Count * 1024];
		            var idx = 0;
		
		            //now let's build the entire data
		            foreach (var t in dataFragments)
		            {
		                for (var j = 0; j < 1024; j++) data[idx++] = ((byte[])t)[j];
		            }
		
		            var newphrase = Encoding.ASCII.GetString(data, 0, recv);
		            csr.Close();
		            memstrm.Close();
		            return newphrase;
		        }
		        public static string UnlockUnicode(byte[] key, byte[] code, byte[] iv)
		        {
		            var aes = new RijndaelManaged();
		            var memstrm = new MemoryStream(code) {Position = 0};
		
		            var csr = new CryptoStream(memstrm, aes.CreateDecryptor(key, iv), CryptoStreamMode.Read);
		
		            var dataFragments = new ArrayList();
		            var recv = 0;
		            while (true)
		            {
		                var dataFragment = new byte[1024];
		                var recvdThisFragment = csr.Read(dataFragment, 0, dataFragment.Length);
		                if (recvdThisFragment == 0) break;
		                dataFragments.Add(dataFragment);
		                recv = recv + recvdThisFragment;
		            }
		
		            var data = new byte[dataFragments.Count * 1024];
		            var idx = 0;
		
		            //now let's build the entire data
		            foreach (var t in dataFragments)
		            {
		                for (var j = 0; j < 1024; j++) data[idx++] = ((byte[])t)[j];
		            }
		
		            var newphrase = Encoding.Unicode.GetString(data, 0, recv);
		            csr.Close();
		            memstrm.Close();
		            return newphrase;
		        }
		    }
		}
		#endregion
		
		#region HashAgent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.Security.Cryptography;
		    using System.Text;
		
		    public static class HashAgent
		    {
		        public static string HashByteArray(byte[] data)
		        {
		            #if __MOBILE__
		                using (var sha1 = new SHA1CryptoServiceProvider())
		            #else
		                using (var sha1 = new SHA1Cng())
		            #endif
		            {
		                return Convert.ToBase64String(sha1.ComputeHash(data));
		            }
		        }
		
		        public static string GenerateGuidSalt()
		        {
		            return Guid.NewGuid().ToString();
		        }
		        public static byte[] GenerateBinaryGuidSalt()
		        {
		            return Converter.StringToByteArr(GenerateGuidSalt());
		        }
		        public static string Generate5MinTimeStampSalt(DateTime dt)
		        {
		            return String.Format("{0:|yyyy|M|d|H|m}", RoundUp5Min(dt));
		        }
		
		        public static string HashPasswordSHA1(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new SHA1CryptoServiceProvider());
		            #else
		                return HashPassword(password, salt, new SHA1Cng());
		            #endif
		        }
		        public static string HashPasswordSHA1Unicode(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new SHA1CryptoServiceProvider());
		            #else
		                return HashPasswordUnicode(password, salt, new SHA1Cng());
		            #endif
		        }
		        public static byte[] HashPasswordSHA1(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new SHA1CryptoServiceProvider());
		            #else
		                return HashPassword(password, salt, new SHA1Cng());
		            #endif
		        }
		        public static byte[] HashPasswordSHA1Unicode(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new SHA1CryptoServiceProvider());
		            #else
		                return HashPasswordUnicode(password, salt, new SHA1Cng());
		            #endif
		        }
		        
		        public static string HashPasswordMD5(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new MD5CryptoServiceProvider());
		            #else
		                return HashPassword(password, salt, new MD5Cng());
		            #endif
		        }
		        public static string HashPasswordMD5Unicode(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new MD5CryptoServiceProvider());
		            #else
		                return HashPasswordUnicode(password, salt, new MD5Cng());
		            #endif
		        }
		        public static byte[] HashPasswordMD5(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new MD5CryptoServiceProvider());
		            #else
		                return HashPassword(password, salt, new MD5Cng());
		            #endif
		        }
		        public static byte[] HashPasswordMD5Unicode(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new MD5CryptoServiceProvider());
		            #else
		                return HashPasswordUnicode(password, salt, new MD5Cng());
		            #endif
		        }
		        
		        public static string HashPasswordSHA256(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new SHA256Managed());
		            #else
		                return HashPassword(password, salt, new SHA256Cng());
		            #endif
		        }
		        public static string HashPasswordSHA256Unicode(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new SHA256Managed());
		            #else
		                return HashPasswordUnicode(password, salt, new SHA256Cng());
		            #endif
		        }
		        public static byte[] HashPasswordSHA256(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new SHA256Managed());
		            #else
		                return HashPassword(password, salt, new SHA256Cng());
		            #endif
		        }
		        public static byte[] HashPasswordSHA256Unicode(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new SHA256Managed());
		            #else
		                return HashPasswordUnicode(password, salt, new SHA256Cng());
		            #endif
		        }
		
		        public static string HashPasswordSHA512(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new SHA512Managed());
		            #else
		                return HashPassword(password, salt, new SHA512Cng());
		            #endif
		        }
		        public static string HashPasswordSHA512Unicode(string password, string salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new SHA512Managed());
		            #else
		                return HashPasswordUnicode(password, salt, new SHA512Cng());
		            #endif
		        }
		        public static byte[] HashPasswordSHA512(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPassword(password, salt, new SHA512Managed());
		            #else
		                return HashPassword(password, salt, new SHA512Cng());
		            #endif
		        }
		        public static byte[] HashPasswordSHA512Unicode(string password, byte[] salt)
		        {
		            #if __MOBILE__
		                return HashPasswordUnicode(password, salt, new SHA512Managed());
		            #else
		                return HashPasswordUnicode(password, salt, new SHA512Cng());
		            #endif
		        }
		
		        private static string HashPassword(string password, string salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException("password");
		            if (salt == null) throw new ArgumentNullException("salt");
		
		            using (hashAlgorithm)
		            {
		                return Converter.BinaryToHex(hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(password + salt)));  //This breaks old code
		            }
		        }
		        private static string HashPasswordUnicode(string password, string salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException("password");
		            if (salt == null) throw new ArgumentNullException("salt");
		
		            using (hashAlgorithm)
		            {
		                return Converter.BinaryToHex(hashAlgorithm.ComputeHash(Converter.StringToByteArr(password + salt)));
		            }
		        }
		        private static byte[] HashPassword(string password, byte[] salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException("password");
		            if (salt == null) throw new ArgumentNullException("salt");
		
		            using (hashAlgorithm)
		            {
		                return hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(password + salt));
		            }
		        }
		        private static byte[] HashPasswordUnicode(string password, byte[] salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException("password");
		            if (salt == null) throw new ArgumentNullException("salt");
		
		            using (hashAlgorithm)
		            {
		                return hashAlgorithm.ComputeHash(Converter.StringToByteArr(password + Converter.BinaryToHex(salt)));
		            }
		        }
		        
		        private static DateTime RoundUp5Min(DateTime dt)
		        {
		            var d = TimeSpan.FromMinutes(5);
		            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
		        }
		    }
		}
		#endregion
		
		#region HashExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.IO;
		    using System.Runtime.Serialization;
		    using System.Security.Cryptography;
		    
		    public static class HashExtensions
		    {
		        public static string GetHash<T>(this object instance) where T : HashAlgorithm, new()
		        {
		            T cryptoServiceProvider = new T();
		            return ComputeHash(instance, cryptoServiceProvider);
		        }
		        public static string GetKeyedHash<T>(this object instance, byte[] key) where T : KeyedHashAlgorithm, new()
		        {
		            T cryptoServiceProvider = new T { Key = key };
		            return ComputeHash(instance, cryptoServiceProvider);
		        }
		        public static string GetMD5Hash(this object instance)
		        {
		            #if __MOBILE__
		                return instance.GetHash<MD5CryptoServiceProvider>();
		            #else
		                return instance.GetHash<MD5Cng>();
		            #endif
		        }
		        public static string GetSHA1Hash(this object instance)
		        {
		            #if __MOBILE__
		                return instance.GetHash<SHA1CryptoServiceProvider>();
		            #else
		                return instance.GetHash<SHA1Cng>();
		            #endif
		        }        
		        public static string GetSHA256Hash(this object instance)
		        {
		            #if __MOBILE__
		                return instance.GetHash<SHA256Managed>();
		            #else
		                return instance.GetHash<SHA256Cng>();
		            #endif
		        }        
		        public static string GetSHA512Hash(this object instance)
		        {
		            #if __MOBILE__
		                return instance.GetHash<SHA512Managed>();
		            #else
		                return instance.GetHash<SHA512Cng>();
		            #endif
		        }        
		        private static string ComputeHash<T>(object instance, T cryptoServiceProvider) where T : HashAlgorithm, new()
		        {
		            var serializer = new DataContractSerializer(instance.GetType());
		            using (var memoryStream = new MemoryStream())
		            {
		                serializer.WriteObject(memoryStream, instance);
		                cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
		                return Convert.ToBase64String(cryptoServiceProvider.Hash);
		            }
		        }
		    }
		}
		#endregion
		
		#region HttpAuthAgent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.Text;
		
			public static class HttpAuthAgent
		    {
		        #region Header Creation Methods
		        //example Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
		        public static AuthHeader CreateBasicAuthHeader(string username, string password)
		        {
		            return new AuthHeader("Basic " + CreateBasicAuthTokenCredentials(username, password));
		        }
		        public static AuthHeader CreateSMCustomEncryptedAuthHeader(byte[] key, params string[] args)
		        {
		            return new AuthHeader("SMCustomEncrypted " + CreateSMCustomEncryptedAuthTokenCredentials(key, args));
		        }
		
				public static string CreateBasicAuthToken(string username, string password)
				{
		            return "Basic " + CreateBasicAuthTokenCredentials(username, password);
				}
				public static string CreateSMCustomEncryptedAuthToken(byte[] key = null, params string[] args)
				{
		            return "SMCustomEncrypted " + CreateSMCustomEncryptedAuthTokenCredentials(key, args);
				}
		        #endregion
		
		        #region Header Read Methods
				public static void ReadBasicAuthToken(string authorizeHeader, out string username, out string password)
				{
				    if (authorizeHeader == null) throw new ArgumentNullException("authorizeHeader");
		            if (!authorizeHeader.StartsWith("Basic ")) throw new ArgumentException("'Basic' authorization scheme is expected");
				    
				    var credentials = authorizeHeader.Replace("Basic ", "");
		            ReadBasicAuthTokenCredentials(credentials, out username, out password);
				}
		        public static void ReadSMCustomEncryptedAuthToken(byte[] key, string authorizeHeader, out string[] args)
				{
				    if (authorizeHeader == null) throw new ArgumentNullException("authorizeHeader");
				    if (!authorizeHeader.StartsWith("SMCustomEncrypted ")) throw new ArgumentException("'SMCustomEncrypted' authorization scheme is expected");
		
		            var encryptedCredentials = authorizeHeader.Replace("SMCustomEncrypted ", "");
		            ReadSMCustomEncryptedAuthTokenCredentials(key, encryptedCredentials, out args);
				}
		        #endregion
		
		        #region Lower-level credential methods
		        public static string CreateBasicAuthTokenCredentials(string username, string password)
		        {
		            var payloadStr = string.Join(":", username, password);
		    		if (username.Contains(":") || password.Contains(":")) throw new ArgumentException("Username and Password cannot contain ':' character for Basic non-encrypted auth");
		            return Convert.ToBase64String(Encoding.UTF8.GetBytes(payloadStr));
		        }
		        public static string CreateSMCustomEncryptedAuthTokenCredentials(byte[] key, params string[] args)
		        {
		            var payloadStrBldr = new StringBuilder();
		            var first = true;
		            foreach (var param in args)
		            {
		                if (first)
		                {
		                    first = false;
		                    payloadStrBldr.Append(Converter.ByteArrToBase64String(Converter.StringToByteArr(param)));
		                }
		                else
		                {
		                    payloadStrBldr.Append(":" + Converter.ByteArrToBase64String(Converter.StringToByteArr(param)));
		                }
		            }
		            byte[] payloadIV;
		            var payloadCode = EncryptorAgent.Lock(key, payloadStrBldr.ToString(), out payloadIV);
		            
		            return Convert.ToBase64String(payloadCode) + "|" + Convert.ToBase64String(payloadIV);
		        }
		        
		        public static void ReadBasicAuthTokenCredentials(string credentials, out string username, out string password)
		        {
		            var parts = Encoding.UTF8.GetString(Convert.FromBase64String(credentials)).Split(':');   
				    if (parts.Length != 2) throw new ArgumentException("Authorization header is badly formatted");
				    username = parts[0].Trim();
				    password = parts[1].Trim();
		        }
		        public static void ReadSMCustomEncryptedAuthTokenCredentials(byte[] key, string encryptedCredentials, out string[] args)
		        {
		            if (key == null) throw new ArgumentException("key is required for parsing an encrypted auth header");
		            if (string.IsNullOrWhiteSpace(encryptedCredentials)) throw new ArgumentException("encryptedCredentials is required for parsing an encrypted auth header");
		
				    var encryptionParts = encryptedCredentials.Split('|');
				    if (encryptionParts.Length != 2) throw new ArgumentException("Authorization header is badly formatted");
		
		            var code = encryptionParts[0];
		            var iv = encryptionParts[1];
		
		            var payloadStr = EncryptorAgent.Unlock(key, Convert.FromBase64String(code), Convert.FromBase64String(iv));
		
		            var argsStr = payloadStr.Split(':');
		            args = new string[argsStr.Length];
		            for (var i=0; i < argsStr.Length; i++) args[i] = Converter.ByteArrToString(Converter.Base64StringToByteArr(argsStr[i]));
		        }
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Exceptions
		
		#region SupermodelException
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Exceptions
		{
		    using System;
		    
		    public class SupermodelException : Exception
		    {
		        public SupermodelException() : this("N/A") { }
		        public SupermodelException(string msg) : base(/*ReflectionHelper.GetThrowingContext() + ": " + */msg) { }
		    }
		}
		#endregion
		
		#region SupermodelValidationException
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Exceptions
		{
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		    using System.Linq;
		    using Newtonsoft.Json;
		    using DataContext.Core;
		    
		    public class SupermodelValidationException : SupermodelException
		    {
		        #region Embedded Classes
		        public class ValidationError : List<ValidationError.Error>
		        {
		            #region Constructors
		            public ValidationError() { }
		            public ValidationError(IEnumerable<ValidationResult> validationResults, PendingAction failedAction, string message)
		            {
		                foreach (var validationResult in validationResults)
		                {
		                    foreach (var memberName in validationResult.MemberNames)
		                    {
		                        var existingError = this.SingleOrDefault(x => x.Name == memberName);
		                        if (existingError != null) existingError.ErrorMessages.Add(validationResult.ErrorMessage);
		                        else Add(new Error (memberName, validationResult.ErrorMessage) );
		                    }
		                }
		                FailedAction = failedAction;
		                Message = message;
		            }
		            #endregion
		
		            #region Embedded Classes
		            public class Error
		            {
		                #region Constructors
		                public Error(string name, List<string> errorMessages)
		                {
		                    Name = name;
		                    ErrorMessages = errorMessages;
		                }
		                public Error(string name, string errorMessage)
		                {
		                    Name = name;
		                    ErrorMessages = new List<string>{ errorMessage };
		                }
		                public Error()
		                {
		                    ErrorMessages = new List<string>();
		                }
		                #endregion
		
		                #region Properties
		                public string Name { get; set; }
		                public List<string> ErrorMessages { get; set; }
		                #endregion
		            }
		            #endregion
		
		            #region Properties
		            [JsonIgnore] public PendingAction FailedAction { get; set; }
		            public string Message { get; set; }
		            #endregion
		        }
		        #endregion
		
		        #region Constructors
		        public SupermodelValidationException(ValidationError validationError)
		        {
		            ValidationErrors = new List<ValidationError>{validationError};
		        }
		        public SupermodelValidationException(List<ValidationError> validationErrors)
		        {
		            ValidationErrors = validationErrors;
		        }
		        #endregion
		
		        #region Properties
		        public List<ValidationError> ValidationErrors { get; set; }
		        #endregion
		    }
		}
		#endregion
		
		#region SupermodelWebApiException
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Exceptions
		{
		    using System.Net;
		    
		    public class SupermodelWebApiException : SupermodelException
		    {
		        public SupermodelWebApiException(HttpStatusCode statusCode, string content) : base((int)statusCode + ":" + statusCode + ". Content: " + content){}
		
				public HttpStatusCode StatusCode { get; set; }
				public string Content { get; set; }
		    }
		}
		#endregion
		
	#endregion
	
	#region Misc
		
		#region DeviceInformation
		 // ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Misc
		{
		    #if __IOS__
		    using Foundation; 
		    using System.IO;
		    using System;
		    using UIKit;
		    using MonoTouch;
		    using Security;
		    #endif
		
		    #if __ANDROID__
		    using Java.Lang;
		    using Java.IO;
		    using Android.OS;
		    using Android.App;
		    #endif
		    
		    public static class DeviceInformation
		    {
				public static bool IsRunningOnEmulator()
		        {
		            var result = false;
		
		            #if __IOS__
		            if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR) result = true;
		            #endif
		
		            #if __ANDROID__
		            if (Build.Fingerprint != null && (Build.Fingerprint.Contains("vbox") || Build.Fingerprint.Contains("generic"))) result = true;
		            #endif
		
		            return result;
		        }
		        public static bool? IsJailbroken()
				{
		            if (IsRunningOnEmulator()) return null; //if we are not running on physical hardware return null (unknown)
		
		            // ReSharper disable once ConvertToConstant.Local
		            var result = false;
		
		            #if __IOS__
		            var paths = new [] {@"/Applications/Cydia.app", @"/Library/MobileSubstrate/MobileSubstrate.dylib", @"/bin/bash", @"/usr/sbin/sshd", @"/etc/apt" };
		            foreach (var path in paths)
		            {
		                if (NSFileManager.DefaultManager.FileExists(path)) result = true;
		            }
		
		            try
		            {
		                const string filename = @"/private/jailbreak.txt";
		                File.WriteAllText(filename, "This is a test.");
		                result = true;
		                File.Delete(filename);
		            }
		            // ReSharper disable once EmptyGeneralCatchClause
		            catch (Exception){} //if exception is thrown, we are not jailbroken
		
		            if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl("cydia://package/com.exapmle.package"))) result = true;
		            #endif
		
		            #if __ANDROID__
		            var buildTags = Android.OS.Build.Tags;
		            if (buildTags != null && buildTags.Contains("test-keys")) result = true;
		
		            if (new File("/system/app/Superuser.apk").Exists()) result = true;
		
		            var paths = new[] { "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su" };
		            foreach(var path in paths) 
		            {
		                if (new File(path).Exists()) result = true;
		            }
		
		            try
		            {
		                using (var process = Runtime.GetRuntime().Exec(new [] { "/system/xbin/which", "su" }))
		                {
		                    var br = new BufferedReader(new InputStreamReader(process.InputStream));
		                    if (br.ReadLine() != null) result = true;
		                }
		            }
		            catch{}
		            #endif
		
				    // ReSharper disable once ConditionIsAlwaysTrueOrFalse
		            return result;
				}
		        public static bool? IsDeviceSecuredByPassocde()
		        {
		            if (IsRunningOnEmulator()) return null; //if we are not running on physical hardware return null (unknown)
		
		            // ReSharper disable once ConvertToConstant.Local
		            var result = false;
		            
		            #if __IOS__
		            const string text = "Supermodel.Mobile Passcode Test";
		            var record = new SecRecord(SecKind.GenericPassword) { Generic = NSData.FromString ("foo"), Accessible = SecAccessible.WhenPasscodeSetThisDeviceOnly };
		            var status = SecKeyChain.Add(record);
		            if (status == SecStatusCode.Success || status == SecStatusCode.DuplicateItem)
		            {
		                result = true;
		                SecKeyChain.Remove(record);
		            }
		            #endif
		
		            #if __ANDROID__
		            var km = (KeyguardManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.KeyguardService);
				    if (km.IsKeyguardSecure) result = true;
		            #endif
		            
		            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
		            return result;
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region Models
		
		#region BinaryFile
		 // ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
			using System;
		    
		    public class BinaryFile
			{
				public String Name { get; set; }
				public Byte[] BinaryContent { get; set; }
			}
		}
		#endregion
		
		#region IModel
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    using System;
		    using System.ComponentModel.DataAnnotations;
		    
		    public interface IModel : IValidatableObject
		    {
		    	int Id { get; set; }
		        DateTime? BroughtFromMasterDbOn { get; set; }
		        
		        IModel PerpareForSerializingForMasterDb();
		        IModel PerpareForSerializingForLocalDb();
		
		        void Add();
		        void Delete();
		        void ForceUpdate();
		    }
		}
		
		#endregion
		
		#region Model
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    using System;
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		    using Newtonsoft.Json;
		    using Repository;
		    using Validation;
		    // ReSharper disable RedundantNameQualifier
		    using Supermodel.Mobile.ReflectionMapper;
		    // ReSharper restore RedundantNameQualifier
		   
		    public class Model : IModel
		    {
		        #region Methods
		        public virtual void Add()
		        {
		            CreateRepo().ExecuteMethod("Add", this);
		        }
		        public virtual void Delete()
		        {
		            CreateRepo().ExecuteMethod("Delete", Id);
		        }
		        public virtual void ForceUpdate()
		        {
		            CreateRepo().ExecuteMethod("ForceUpdate", this);
		        }
		        public virtual object CreateRepo()
		        {
		            return RepoFactory.CreateForRuntimeType(GetType());
		        }
		        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		        {
		            return new ValidationResultList();
		        }
		        #endregion
		
		        #region Properties
		        public int Id { get; set; }
		
		        [NotRMapped] public DateTime? BroughtFromMasterDbOn { get; set; }
		        public bool ShouldSerializeBroughtFromMasterDbOn() { return !SerializingForMasterDb; }
		        public IModel PerpareForSerializingForMasterDb() { SerializingForMasterDb = true; return this; }
		        public IModel PerpareForSerializingForLocalDb() { SerializingForMasterDb = false; return this; }
		        [JsonIgnore] protected bool SerializingForMasterDb { get; set; }
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Multipart
		
		#region FormattingUtilities
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Globalization;
		    using System.Linq;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using Newtonsoft.Json.Linq;
		    
		    public static class FormattingUtilities
		    {
		        // Supported date formats for input.
		        private static readonly string[] _dateFormats =
		        {
		            // "r", // RFC 1123, required output format but too strict for input
		            "ddd, d MMM yyyy H:m:s 'GMT'", // RFC 1123 (r, except it allows both 1 and 01 for date and time)
		            "ddd, d MMM yyyy H:m:s", // RFC 1123, no zone - assume GMT
		            "d MMM yyyy H:m:s 'GMT'", // RFC 1123, no day-of-week
		            "d MMM yyyy H:m:s", // RFC 1123, no day-of-week, no zone
		            "ddd, d MMM yy H:m:s 'GMT'", // RFC 1123, short year
		            "ddd, d MMM yy H:m:s", // RFC 1123, short year, no zone
		            "d MMM yy H:m:s 'GMT'", // RFC 1123, no day-of-week, short year
		            "d MMM yy H:m:s", // RFC 1123, no day-of-week, short year, no zone
		
		            "dddd, d'-'MMM'-'yy H:m:s 'GMT'", // RFC 850
		            "dddd, d'-'MMM'-'yy H:m:s", // RFC 850 no zone
		            "ddd MMM d H:m:s yyyy", // ANSI C's asctime() format
		
		            "ddd, d MMM yyyy H:m:s zzz", // RFC 5322
		            "ddd, d MMM yyyy H:m:s", // RFC 5322 no zone
		            "d MMM yyyy H:m:s zzz", // RFC 5322 no day-of-week
		            "d MMM yyyy H:m:s" // RFC 5322 no day-of-week, no zone
		        };
		
		        // Valid header token characters are within the range 0x20 < c < 0x7F excluding the following characters
		        private const string NonTokenChars = "()<>@,;:\\\"/[]?={}";
		
		        public const double Match = 1.0;
		        public const double NoMatch = 0.0;
		        public const int DefaultMaxDepth = 256;
		        public const int DefaultMinDepth = 1;
		        public const string HttpRequestedWithHeader = @"x-requested-with";
		        public const string HttpRequestedWithHeaderValue = @"XMLHttpRequest";
		        public const string HttpHostHeader = "Host";
		        public const string HttpVersionToken = "HTTP";
		        public static readonly Type HttpRequestMessageType = typeof(HttpRequestMessage);
		        public static readonly Type HttpResponseMessageType = typeof(HttpResponseMessage);
		        public static readonly Type HttpContentType = typeof(HttpContent);
		        //public static readonly Type DelegatingEnumerableGenericType = typeof(DelegatingEnumerable<>);
		        public static readonly Type EnumerableInterfaceGenericType = typeof(IEnumerable<>);
		        public static readonly Type QueryableInterfaceGenericType = typeof(IQueryable<>);
		        public static bool IsJTokenType(Type type)
		        {
		            return typeof(JToken).IsAssignableFrom(type);
		        }
		        public static HttpContentHeaders CreateEmptyContentHeaders()
		        {
		            HttpContent tempContent = null;
		            HttpContentHeaders contentHeaders;
		            try
		            {
		                tempContent = new StringContent(String.Empty);
		                contentHeaders = tempContent.Headers;
		                contentHeaders.Clear();
		            }
		            finally
		            {
		                // We can dispose the content without touching the headers
		                if (tempContent != null) tempContent.Dispose();
		            }
		
		            return contentHeaders;
		        }
		        public static string UnquoteToken(string token)
		        {
		            if (String.IsNullOrWhiteSpace(token)) return token;
		            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1) return token.Substring(1, token.Length - 2);
		            return token;
		        }
		
		        public static bool ValidateHeaderToken(string token)
		        {
		            if (token == null) return false;
		            foreach (var c in token)
		            {
		                if (c < 0x21 || c > 0x7E || NonTokenChars.IndexOf(c) != -1) return false;
		            }
		            return true;
		        }
		
		        public static string DateToString(DateTimeOffset dateTime)
		        {
		            // Format according to RFC1123; 'r' uses invariant info (DateTimeFormatInfo.InvariantInfo)
		            return dateTime.ToUniversalTime().ToString("r", CultureInfo.InvariantCulture);
		        }
		
		        public static bool TryParseDate(string input, out DateTimeOffset result)
		        {
		            return DateTimeOffset.TryParseExact(input, _dateFormats, DateTimeFormatInfo.InvariantInfo,
		                                                DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeUniversal,
		                                                out result);
		        }
		
		        public static bool TryParseInt32(string value, out int result)
		        {
		            return Int32.TryParse(value, NumberStyles.None, NumberFormatInfo.InvariantInfo, out result);
		        }    
		    }
		}
		#endregion
		
		#region HttpContentMessageExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.Globalization;
		    using System.IO;
		    using System.Linq;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class HttpContentMessageExtensions
		    {
		        private const int MinBufferSize = 256;
		        private const int DefaultBufferSize = 32 * 1024;
		
		        public static bool IsHttpRequestMessageContent(this HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		
		            try
		            {
		                return HttpMessageContent.ValidateHttpMessageContent(content, true, false);
		            }
		            catch (Exception)
		            {
		                return false;
		            }
		        }
		
		        public static bool IsHttpResponseMessageContent(this HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            try
		            {
		                return HttpMessageContent.ValidateHttpMessageContent(content, false, false);
		            }
		            catch (Exception)
		            {
		                return false;
		            }
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content)
		        {
		            return ReadAsHttpRequestMessageAsync(content, "http", DefaultBufferSize);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpRequestMessageAsync(content, "http", DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, DefaultBufferSize);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, int bufferSize, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, bufferSize, HttpRequestHeaderParser.DefaultMaxHeaderSize, cancellationToken);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, int bufferSize, int maxHeaderSize = HttpRequestHeaderParser.DefaultMaxHeaderSize)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, bufferSize, maxHeaderSize, CancellationToken.None);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            if (uriScheme == null) throw new ArgumentNullException("uriScheme");
		            if (!Uri.CheckSchemeName(uriScheme)) throw new ArgumentException("HttpMessageParserInvalidUriScheme", "uriScheme");
		            if (bufferSize < MinBufferSize) throw new ArgumentOutOfRangeException("bufferSize");
		            if (maxHeaderSize < InternetMessageFormatHeaderParser.MinHeaderSize) throw new ArgumentOutOfRangeException("maxHeaderSize");
		
		            HttpMessageContent.ValidateHttpMessageContent(content, true, true);
		
		            return content.ReadAsHttpRequestMessageAsyncCore(uriScheme, bufferSize, maxHeaderSize, cancellationToken);
		        }
		
		        private static async Task<HttpRequestMessage> ReadAsHttpRequestMessageAsyncCore(this HttpContent content, string uriScheme, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            cancellationToken.ThrowIfCancellationRequested();
		            Stream stream = await content.ReadAsStreamAsync();
		
		            var httpRequest = new HttpUnsortedRequest();
		            var parser = new HttpRequestHeaderParser(httpRequest, HttpRequestHeaderParser.DefaultMaxRequestLineSize, maxHeaderSize);
		
		            var buffer = new byte[bufferSize];
		            var headerConsumed = 0;
		
		            while (true)
		            {
		                int bytesRead;
		                try
		                {
		                    bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length, cancellationToken);
		                }
		                catch (Exception e)
		                {
		                    throw new IOException("HttpMessageErrorReading", e);
		                }
		
		                ParserState parseStatus;
		                try
		                {
		                    parseStatus = parser.ParseBuffer(buffer, bytesRead, ref headerConsumed);
		                }
		                catch (Exception)
		                {
		                    parseStatus = ParserState.Invalid;
		                }
		
		                if (parseStatus == ParserState.Done) return CreateHttpRequestMessage(uriScheme, httpRequest, stream, bytesRead - headerConsumed);
		                if (parseStatus != ParserState.NeedMoreData) throw new InvalidOperationException("HttpMessageParserError");
		                if (bytesRead == 0) throw new IOException("ReadAsHttpMessageUnexpectedTermination");
		            }
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content)
		        {
		            return ReadAsHttpResponseMessageAsync(content, DefaultBufferSize);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpResponseMessageAsync(content, DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize)
		        {
		            return ReadAsHttpResponseMessageAsync(content, bufferSize, HttpResponseHeaderParser.DefaultMaxHeaderSize);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize,
		            CancellationToken cancellationToken)
		        {
		            return ReadAsHttpResponseMessageAsync(content, bufferSize, HttpResponseHeaderParser.DefaultMaxHeaderSize, cancellationToken);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize, int maxHeaderSize)
		        {
		            return ReadAsHttpResponseMessageAsync(content, bufferSize, maxHeaderSize, CancellationToken.None);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            if (bufferSize < MinBufferSize) throw new ArgumentOutOfRangeException("bufferSize");
		            if (maxHeaderSize < InternetMessageFormatHeaderParser.MinHeaderSize) throw new ArgumentOutOfRangeException("maxHeaderSize");
		            HttpMessageContent.ValidateHttpMessageContent(content, false, true);
		
		            return content.ReadAsHttpResponseMessageAsyncCore(bufferSize, maxHeaderSize, cancellationToken);
		        }
		
		        private static async Task<HttpResponseMessage> ReadAsHttpResponseMessageAsyncCore(this HttpContent content, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            cancellationToken.ThrowIfCancellationRequested();
		            Stream stream = await content.ReadAsStreamAsync();
		
		            var httpResponse = new HttpUnsortedResponse();
		            var parser = new HttpResponseHeaderParser(httpResponse, HttpResponseHeaderParser.DefaultMaxStatusLineSize, maxHeaderSize);
		            ParserState parseStatus;
		
		            var buffer = new byte[bufferSize];
		            int bytesRead;
		            var headerConsumed = 0;
		
		            while (true)
		            {
		                try
		                {
		                    bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length, cancellationToken);
		                }
		                catch (Exception e)
		                {
		                    throw new IOException("HttpMessageErrorReading", e);
		                }
		
		                try
		                {
		                    parseStatus = parser.ParseBuffer(buffer, bytesRead, ref headerConsumed);
		                }
		                catch (Exception)
		                {
		                    parseStatus = ParserState.Invalid;
		                }
		
		                if (parseStatus == ParserState.Done) return CreateHttpResponseMessage(httpResponse, stream, bytesRead - headerConsumed); // Create and return parsed HttpResponseMessage
		                else if (parseStatus != ParserState.NeedMoreData) throw new InvalidOperationException("HttpMessageParserError");
		                else if (bytesRead == 0) throw new IOException("ReadAsHttpMessageUnexpectedTermination");
		            }
		        }
		
		        private static Uri CreateRequestUri(string uriScheme, HttpUnsortedRequest httpRequest)
		        {
		            Contract.Assert(httpRequest != null, "httpRequest cannot be null.");
		            Contract.Assert(uriScheme != null, "uriScheme cannot be null");
		
		            IEnumerable<string> hostValues;
		            if (httpRequest.HttpHeaders.TryGetValues(FormattingUtilities.HttpHostHeader, out hostValues))
		            {
		                // ReSharper disable once PossibleMultipleEnumeration
		                var hostCount = hostValues.Count();
		                if (hostCount != 1) throw new InvalidOperationException("HttpMessageParserInvalidHostCount");
		            }
		            else
		            {
		                throw new InvalidOperationException("HttpMessageParserInvalidHostCount");
		            }
		
		            // We don't use UriBuilder as hostValues.ElementAt(0) contains 'host:port' and UriBuilder needs these split out into separate host and port.
		            string requestUri = String.Format(CultureInfo.InvariantCulture, "{0}://{1}{2}", uriScheme, hostValues.ElementAt(0), httpRequest.RequestUri);
		            return new Uri(requestUri);
		        }
		
		        // ReSharper disable once ParameterTypeCanBeEnumerable.Local
		        private static HttpContent CreateHeaderFields(HttpHeaders source, HttpHeaders destination, Stream contentStream, int rewind)
		        {
		            Contract.Assert(source != null, "source headers cannot be null");
		            Contract.Assert(destination != null, "destination headers cannot be null");
		            Contract.Assert(contentStream != null, "contentStream must be non null");
		            HttpContentHeaders contentHeaders = null;
		            HttpContent content = null;
		
		            // Set the header fields
		            foreach (KeyValuePair<string, IEnumerable<string>> header in source)
		            {
		                if (!destination.TryAddWithoutValidation(header.Key, header.Value))
		                {
		                    if (contentHeaders == null) contentHeaders = FormattingUtilities.CreateEmptyContentHeaders();
		                    contentHeaders.TryAddWithoutValidation(header.Key, header.Value);
		                }
		            }
		
		            // If we have content headers then create an HttpContent for this Response
		            if (contentHeaders != null)
		            {
		                // Need to rewind the input stream to be at the position right after the HTTP header
		                // which we may already have parsed as we read the content stream.
		                if (!contentStream.CanSeek) throw new InvalidOperationException("HttpMessageContentStreamMustBeSeekable");
		
		                contentStream.Seek(0 - rewind, SeekOrigin.Current);
		                content = new StreamContent(contentStream);
		                contentHeaders.CopyTo(content.Headers);
		            }
		
		            return content;
		        }
		
		        private static HttpRequestMessage CreateHttpRequestMessage(string uriScheme, HttpUnsortedRequest httpRequest, Stream contentStream, int rewind)
		        {
		            Contract.Assert(uriScheme != null, "URI scheme must be non null");
		            Contract.Assert(httpRequest != null, "httpRequest must be non null");
		            Contract.Assert(contentStream != null, "contentStream must be non null");
		
		            HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
		
		            // Set method, requestURI, and version
		            httpRequestMessage.Method = httpRequest.Method;
		            httpRequestMessage.RequestUri = CreateRequestUri(uriScheme, httpRequest);
		            httpRequestMessage.Version = httpRequest.Version;
		
		            // Set the header fields and content if any
		            httpRequestMessage.Content = CreateHeaderFields(httpRequest.HttpHeaders, httpRequestMessage.Headers, contentStream, rewind);
		
		            return httpRequestMessage;
		        }
		
		        private static HttpResponseMessage CreateHttpResponseMessage(HttpUnsortedResponse httpResponse, Stream contentStream, int rewind)
		        {
		            Contract.Assert(httpResponse != null, "httpResponse must be non null");
		            Contract.Assert(contentStream != null, "contentStream must be non null");
		
		            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
		
		            // Set version, status code and reason phrase
		            httpResponseMessage.Version = httpResponse.Version;
		            httpResponseMessage.StatusCode = httpResponse.StatusCode;
		            httpResponseMessage.ReasonPhrase = httpResponse.ReasonPhrase;
		
		            // Set the header fields and content if any
		            httpResponseMessage.Content = CreateHeaderFields(httpResponse.HttpHeaders, httpResponseMessage.Headers, contentStream, rewind);
		
		            return httpResponseMessage;
		        }
		    }
		}
		#endregion
		
		#region HttpContentMultipartExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net.Http;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class HttpContentMultipartExtensions
		    {
		        private const int MinBufferSize = 256;
		        private const int DefaultBufferSize = 32 * 1024;
		
		        public static bool IsMimeMultipartContent(this HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            return MimeMultipartBodyPartParser.IsMimeMultipartContent(content);
		        }
		
		        public static bool IsMimeMultipartContent(this HttpContent content, string subtype)
		        {
		            if (String.IsNullOrWhiteSpace(subtype)) throw new ArgumentNullException("subtype");
		            if (IsMimeMultipartContent(content))
		            {
		                if (content.Headers.ContentType.MediaType.Equals("multipart/" + subtype, StringComparison.OrdinalIgnoreCase)) return true;
		            }
		
		            return false;
		        }
		
		        public static Task<MultipartMemoryStreamProvider> ReadAsMultipartAsync(this HttpContent content)
		        {
		            return ReadAsMultipartAsync(content, new MultipartMemoryStreamProvider(), DefaultBufferSize);
		        }
		
		        public static Task<MultipartMemoryStreamProvider> ReadAsMultipartAsync(this HttpContent content, CancellationToken cancellationToken)
		        {
		            return ReadAsMultipartAsync(content, new MultipartMemoryStreamProvider(), DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider) where T : MultipartStreamProvider
		        {
		            return ReadAsMultipartAsync(content, streamProvider, DefaultBufferSize);
		        }
		
		        public static Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider, CancellationToken cancellationToken) where T : MultipartStreamProvider
		        {
		            return ReadAsMultipartAsync(content, streamProvider, DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider, int bufferSize) where T : MultipartStreamProvider
		        {
		            return ReadAsMultipartAsync(content, streamProvider, bufferSize, CancellationToken.None);
		        }
		
		        public static async Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider, int bufferSize, CancellationToken cancellationToken) where T : MultipartStreamProvider
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            if (streamProvider == null) throw new ArgumentNullException("streamProvider");
		
		            if (bufferSize < MinBufferSize) throw new ArgumentOutOfRangeException("bufferSize");
		
		            Stream stream;
		            try
		            {
		                stream = await content.ReadAsStreamAsync();
		            }
		            catch (Exception e)
		            {
		                throw new IOException("ReadAsMimeMultipartErrorReading", e);
		            }
		
		            using (var parser = new MimeMultipartBodyPartParser(content, streamProvider))
		            {
		                byte[] data = new byte[bufferSize];
		                MultipartAsyncContext context = new MultipartAsyncContext(stream, parser, data, streamProvider.Contents);
		
		                // Start async read/write loop
		                await MultipartReadAsync(context, cancellationToken);
		
		                // Let the stream provider post-process when everything is complete
		                await streamProvider.ExecutePostProcessingAsync(cancellationToken);
		                return streamProvider;
		            }
		        }
		
		        private static async Task MultipartReadAsync(MultipartAsyncContext context, CancellationToken cancellationToken)
		        {
		            Contract.Assert(context != null, "context cannot be null");
		            while (true)
		            {
		                int bytesRead;
		                try
		                {
		                    bytesRead = await context.ContentStream.ReadAsync(context.Data, 0, context.Data.Length, cancellationToken);
		                }
		                catch (Exception e)
		                {
		                    throw new IOException("ReadAsMimeMultipartErrorReading", e);
		                }
		
		                IEnumerable<MimeBodyPart> parts = context.MimeParser.ParseBuffer(context.Data, bytesRead);
		
		                foreach (MimeBodyPart part in parts)
		                {
		                    foreach (ArraySegment<byte> segment in part.Segments)
		                    {
		                        try
		                        {
		                            await part.WriteSegment(segment, cancellationToken);
		                        }
		                        catch (Exception e)
		                        {
		                            part.Dispose();
		                            throw new IOException("ReadAsMimeMultipartErrorWriting", e);
		                        }
		                    }
		
		                    if (CheckIsFinalPart(part, context.Result)) return;
		                }
		            }
		        }
		
		        private static bool CheckIsFinalPart(MimeBodyPart part, ICollection<HttpContent> result)
		        {
		            Contract.Assert(part != null, "part cannot be null.");
		            Contract.Assert(result != null, "result cannot be null.");
		            if (part.IsComplete)
		            {
		                var partContent = part.GetCompletedHttpContent();
		                if (partContent != null)
		                {
		                    result.Add(partContent);
		                }
		
		                bool isFinal = part.IsFinal;
		                part.Dispose();
		                return isFinal;
		            }
		
		            return false;
		        }
		
		        private class MultipartAsyncContext
		        {
		            public MultipartAsyncContext(Stream contentStream, MimeMultipartBodyPartParser mimeParser, byte[] data, ICollection<HttpContent> result)
		            {
		                Contract.Assert(contentStream != null);
		                Contract.Assert(mimeParser != null);
		                Contract.Assert(data != null);
		
		                ContentStream = contentStream;
		                Result = result;
		                MimeParser = mimeParser;
		                Data = data;
		            }
		
		            public Stream ContentStream { get; private set; }
		
		            public ICollection<HttpContent> Result { get; private set; }
		
		            public byte[] Data { get; private set; }
		
		            public MimeMultipartBodyPartParser MimeParser { get; private set; }
		        }
		    }
		}
		#endregion
		
		#region HttpHeaderExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.Net.Http.Headers;
		    
		    public static class HttpHeaderExtensions
		    {
		        public static void CopyTo(this HttpContentHeaders fromHeaders, HttpContentHeaders toHeaders)
		        {
		            Contract.Assert(fromHeaders != null, "fromHeaders cannot be null.");
		            Contract.Assert(toHeaders != null, "toHeaders cannot be null.");
		
		            foreach (KeyValuePair<string, IEnumerable<string>> header in fromHeaders)
		            {
		                toHeaders.TryAddWithoutValidation(header.Key, header.Value);
		            }
		        }
		    }
		}
		#endregion
		
		#region HttpMessageContent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Text;
		    using System.Threading.Tasks;
		    
		    public class HttpMessageContent : HttpContent
		    {
		        // ReSharper disable InconsistentNaming
		        private const string SP = " ";
		        private const string ColonSP = ": ";
		        private const string CRLF = "\r\n";
		        // ReSharper restore InconsistentNaming
		        private const string CommaSeparator = ", ";
		
		        private const int DefaultHeaderAllocation = 2 * 1024;
		
		        private const string DefaultMediaType = "application/http";
		
		        private const string MsgTypeParameter = "msgtype";
		        private const string DefaultRequestMsgType = "request";
		        private const string DefaultResponseMsgType = "response";
		
		        //private const string DefaultRequestMediaType = DefaultMediaType + "; " + MsgTypeParameter + "=" + DefaultRequestMsgType;
		        //private const string DefaultResponseMediaType = DefaultMediaType + "; " + MsgTypeParameter + "=" + DefaultResponseMsgType;
		
		        // Set of header fields that only support single values such as Set-Cookie.
		        private static readonly HashSet<string> _singleValueHeaderFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		        {
		            "Cookie",
		            "Set-Cookie",
		            "X-Powered-By",
		        };
		
		        // Set of header fields that should get serialized as space-separated values such as User-Agent.
		        private static readonly HashSet<string> _spaceSeparatedValueHeaderFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		        {
		            "User-Agent",
		        };
		
		        private bool _contentConsumed;
		        private Lazy<Task<Stream>> _streamTask;
		
		        public HttpMessageContent(HttpRequestMessage httpRequest)
		        {
		            if (httpRequest == null) throw new ArgumentNullException("httpRequest");
		
		            HttpRequestMessage = httpRequest;
		            Headers.ContentType = new MediaTypeHeaderValue(DefaultMediaType);
		            Headers.ContentType.Parameters.Add(new NameValueHeaderValue(MsgTypeParameter, DefaultRequestMsgType));
		
		            InitializeStreamTask();
		        }
		
		        public HttpMessageContent(HttpResponseMessage httpResponse)
		        {
		            if (httpResponse == null) throw new ArgumentNullException("httpResponse");
		
		            HttpResponseMessage = httpResponse;
		            Headers.ContentType = new MediaTypeHeaderValue(DefaultMediaType);
		            Headers.ContentType.Parameters.Add(new NameValueHeaderValue(MsgTypeParameter, DefaultResponseMsgType));
		
		            InitializeStreamTask();
		        }
		
		        private HttpContent Content
		        {
		            get { return HttpRequestMessage != null ? HttpRequestMessage.Content : HttpResponseMessage.Content; }
		        }
		
		        public HttpRequestMessage HttpRequestMessage { get; private set; }
		
		        public HttpResponseMessage HttpResponseMessage { get; private set; }
		
		        private void InitializeStreamTask()
		        {
		            _streamTask = new Lazy<Task<Stream>>(() => Content == null ? null : Content.ReadAsStreamAsync());
		        }
		
		        public static bool ValidateHttpMessageContent(HttpContent content, bool isRequest, bool throwOnError)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		
		            var contentType = content.Headers.ContentType;
		            if (contentType != null)
		            {
		                if (!contentType.MediaType.Equals(DefaultMediaType, StringComparison.OrdinalIgnoreCase))
		                {
		                    if (throwOnError) throw new ArgumentException("content");
		                    return false;
		                }
		
		                foreach (NameValueHeaderValue parameter in contentType.Parameters)
		                {
		                    if (parameter.Name.Equals(MsgTypeParameter, StringComparison.OrdinalIgnoreCase))
		                    {
		                        var msgType = FormattingUtilities.UnquoteToken(parameter.Value);
		                        if (!msgType.Equals(isRequest ? DefaultRequestMsgType : DefaultResponseMsgType, StringComparison.OrdinalIgnoreCase))
		                        {
		                            if (throwOnError) throw new ArgumentException("content");
		                            return false;
		                        }
		
		                        return true;
		                    }
		                }
		            }
		
		            if (throwOnError) throw new ArgumentException("content");
		            return false;
		        }
		
		        protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context)
		        {
		            if (stream == null) throw new ArgumentNullException("stream");
		
		            var header = SerializeHeader();
		            await stream.WriteAsync(header, 0, header.Length);
		
		            if (Content != null)
		            {
		                var readStream = await _streamTask.Value;
		                ValidateStreamForReading(readStream);
		                await Content.CopyToAsync(stream);
		            }
		        }
		
		        protected override bool TryComputeLength(out long length)
		        {
		            // We have four states we could be in:
		            //   1. We have content, but the task is still running or finished without success
		            //   2. We have content, the task has finished successfully, and the stream came back as a null or non-seekable
		            //   3. We have content, the task has finished successfully, and the stream is seekable, so we know its length
		            //   4. We don't have content (streamTask.Value == null)
		            //
		            // For #1 and #2, we return false.
		            // For #3, we return true & the size of our headers + the content length
		            // For #4, we return true & the size of our headers
		
		            var hasContent = _streamTask.Value != null;
		            length = 0;
		
		            // Cases #1, #2, #3
		            if (hasContent)
		            {
		                Stream readStream;
		                if (!_streamTask.Value.TryGetResult(out readStream) /* Case #1 */ || readStream == null || !readStream.CanSeek /* Case #2 */) 
		                {
		                    length = -1;
		                    return false;
		                }
		                length = readStream.Length; // Case #3
		            }
		
		            // We serialize header to a StringBuilder so that we can determine the length
		            // following the pattern for HttpContent to try and determine the message length.
		            // The perf overhead is no larger than for the other HttpContent implementations.
		            var header = SerializeHeader();
		            length += header.Length;
		            return true;
		        }
		
		        protected override void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                if (HttpRequestMessage != null)
		                {
		                    HttpRequestMessage.Dispose();
		                    HttpRequestMessage = null;
		                }
		
		                if (HttpResponseMessage != null)
		                {
		                    HttpResponseMessage.Dispose();
		                    HttpResponseMessage = null;
		                }
		            }
		
		            base.Dispose(disposing);
		        }
		
		        private static void SerializeRequestLine(StringBuilder message, HttpRequestMessage httpRequest)
		        {
		            Contract.Assert(message != null, "message cannot be null");
		            message.Append(httpRequest.Method + SP);
		            message.Append(httpRequest.RequestUri.PathAndQuery + SP);
		            message.Append(FormattingUtilities.HttpVersionToken + "/" + (httpRequest.Version != null ? httpRequest.Version.ToString(2) : "1.1") + CRLF);
		
		            // Only insert host header if not already present.
		            if (httpRequest.Headers.Host == null)
		            {
		                message.Append(FormattingUtilities.HttpHostHeader + ColonSP + httpRequest.RequestUri.Authority + CRLF);
		            }
		        }
		
		        private static void SerializeStatusLine(StringBuilder message, HttpResponseMessage httpResponse)
		        {
		            Contract.Assert(message != null, "message cannot be null");
		            message.Append(FormattingUtilities.HttpVersionToken + "/" + (httpResponse.Version != null ? httpResponse.Version.ToString(2) : "1.1") + SP);
		            message.Append((int)httpResponse.StatusCode + SP);
		            message.Append(httpResponse.ReasonPhrase + CRLF);
		        }
		
		        private static void SerializeHeaderFields(StringBuilder message, HttpHeaders headers)
		        {
		            Contract.Assert(message != null, "message cannot be null");
		            if (headers != null)
		            {
		                foreach (KeyValuePair<string, IEnumerable<string>> header in headers)
		                {
		                    if (_singleValueHeaderFields.Contains(header.Key))
		                    {
		                        foreach (string value in header.Value) message.Append(header.Key + ColonSP + value + CRLF);
		                    }
		                    else if (_spaceSeparatedValueHeaderFields.Contains(header.Key))
		                    {
		                        message.Append(header.Key + ColonSP + String.Join(SP, header.Value) + CRLF);
		                    }
		                    else
		                    {
		                        message.Append(header.Key + ColonSP + String.Join(CommaSeparator, header.Value) + CRLF);
		                    }
		                }
		            }
		        }
		
		        private byte[] SerializeHeader()
		        {
		            var message = new StringBuilder(DefaultHeaderAllocation);
		            HttpHeaders headers;
		            HttpContent content;
		            if (HttpRequestMessage != null)
		            {
		                SerializeRequestLine(message, HttpRequestMessage);
		                headers = HttpRequestMessage.Headers;
		                content = HttpRequestMessage.Content;
		            }
		            else
		            {
		                SerializeStatusLine(message, HttpResponseMessage);
		                headers = HttpResponseMessage.Headers;
		                content = HttpResponseMessage.Content;
		            }
		
		            SerializeHeaderFields(message, headers);
		            if (content != null)
		            {
		                SerializeHeaderFields(message, content.Headers);
		            }
		
		            message.Append(CRLF);
		            return Encoding.UTF8.GetBytes(message.ToString());
		        }
		
		        private void ValidateStreamForReading(Stream stream)
		        {
		            // If the content needs to be written to a target stream a 2nd time, then the stream must support
		            // seeking (e.g. a FileStream), otherwise the stream can't be copied a second time to a target 
		            // stream (e.g. a NetworkStream).
		            if (_contentConsumed)
		            {
		                if (stream != null && stream.CanRead) stream.Position = 0;
		                else throw new InvalidOperationException();
		            }
		
		            _contentConsumed = true;
		        }
		    }
		}
		#endregion
		
		#region HttpRequestHeaderParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		
		    public class HttpRequestHeaderParser
		    {
		        internal const int DefaultMaxRequestLineSize = 2 * 1024;
		        internal const int DefaultMaxHeaderSize = 16 * 1024; // Same default size as IIS has for regular requests
		
		        private HttpRequestState _requestStatus = HttpRequestState.RequestLine;
		
		        private readonly HttpRequestLineParser _requestLineParser;
		        private readonly InternetMessageFormatHeaderParser _headerParser;
		
		        public HttpRequestHeaderParser(HttpUnsortedRequest httpRequest, int maxRequestLineSize = DefaultMaxRequestLineSize, int maxHeaderSize = DefaultMaxHeaderSize)
		        {
		            if (httpRequest == null) throw new ArgumentNullException("httpRequest");
		
		            HttpUnsortedRequest httpRequest1 = httpRequest;
		
		            // Create request line parser
		            _requestLineParser = new HttpRequestLineParser(httpRequest1, maxRequestLineSize);
		
		            // Create header parser
		            _headerParser = new InternetMessageFormatHeaderParser(httpRequest1.HttpHeaders, maxHeaderSize);
		        }
		
		        private enum HttpRequestState
		        {
		            RequestLine = 0, // parsing request line
		            RequestHeaders // reading headers
		        }
		
		        public ParserState ParseBuffer(byte[] buffer, int bytesReady, ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            var parseStatus = ParserState.NeedMoreData;
		            ParserState subParseStatus;
		
		            switch (_requestStatus)
		            {
		                case HttpRequestState.RequestLine:
		                    try
		                    {
		                        subParseStatus = _requestLineParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done)
		                    {
		                        _requestStatus = HttpRequestState.RequestHeaders;
		                        goto case HttpRequestState.RequestHeaders;
		                    }
		                    else if (subParseStatus != ParserState.NeedMoreData)
		                    {
		                        // Report error - either Invalid or DataTooBig
		                        parseStatus = subParseStatus;
		                        // ReSharper disable once RedundantJumpStatement
		                        break;
		                    }
		
		                    break; // read more data
		
		                case HttpRequestState.RequestHeaders:
		                    if (bytesConsumed >= bytesReady) break; // we already can tell we need more data
		                    try
		                    {
		                        subParseStatus = _headerParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done) parseStatus = subParseStatus;
		                    else if (subParseStatus != ParserState.NeedMoreData) parseStatus = subParseStatus;
		
		                    break; // need more data
		            }
		
		            return parseStatus;
		        }
		    }
		}
		#endregion
		
		#region HttpRequestLineParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics.Contracts;
		    using System.Net.Http;
		    using System.Text;
		    
		    public class HttpRequestLineParser
		    {
		        public const int MinRequestLineSize = 14;
		        private const int DefaultTokenAllocation = 2 * 1024;
		
		        private int _totalBytesConsumed;
		        private readonly int _maximumHeaderLength;
		
		        private HttpRequestLineState _requestLineState;
		        private HttpUnsortedRequest _httpRequest;
		        private readonly StringBuilder _currentToken = new StringBuilder(DefaultTokenAllocation);
		
		        public HttpRequestLineParser(HttpUnsortedRequest httpRequest, int maxRequestLineSize)
		        {
		            // The minimum length which would be an empty header terminated by CRLF
		            if (maxRequestLineSize < MinRequestLineSize) throw new ArgumentOutOfRangeException("maxRequestLineSize");
		            if (httpRequest == null) throw new ArgumentNullException("httpRequest");
		
		            _httpRequest = httpRequest;
		            _maximumHeaderLength = maxRequestLineSize;
		        }
		
		        private enum HttpRequestLineState
		        {
		            RequestMethod = 0,
		            RequestUri,
		            BeforeVersionNumbers,
		            MajorVersionNumber,
		            MinorVersionNumber,
		            AfterCarriageReturn
		        }
		
		        public ParserState ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            ParserState parseStatus = ParserState.NeedMoreData;
		
		            if (bytesConsumed >= bytesReady) return parseStatus;  // We already can tell we need more data
		
		            try
		            {
		                parseStatus = ParseRequestLine(buffer, bytesReady, ref bytesConsumed, ref _requestLineState, _maximumHeaderLength, ref _totalBytesConsumed, _currentToken, _httpRequest);
		            }
		            catch (Exception)
		            {
		                parseStatus = ParserState.Invalid;
		            }
		
		            return parseStatus;
		        }
		
		        private static ParserState ParseRequestLine(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            ref HttpRequestLineState requestLineState,
		            int maximumHeaderLength,
		            ref int totalBytesConsumed,
		            StringBuilder currentToken,
		            HttpUnsortedRequest httpRequest)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseRequestLine()|(bytesReady - bytesConsumed) < 0");
		            Contract.Assert(maximumHeaderLength <= 0 || totalBytesConsumed <= maximumHeaderLength, "ParseRequestLine()|Headers already read exceeds limit.");
		
		            // Remember where we started.
		            var initialBytesParsed = bytesConsumed;
		            int segmentStart;
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            var parseStatus = ParserState.DataTooBig;
		            var effectiveMax = maximumHeaderLength <= 0 ? Int32.MaxValue : (maximumHeaderLength - totalBytesConsumed + bytesConsumed);
		            if (bytesReady < effectiveMax)
		            {
		                parseStatus = ParserState.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (requestLineState)
		            {
		                case HttpRequestLineState.RequestMethod:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] < 0x21 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            var method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(method);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        var method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(method);
		                    }
		
		                    // Copy value out
		                    httpRequest.Method = new HttpMethod(currentToken.ToString());
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    requestLineState = HttpRequestLineState.RequestUri;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpRequestLineState.RequestUri;
		
		                case HttpRequestLineState.RequestUri:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] == '\r')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(addr);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(addr);
		                    }
		
		                    // URI validation happens when we create the URI later.
		                    if (currentToken.Length == 0) throw new FormatException("HttpMessageParserEmptyUri");
		
		                    // Copy value out
		                    httpRequest.RequestUri = currentToken.ToString();
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    requestLineState = HttpRequestLineState.BeforeVersionNumbers;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HttpRequestLineState.BeforeVersionNumbers;
		
		                case HttpRequestLineState.BeforeVersionNumbers:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '/')
		                    {
		                        if (buffer[bytesConsumed] < 0x21 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(token);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(token);
		                    }
		
		                    // Validate value
		                    var version = currentToken.ToString();
		                    if (String.CompareOrdinal(FormattingUtilities.HttpVersionToken, version) != 0) throw new FormatException("HttpInvalidVersion");
		                    currentToken.Clear();
		
		                    // Move past the '/'
		                    requestLineState = HttpRequestLineState.MajorVersionNumber;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpRequestLineState.MajorVersionNumber;
		
		                case HttpRequestLineState.MajorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '.')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(major);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        var major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(major);
		                    }
		
		                    // Move past the "."
		                    currentToken.Append('.');
		                    requestLineState = HttpRequestLineState.MinorVersionNumber;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HttpRequestLineState.MinorVersionNumber;
		
		                case HttpRequestLineState.MinorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '\r')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(minor);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(minor);
		                    }
		
		                    // Copy out value
		                    httpRequest.Version = Version.Parse(currentToken.ToString());
		                    currentToken.Clear();
		
		                    // Move past the CR
		                    requestLineState = HttpRequestLineState.AfterCarriageReturn;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		                    goto case HttpRequestLineState.AfterCarriageReturn;
		
		                case HttpRequestLineState.AfterCarriageReturn:
		                    if (buffer[bytesConsumed] != '\n')
		                    {
		                        parseStatus = ParserState.Invalid;
		                        goto quit;
		                    }
		
		                    parseStatus = ParserState.Done;
		                    bytesConsumed++;
		                    break;
		            }
		
		        quit:
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		    }
		}
		#endregion
		
		#region HttpResponseHeaderParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		
		    public class HttpResponseHeaderParser
		    {
		        public const int DefaultMaxStatusLineSize = 2 * 1024;
		        public const int DefaultMaxHeaderSize = 16 * 1024; // Same default size as IIS has for HTTP requests
		
		        private HttpResponseState _responseStatus = HttpResponseState.StatusLine;
		
		        private readonly HttpStatusLineParser _statusLineParser;
		        private readonly InternetMessageFormatHeaderParser _headerParser;
		
		        public HttpResponseHeaderParser(HttpUnsortedResponse httpResponse, int maxResponseLineSize = DefaultMaxStatusLineSize, int maxHeaderSize = DefaultMaxHeaderSize)
		        {
		            if (httpResponse == null) throw new ArgumentNullException("httpResponse");
		
		            HttpUnsortedResponse httpResponse1 = httpResponse;
		
		            // Create status line parser
		            _statusLineParser = new HttpStatusLineParser(httpResponse1, maxResponseLineSize);
		
		            // Create header parser
		            _headerParser = new InternetMessageFormatHeaderParser(httpResponse1.HttpHeaders, maxHeaderSize);
		        }
		
		        private enum HttpResponseState
		        {
		            StatusLine = 0, // parsing status line
		            ResponseHeaders // reading headers
		        }
		
		        public ParserState ParseBuffer(byte[] buffer, int bytesReady, ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            var parseStatus = ParserState.NeedMoreData;
		            ParserState subParseStatus;
		
		            switch (_responseStatus)
		            {
		                case HttpResponseState.StatusLine:
		                    try
		                    {
		                        subParseStatus = _statusLineParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done)
		                    {
		                        _responseStatus = HttpResponseState.ResponseHeaders;
		                        goto case HttpResponseState.ResponseHeaders;
		                    }
		                    else if (subParseStatus != ParserState.NeedMoreData)
		                    {
		                        // Report error - either Invalid or DataTooBig
		                        parseStatus = subParseStatus;
		                    }
		
		                    break; // read more data
		
		                case HttpResponseState.ResponseHeaders:
		                    if (bytesConsumed >= bytesReady) break;  // we already can tell we need more data
		
		                    try
		                    {
		                        subParseStatus = _headerParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done)
		                    {
		                        parseStatus = subParseStatus;
		                    }
		                    else if (subParseStatus != ParserState.NeedMoreData)
		                    {
		                        parseStatus = subParseStatus;
		                        // ReSharper disable once RedundantJumpStatement
		                        break;
		                    }
		
		                    break; // need more data
		            }
		
		            return parseStatus;
		        }
		    }
		}
		#endregion
		
		#region HttpStatusLineParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics.Contracts;
		    using System.Globalization;
		    using System.Net;
		    using System.Text;
		    
		    public class HttpStatusLineParser
		    {
		        public const int MinStatusLineSize = 15;
		        private const int DefaultTokenAllocation = 2 * 1024;
		        //private const int MaxStatusCode = 1000;
		
		        private int _totalBytesConsumed;
		        private readonly int _maximumHeaderLength;
		
		        private HttpStatusLineState _statusLineState;
		        private HttpUnsortedResponse _httpResponse;
		        private readonly StringBuilder _currentToken = new StringBuilder(DefaultTokenAllocation);
		
		        public HttpStatusLineParser(HttpUnsortedResponse httpResponse, int maxStatusLineSize)
		        {
		            // The minimum length which would be an empty header terminated by CRLF
		            if (maxStatusLineSize < MinStatusLineSize) throw new ArgumentOutOfRangeException("maxStatusLineSize");
		            if (httpResponse == null) throw new ArgumentNullException("httpResponse");
		
		            _httpResponse = httpResponse;
		            _maximumHeaderLength = maxStatusLineSize;
		        }
		
		        private enum HttpStatusLineState
		        {
		            BeforeVersionNumbers = 0,
		            MajorVersionNumber,
		            MinorVersionNumber,
		            StatusCode,
		            ReasonPhrase,
		            AfterCarriageReturn
		        }
		
		        public ParserState ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            var parseStatus = ParserState.NeedMoreData;
		
		            if (bytesConsumed >= bytesReady) return parseStatus; // We already can tell we need more data
		
		            try
		            {
		                parseStatus = ParseStatusLine(buffer, bytesReady, ref bytesConsumed, ref _statusLineState, _maximumHeaderLength, ref _totalBytesConsumed, _currentToken, _httpResponse);
		            }
		            catch (Exception)
		            {
		                parseStatus = ParserState.Invalid;
		            }
		
		            return parseStatus;
		        }
		
		        private static ParserState ParseStatusLine(byte[] buffer, int bytesReady, ref int bytesConsumed, ref HttpStatusLineState statusLineState, int maximumHeaderLength, ref int totalBytesConsumed, StringBuilder currentToken, HttpUnsortedResponse httpResponse)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseRequestLine()|(bytesReady - bytesConsumed) < 0");
		            Contract.Assert(maximumHeaderLength <= 0 || totalBytesConsumed <= maximumHeaderLength, "ParseRequestLine()|Headers already read exceeds limit.");
		
		            // Remember where we started.
		            var initialBytesParsed = bytesConsumed;
		            int segmentStart;
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            var parseStatus = ParserState.DataTooBig;
		            int effectiveMax = maximumHeaderLength <= 0 ? Int32.MaxValue : (maximumHeaderLength - totalBytesConsumed + bytesConsumed);
		            if (bytesReady < effectiveMax)
		            {
		                parseStatus = ParserState.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (statusLineState)
		            {
		                case HttpStatusLineState.BeforeVersionNumbers:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '/')
		                    {
		                        if (buffer[bytesConsumed] < 0x21 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            var token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(token);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        var token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(token);
		                    }
		
		                    // Validate value
		                    var version = currentToken.ToString();
		                    if (String.CompareOrdinal(FormattingUtilities.HttpVersionToken, version) != 0) throw new FormatException("HttpInvalidVersion");
		
		                    currentToken.Clear();
		
		                    // Move past the '/'
		                    statusLineState = HttpStatusLineState.MajorVersionNumber;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HttpStatusLineState.MajorVersionNumber;
		
		                case HttpStatusLineState.MajorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '.')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(major);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(major);
		                    }
		
		                    // Move past the "."
		                    currentToken.Append('.');
		                    statusLineState = HttpStatusLineState.MinorVersionNumber;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.MinorVersionNumber;
		
		                case HttpStatusLineState.MinorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(minor);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(minor);
		                    }
		
		                    // Copy out value
		                    httpResponse.Version = Version.Parse(currentToken.ToString());
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    statusLineState = HttpStatusLineState.StatusCode;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.StatusCode;
		
		                case HttpStatusLineState.StatusCode:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(method);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(method);
		                    }
		
		                    // Copy value out
		                    int statusCode = Int32.Parse(currentToken.ToString(), CultureInfo.InvariantCulture);
		                    if (statusCode < 100 || statusCode > 1000)
		                    {
		                        throw new FormatException("HttpInvalidStatusCode");
		                    }
		
		                    httpResponse.StatusCode = (HttpStatusCode)statusCode;
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    statusLineState = HttpStatusLineState.ReasonPhrase;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.ReasonPhrase;
		
		                case HttpStatusLineState.ReasonPhrase:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '\r')
		                    {
		                        if (buffer[bytesConsumed] < 0x20 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(addr);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(addr);
		                    }
		
		                    // Copy value out
		                    httpResponse.ReasonPhrase = currentToken.ToString();
		                    currentToken.Clear();
		
		                    // Move past the CR
		                    statusLineState = HttpStatusLineState.AfterCarriageReturn;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.AfterCarriageReturn;
		
		                case HttpStatusLineState.AfterCarriageReturn:
		                    if (buffer[bytesConsumed] != '\n')
		                    {
		                        parseStatus = ParserState.Invalid;
		                        goto quit;
		                    }
		
		                    parseStatus = ParserState.Done;
		                    bytesConsumed++;
		                    break;
		            }
		
		        quit:
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		    }
		
		}
		#endregion
		
		#region HttpUnsortedHeaders
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Net.Http.Headers;
		    
		    public class HttpUnsortedHeaders : HttpHeaders {}
		}
		#endregion
		
		#region HttpUnsortedRequest
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    
		    public class HttpUnsortedRequest
		    {
		        public HttpUnsortedRequest()
		        {
		            // Collection of unsorted headers. Later we will sort it into the appropriate
		            // HttpContentHeaders, HttpRequestHeaders, and HttpResponseHeaders.
		            HttpHeaders = new HttpUnsortedHeaders();
		        }
		
		        public HttpMethod Method { get; set; }
		
		        public string RequestUri { get; set; }
		
		        public Version Version { get; set; }
		
		        public HttpHeaders HttpHeaders { get; private set; }
		    }
		}
		#endregion
		
		#region HttpUnsortedResponse
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Net;
		    using System.Net.Http.Headers;
		
		    public class HttpUnsortedResponse
		    {
		        public HttpUnsortedResponse()
		        {
		            // Collection of unsorted headers. Later we will sort it into the appropriate
		            // HttpContentHeaders, HttpRequestHeaders, and HttpResponseHeaders.
		            HttpHeaders = new HttpUnsortedHeaders();
		        }
		
		        public Version Version { get; set; }
		
		        public HttpStatusCode StatusCode { get; set; }
		
		        public string ReasonPhrase { get; set; }
		
		        public HttpHeaders HttpHeaders { get; private set; }
		    }
		}
		#endregion
		
		#region HttpUtilities
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Net;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class HttpUtilities
		    {
		        internal static readonly Version DefaultVersion = HttpVersion.Version11;
		        internal static readonly byte[] EmptyByteArray = new byte[0];
		
		        static HttpUtilities() { }
		
		        public static bool IsHttpUri(Uri uri)
		        {
		            var scheme = uri.Scheme;
		            if (string.Compare("http", scheme, StringComparison.OrdinalIgnoreCase) != 0) return string.Compare("https", scheme, StringComparison.OrdinalIgnoreCase) == 0;
		            else return true;
		        }
		
		        public static bool HandleFaultsAndCancelation<T>(Task task, TaskCompletionSource<T> tcs)
		        {
		            if (task.IsFaulted)
		            {
		                // ReSharper disable once PossibleNullReferenceException
		                tcs.TrySetException(task.Exception.GetBaseException());
		                return true;
		            }
		
		            if (!task.IsCanceled) return false;
		            tcs.TrySetCanceled();
		            return true;
		        }
		
		        public static Task ContinueWithStandard(this Task task, Action<Task> continuation)
		        {
		            return task.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		        }
		
		        public static Task ContinueWithStandard<T>(this Task<T> task, Action<Task<T>> continuation)
		        {
		            return task.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		        }
		    }
		}
		#endregion
		
		#region InternetMessageFormatHeaderParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics.Contracts;
		    using System.Net.Http.Headers;
		    using System.Text;
		    
		    public enum ParserState
		    {
		        NeedMoreData,
		        Done,
		        Invalid,
		        DataTooBig,
		    }
		    
		    public class InternetMessageFormatHeaderParser
		    {
		        internal const int MinHeaderSize = 2;
		
		        private int _totalBytesConsumed;
		        private readonly int _maxHeaderSize;
		
		        private HeaderFieldState _headerState;
		        private readonly HttpHeaders _headers;
		        private readonly CurrentHeaderFieldStore _currentHeader;
		
		        public InternetMessageFormatHeaderParser(HttpHeaders headers, int maxHeaderSize)
		        {
		            // The minimum length which would be an empty header terminated by CRLF
		            if (maxHeaderSize < MinHeaderSize) throw new ArgumentOutOfRangeException("maxHeaderSize");
		            if (headers == null) throw new ArgumentNullException("headers");
		            _headers = headers;
		            _maxHeaderSize = maxHeaderSize;
		            _currentHeader = new CurrentHeaderFieldStore();
		        }
		
		        private enum HeaderFieldState
		        {
		            Name = 0,
		            Value,
		            AfterCarriageReturn,
		            FoldingLine
		        }
		
		        public ParserState ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		            var parseStatus = ParserState.NeedMoreData;
		
		            if (bytesConsumed >= bytesReady) return parseStatus;  // We already can tell we need more data
		
		            try
		            {
		                parseStatus = ParseHeaderFields(
		                    buffer,
		                    bytesReady,
		                    ref bytesConsumed,
		                    ref _headerState,
		                    _maxHeaderSize,
		                    ref _totalBytesConsumed,
		                    _currentHeader,
		                    _headers);
		            }
		            catch (Exception)
		            {
		                parseStatus = ParserState.Invalid;
		            }
		
		            return parseStatus;
		        }
		
		        private static ParserState ParseHeaderFields(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            ref HeaderFieldState requestHeaderState,
		            int maximumHeaderLength,
		            ref int totalBytesConsumed,
		            CurrentHeaderFieldStore currentField,
		            HttpHeaders headers)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseHeaderFields()|(inputBufferLength - bytesParsed) < 0");
		            Contract.Assert(maximumHeaderLength <= 0 || totalBytesConsumed <= maximumHeaderLength, "ParseHeaderFields()|Headers already read exceeds limit.");
		
		            // Remember where we started.
		            var initialBytesParsed = bytesConsumed;
		            int segmentStart;
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            ParserState parseStatus = ParserState.DataTooBig;
		            var effectiveMax = maximumHeaderLength <= 0 ? Int32.MaxValue : maximumHeaderLength - totalBytesConsumed + initialBytesParsed;
		            if (bytesReady < effectiveMax)
		            {
		                parseStatus = ParserState.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (requestHeaderState)
		            {
		                case HeaderFieldState.Name:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ':')
		                    {
		                        if (buffer[bytesConsumed] == '\r')
		                        {
		                            if (!currentField.IsEmpty())
		                            {
		                                parseStatus = ParserState.Invalid;
		                                goto quit;
		                            }
		                            else
		                            {
		                                // Move past the '\r'
		                                requestHeaderState = HeaderFieldState.AfterCarriageReturn;
		                                if (++bytesConsumed == effectiveMax)
		                                {
		                                    goto quit;
		                                }
		
		                                goto case HeaderFieldState.AfterCarriageReturn;
		                            }
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string headerFieldName = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentField.Name.Append(headerFieldName);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string headerFieldName = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentField.Name.Append(headerFieldName);
		                    }
		
		                    // Move past the ':'
		                    requestHeaderState = HeaderFieldState.Value;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HeaderFieldState.Value;
		
		                case HeaderFieldState.Value:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '\r')
		                    {
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string headerFieldValue = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentField.Value.Append(headerFieldValue);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string headerFieldValue = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentField.Value.Append(headerFieldValue);
		                    }
		
		                    // Move past the CR
		                    requestHeaderState = HeaderFieldState.AfterCarriageReturn;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HeaderFieldState.AfterCarriageReturn;
		
		                case HeaderFieldState.AfterCarriageReturn:
		                    if (buffer[bytesConsumed] != '\n')
		                    {
		                        parseStatus = ParserState.Invalid;
		                        goto quit;
		                    }
		
		                    if (currentField.IsEmpty())
		                    {
		                        parseStatus = ParserState.Done;
		                        bytesConsumed++;
		                        goto quit;
		                    }
		
		                    requestHeaderState = HeaderFieldState.FoldingLine;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HeaderFieldState.FoldingLine;
		
		                case HeaderFieldState.FoldingLine:
		                    if (buffer[bytesConsumed] != ' ' && buffer[bytesConsumed] != '\t')
		                    {
		                        currentField.CopyTo(headers);
		                        requestHeaderState = HeaderFieldState.Name;
		                        if (bytesConsumed == effectiveMax)
		                        {
		                            goto quit;
		                        }
		
		                        goto case HeaderFieldState.Name;
		                    }
		
		                    // Unfold line by inserting SP instead
		                    currentField.Value.Append(' ');
		
		                    // Continue parsing header field value
		                    requestHeaderState = HeaderFieldState.Value;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HeaderFieldState.Value;
		            }
		
		        quit:
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		
		        private class CurrentHeaderFieldStore
		        {
		            private const int DefaultFieldNameAllocation = 128;
		            private const int DefaultFieldValueAllocation = 2 * 1024;
		
		            private static readonly char[] _linearWhiteSpace = { ' ', '\t' };
		
		            private readonly StringBuilder _name = new StringBuilder(DefaultFieldNameAllocation);
		            private readonly StringBuilder _value = new StringBuilder(DefaultFieldValueAllocation);
		
		            public StringBuilder Name
		            {
		                get { return _name; }
		            }
		
		            public StringBuilder Value
		            {
		                get { return _value; }
		            }
		
		            public void CopyTo(HttpHeaders headers)
		            {
		                headers.Add(_name.ToString(), _value.ToString().Trim(_linearWhiteSpace));
		                Clear();
		            }
		
		            public bool IsEmpty()
		            {
		                return _name.Length == 0 && _value.Length == 0;
		            }
		
		            private void Clear()
		            {
		                _name.Clear();
		                _value.Clear();
		            }
		        }
		    }
		}
		#endregion
		
		#region MimeBodyPart
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public class MimeBodyPart : IDisposable
		    {
		        //private static readonly Type _streamType = typeof(Stream);
		        private Stream _outputStream;
		        private MultipartStreamProvider _streamProvider;
		        private HttpContent _parentContent;
		        private HttpContent _content;
		        private HttpContentHeaders _headers;
		
		        public MimeBodyPart(MultipartStreamProvider streamProvider, int maxBodyPartHeaderSize, HttpContent parentContent)
		        {
		            Contract.Assert(streamProvider != null);
		            Contract.Assert(parentContent != null);
		            _streamProvider = streamProvider;
		            _parentContent = parentContent;
		            Segments = new List<ArraySegment<byte>>(2);
		            _headers = FormattingUtilities.CreateEmptyContentHeaders();
		            HeaderParser = new InternetMessageFormatHeaderParser(_headers, maxBodyPartHeaderSize);
		        }
		
		        public InternetMessageFormatHeaderParser HeaderParser { get; private set; }
		
		        public HttpContent GetCompletedHttpContent()
		        {
		            Contract.Assert(IsComplete);
		            if (_content == null) return null;
		            _headers.CopyTo(_content.Headers);
		            return _content;
		        }
		
		        public List<ArraySegment<byte>> Segments { get; private set; }
		
		        public bool IsComplete { get; set; }
		
		        public bool IsFinal { get; set; }
		
		        public async Task WriteSegment(ArraySegment<byte> segment, CancellationToken cancellationToken)
		        {
		            var stream = GetOutputStream();
		            await stream.WriteAsync(segment.Array, segment.Offset, segment.Count, cancellationToken);
		        }
		
		        private Stream GetOutputStream()
		        {
		            if (_outputStream == null)
		            {
		                try
		                {
		                    _outputStream = _streamProvider.GetStream(_parentContent, _headers);
		                }
		                catch (Exception)
		                {
		                    throw new InvalidOperationException("ReadAsMimeMultipartStreamProviderException");
		                }
		
		                if (_outputStream == null)
		                {
		                    throw new InvalidOperationException("ReadAsMimeMultipartStreamProviderNull");
		                }
		
		                if (!_outputStream.CanWrite)
		                {
		                    throw new InvalidOperationException("ReadAsMimeMultipartStreamProviderReadOnly");
		                }
		                _content = new StreamContent(_outputStream);
		            }
		
		            return _outputStream;
		        }
		
		        public void Dispose()
		        {
		            Dispose(true);
		            GC.SuppressFinalize(this);
		        }
		
		        protected void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                CleanupOutputStream();
		                CleanupHttpContent();
		                _parentContent = null;
		                HeaderParser = null;
		                Segments.Clear();
		            }
		        }
		
		        private void CleanupHttpContent()
		        {
		            if (!IsComplete && _content != null) _content.Dispose();
		            _content = null;
		        }
		
		        private void CleanupOutputStream()
		        {
		            if (_outputStream != null)
		            {
		                var output = _outputStream as MemoryStream;
		                if (output != null)
		                {
		                    output.Position = 0;
		                }
		                else
		                {
		                    _outputStream.Close();
		                }
		
		                _outputStream = null;
		            }
		        }
		    }
		}
		#endregion
		
		#region MimeMultipartBodyPartParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    
		    public class MimeMultipartBodyPartParser : IDisposable
		    {
		        public const long DefaultMaxMessageSize = Int64.MaxValue;
		        private const int DefaultMaxBodyPartHeaderSize = 4 * 1024;
		
		        // MIME parser
		        private MimeMultipartParser _mimeParser;
		        private MimeMultipartParser.State _mimeStatus = MimeMultipartParser.State.NeedMoreData;
		        private readonly ArraySegment<byte>[] _parsedBodyPart = new ArraySegment<byte>[2];
		        private MimeBodyPart _currentBodyPart;
		        private bool _isFirst = true;
		
		        // Header field parser
		        private ParserState _bodyPartHeaderStatus = ParserState.NeedMoreData;
		        private readonly int _maxBodyPartHeaderSize;
		
		        // Stream provider
		        private readonly MultipartStreamProvider _streamProvider;
		
		        private readonly HttpContent _content;
		
		        public MimeMultipartBodyPartParser(HttpContent content, MultipartStreamProvider streamProvider) : this(content, streamProvider, DefaultMaxMessageSize, DefaultMaxBodyPartHeaderSize) {}
		
		        public MimeMultipartBodyPartParser(
		            HttpContent content,
		            MultipartStreamProvider streamProvider,
		            long maxMessageSize,
		            int maxBodyPartHeaderSize)
		        {
		            Contract.Assert(content != null, "content cannot be null.");
		            Contract.Assert(streamProvider != null, "streamProvider cannot be null.");
		
		            string boundary = ValidateArguments(content, maxMessageSize, true);
		
		            _mimeParser = new MimeMultipartParser(boundary, maxMessageSize);
		            _currentBodyPart = new MimeBodyPart(streamProvider, maxBodyPartHeaderSize, content);
		            _content = content;
		            _maxBodyPartHeaderSize = maxBodyPartHeaderSize;
		
		            _streamProvider = streamProvider;
		        }
		
		        public static bool IsMimeMultipartContent(HttpContent content)
		        {
		            Contract.Assert(content != null, "content cannot be null.");
		            try
		            {
		                string boundary = ValidateArguments(content, DefaultMaxMessageSize, false);
		                return boundary != null;
		            }
		            catch (Exception)
		            {
		                return false;
		            }
		        }
		
		        public void Dispose()
		        {
		            Dispose(true);
		            GC.SuppressFinalize(this);
		        }
		
		        public IEnumerable<MimeBodyPart> ParseBuffer(byte[] data, int bytesRead)
		        {
		            var bytesConsumed = 0;
		            // There's a special case here - if we've reached the end of the message and there's no optional
		            // CRLF, then we're out of bytes to read, but we have finished the message. 
		            //
		            // If IsWaitingForEndOfMessage is true and we're at the end of the stream, then we're going to 
		            // call into the parser again with an empty array as the buffer to signal the end of the parse. 
		            // Then the final boundary segment will be marked as complete. 
		            if (bytesRead == 0 && !_mimeParser.IsWaitingForEndOfMessage)
		            {
		                CleanupCurrentBodyPart();
		                throw new IOException("ReadAsMimeMultipartUnexpectedTermination");
		            }
		
		            // Make sure we remove an old array segments.
		            _currentBodyPart.Segments.Clear();
		
		            while (_mimeParser.CanParseMore(bytesRead, bytesConsumed))
		            {
		                bool isFinal;
		                _mimeStatus = _mimeParser.ParseBuffer(data, bytesRead, ref bytesConsumed, out _parsedBodyPart[0], out _parsedBodyPart[1], out isFinal);
		                if (_mimeStatus != MimeMultipartParser.State.BodyPartCompleted && _mimeStatus != MimeMultipartParser.State.NeedMoreData)
		                {
		                    CleanupCurrentBodyPart();
		                    throw new InvalidOperationException("ReadAsMimeMultipartParseError");
		                }
		
		                // First body is empty preamble which we just ignore
		                if (_isFirst)
		                {
		                    if (_mimeStatus == MimeMultipartParser.State.BodyPartCompleted) _isFirst = false;
		                    continue;
		                }
		
		                // Parse the two array segments containing parsed body parts that the MIME parser gave us
		                foreach (ArraySegment<byte> part in _parsedBodyPart)
		                {
		                    if (part.Count == 0) continue;
		                    if (_bodyPartHeaderStatus != ParserState.Done)
		                    {
		                        int headerConsumed = part.Offset;
		                        _bodyPartHeaderStatus = _currentBodyPart.HeaderParser.ParseBuffer(part.Array, part.Count + part.Offset, ref headerConsumed);
		                        if (_bodyPartHeaderStatus == ParserState.Done)
		                        {
		                            // Add the remainder as body part content
		                            _currentBodyPart.Segments.Add(new ArraySegment<byte>(part.Array, headerConsumed, part.Count + part.Offset - headerConsumed));
		                        }
		                        else if (_bodyPartHeaderStatus != ParserState.NeedMoreData)
		                        {
		                            CleanupCurrentBodyPart();
		                            throw new InvalidOperationException("ReadAsMimeMultipartHeaderParseError");
		                        }
		                    }
		                    else
		                    {
		                        // Add the data as body part content
		                        _currentBodyPart.Segments.Add(part);
		                    }
		                }
		
		                if (_mimeStatus == MimeMultipartParser.State.BodyPartCompleted)
		                {
		                    // If body is completed then swap current body part
		                    MimeBodyPart completed = _currentBodyPart;
		                    completed.IsComplete = true;
		                    completed.IsFinal = isFinal;
		
		                    _currentBodyPart = new MimeBodyPart(_streamProvider, _maxBodyPartHeaderSize, _content);
		
		                    _mimeStatus = MimeMultipartParser.State.NeedMoreData;
		                    _bodyPartHeaderStatus = ParserState.NeedMoreData;
		                    yield return completed;
		                }
		                else
		                {
		                    // Otherwise return what we have 
		                    yield return _currentBodyPart;
		                }
		            }
		        }
		
		        /// <summary>
		        /// Releases unmanaged and - optionally - managed resources
		        /// </summary>
		        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		        protected void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                _mimeParser = null;
		                CleanupCurrentBodyPart();
		            }
		        }
		
		        private static string ValidateArguments(HttpContent content, long maxMessageSize, bool throwOnError)
		        {
		            Contract.Assert(content != null, "content cannot be null.");
		            if (maxMessageSize < MimeMultipartParser.MinMessageSize)
		            {
		                if (throwOnError) throw new ArgumentOutOfRangeException("maxMessageSize");
		                else return null;
		            }
		
		            MediaTypeHeaderValue contentType = content.Headers.ContentType;
		            if (contentType == null)
		            {
		                if (throwOnError) throw new ArgumentException("ReadAsMimeMultipartArgumentNoContentType", "content");
		                else return null;
		            }
		
		            if (!contentType.MediaType.StartsWith("multipart", StringComparison.OrdinalIgnoreCase))
		            {
		                if (throwOnError) throw new ArgumentException("ReadAsMimeMultipartArgumentNoMultipart", "content");
		                else return null;
		            }
		
		            string boundary = null;
		            foreach (NameValueHeaderValue p in contentType.Parameters)
		            {
		                if (p.Name.Equals("boundary", StringComparison.OrdinalIgnoreCase))
		                {
		                    boundary = FormattingUtilities.UnquoteToken(p.Value);
		                    break;
		                }
		            }
		
		            if (boundary == null)
		            {
		                if (throwOnError) throw new ArgumentException("ReadAsMimeMultipartArgumentNoBoundary", "content"); 
		                else return null;
		            }
		
		            return boundary;
		        }
		
		        private void CleanupCurrentBodyPart()
		        {
		            if (_currentBodyPart != null)
		            {
		                _currentBodyPart.Dispose();
		                _currentBodyPart = null;
		            }
		        }
		    }
		}
		#endregion
		
		#region MimeMultipartParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics;
		    using System.Diagnostics.CodeAnalysis;
		    using System.Diagnostics.Contracts;
		    using System.Globalization;
		    using System.Text;
		
		    public class MimeMultipartParser
		    {
		        public const int MinMessageSize = 10;
		
		        private const int MaxBoundarySize = 256;
		
		        // ReSharper disable InconsistentNaming
		        private const byte HTAB = 0x09;
		        private const byte SP = 0x20;
		        private const byte CR = 0x0D;
		        private const byte LF = 0x0A;
		        // ReSharper restore InconsistentNaming
		        private const byte Dash = 0x2D;
		        private static readonly ArraySegment<byte> _emptyBodyPart = new ArraySegment<byte>(new byte[0]);
		
		        private long _totalBytesConsumed;
		        private readonly long _maxMessageSize;
		
		        private BodyPartState _bodyPartState;
		        private readonly CurrentBodyPartStore _currentBoundary;
		
		        public MimeMultipartParser(string boundary, long maxMessageSize)
		        {
		            // The minimum length which would be an empty message terminated by CRLF
		            if (maxMessageSize < MinMessageSize) throw new ArgumentOutOfRangeException("maxMessageSize");
		            if (String.IsNullOrWhiteSpace(boundary)) throw new ArgumentNullException("boundary");
		            if (boundary.Length > MaxBoundarySize - 10) throw new ArgumentOutOfRangeException("boundary");
		            if (boundary.EndsWith(" ", StringComparison.Ordinal)) throw new ArgumentException("MimeMultipartParserBadBoundary", "boundary");
		
		            _maxMessageSize = maxMessageSize;
		            _currentBoundary = new CurrentBodyPartStore(boundary);
		            _bodyPartState = BodyPartState.AfterFirstLineFeed;
		        }
		
		        public bool IsWaitingForEndOfMessage
		        {
		            get
		            {
		                return
		                    _bodyPartState == BodyPartState.AfterBoundary &&
		                    _currentBoundary != null &&
		                    _currentBoundary.IsFinal;
		            }
		        }
		
		        private enum BodyPartState
		        {
		            BodyPart = 0,
		            AfterFirstCarriageReturn,
		            AfterFirstLineFeed,
		            AfterFirstDash,
		            Boundary,
		            AfterBoundary,
		            AfterSecondDash,
		            AfterSecondCarriageReturn
		        }
		
		        // ReSharper disable once UnusedMember.Local
		        private enum MessageState
		        {
		            Boundary = 0, // about to parse boundary
		            BodyPart, // about to parse body-part
		            CloseDelimiter // about to read close-delimiter
		        }
		
		        public enum State
		        {
		            /// <summary>
		            /// Need more data
		            /// </summary>
		            NeedMoreData = 0,
		
		            /// <summary>
		            /// Parsing of a complete body part succeeded.
		            /// </summary>
		            BodyPartCompleted,
		
		            /// <summary>
		            /// Bad data format
		            /// </summary>
		            Invalid,
		
		            /// <summary>
		            /// Data exceeds the allowed size
		            /// </summary>
		            DataTooBig,
		        }
		
		        public bool CanParseMore(int bytesRead, int bytesConsumed)
		        {
		            if (bytesConsumed < bytesRead)
		            {
		                // If there's more bytes we haven't parsed, then we can parse more
		                return true;
		            }
		
		            if (bytesRead == 0 && IsWaitingForEndOfMessage)
		            {
		                // If we're waiting for the end of the message and we've arrived there, we want parse to be called
		                // again so we can mark the parse as complete.
		                //
		                // This can happen when the last boundary segment doesn't have a trailing CRLF. We need to wait until
		                // the end of the message to complete the parse because we need to consume any trailing whitespace that's
		                //present.
		                return true;
		            }
		
		            return false;
		        }
		
		        public State ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            out ArraySegment<byte> remainingBodyPart,
		            out ArraySegment<byte> bodyPart,
		            out bool isFinalBodyPart)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            State parseStatus;
		            isFinalBodyPart = false;
		
		            try
		            {
		                parseStatus = ParseBodyPart(
		                    buffer,
		                    bytesReady,
		                    ref bytesConsumed,
		                    ref _bodyPartState,
		                    _maxMessageSize,
		                    ref _totalBytesConsumed,
		                    _currentBoundary);
		            }
		            catch (Exception)
		            {
		                parseStatus = State.Invalid;
		            }
		
		            remainingBodyPart = _currentBoundary.GetDiscardedBoundary();
		            bodyPart = _currentBoundary.BodyPart;
		            if (parseStatus == State.BodyPartCompleted)
		            {
		                isFinalBodyPart = _currentBoundary.IsFinal;
		                _currentBoundary.ClearAll();
		            }
		            else
		            {
		                _currentBoundary.ClearBodyPart();
		            }
		
		            return parseStatus;
		        }
		
		        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "This is a parser which cannot be split up for performance reasons.")]
		        private static State ParseBodyPart(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            ref BodyPartState bodyPartState,
		            long maximumMessageLength,
		            ref long totalBytesConsumed,
		            CurrentBodyPartStore currentBodyPart)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseBodyPart()|(bytesReady - bytesConsumed) < 0");
		            Contract.Assert(maximumMessageLength <= 0 || totalBytesConsumed <= maximumMessageLength, "ParseBodyPart()|Message already read exceeds limit.");
		
		            // Remember where we started.
		            int segmentStart;
		            var initialBytesParsed = bytesConsumed;
		
		            if (bytesReady == 0 && bodyPartState == BodyPartState.AfterBoundary && currentBodyPart.IsFinal)
		            {
		                // We've seen the end of the stream - the final body part has no trailing CRLF
		                return State.BodyPartCompleted;
		            }
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            State parseStatus = State.DataTooBig;
		            var effectiveMax = maximumMessageLength <= 0 ? Int64.MaxValue : (maximumMessageLength - totalBytesConsumed + bytesConsumed);
		            if (effectiveMax == 0)
		            {
		                // effectiveMax is based on our max message size - if we've arrrived at the max size, then we need
		                // to stop parsing.
		                return State.DataTooBig;
		            }
		
		            if (bytesReady <= effectiveMax)
		            {
		                parseStatus = State.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            currentBodyPart.ResetBoundaryOffset();
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (bodyPartState)
		            {
		                case BodyPartState.BodyPart:
		                    while (buffer[bytesConsumed] != CR)
		                    {
		                        if (++bytesConsumed == effectiveMax) goto quit;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(CR);
		
		                    // Move past the CR
		                    bodyPartState = BodyPartState.AfterFirstCarriageReturn;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.AfterFirstCarriageReturn;
		
		                case BodyPartState.AfterFirstCarriageReturn:
		                    if (buffer[bytesConsumed] != LF)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(LF);
		
		                    // Move past the CR
		                    bodyPartState = BodyPartState.AfterFirstLineFeed;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.AfterFirstLineFeed;
		
		                case BodyPartState.AfterFirstLineFeed:
		                    if (buffer[bytesConsumed] == CR)
		                    {
		                        // Remember potential boundary
		                        currentBodyPart.ResetBoundary();
		                        currentBodyPart.AppendBoundary(CR);
		
		                        // Move past the CR
		                        bodyPartState = BodyPartState.AfterFirstCarriageReturn;
		                        if (++bytesConsumed == effectiveMax) goto quit;
		
		                        goto case BodyPartState.AfterFirstCarriageReturn;
		                    }
		
		                    if (buffer[bytesConsumed] != Dash)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(Dash);
		
		                    // Move past the Dash
		                    bodyPartState = BodyPartState.AfterFirstDash;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.AfterFirstDash;
		
		                case BodyPartState.AfterFirstDash:
		                    if (buffer[bytesConsumed] != Dash)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(Dash);
		
		                    // Move past the Dash
		                    bodyPartState = BodyPartState.Boundary;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.Boundary;
		
		                case BodyPartState.Boundary:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != CR)
		                    {
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            if (currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                            {
		                                if (currentBodyPart.IsBoundaryComplete())
		                                {
		                                    // At this point we've seen the end of a boundary segment that is aligned at the end
		                                    // of the buffer - this might be because we have another segment coming or it might
		                                    // truly be the end of the message.
		                                    bodyPartState = BodyPartState.AfterBoundary;
		                                }
		                            }
		                            else
		                            {
		                                currentBodyPart.ResetBoundary();
		                                bodyPartState = BodyPartState.BodyPart;
		                            }
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        if (!currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                        {
		                            currentBodyPart.ResetBoundary();
		                            bodyPartState = BodyPartState.BodyPart;
		                            goto case BodyPartState.BodyPart;
		                        }
		                    }
		
		                    goto case BodyPartState.AfterBoundary;
		
		                case BodyPartState.AfterBoundary:
		
		                    // This state means that we just saw the end of a boundary. It might by a 'normal' boundary, in which
		                    // case it's followed by optional whitespace and a CRLF. Or it might be the 'final' boundary and will 
		                    // be followed by '--', optional whitespace and an optional CRLF.
		                    if (buffer[bytesConsumed] == Dash && !currentBodyPart.IsFinal)
		                    {
		                        currentBodyPart.AppendBoundary(Dash);
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            bodyPartState = BodyPartState.AfterSecondDash;
		                            goto quit;
		                        }
		
		                        goto case BodyPartState.AfterSecondDash;
		                    }
		
		                    // Capture optional whitespace
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != CR)
		                    {
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            if (!currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                            {
		                                // It's an unexpected character
		                                currentBodyPart.ResetBoundary();
		                                bodyPartState = BodyPartState.BodyPart;
		                            }
		
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        if (!currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                        {
		                            currentBodyPart.ResetBoundary();
		                            bodyPartState = BodyPartState.BodyPart;
		                            goto case BodyPartState.BodyPart;
		                        }
		                    }
		
		                    if (buffer[bytesConsumed] == CR)
		                    {
		                        currentBodyPart.AppendBoundary(CR);
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            bodyPartState = BodyPartState.AfterSecondCarriageReturn;
		                            goto quit;
		                        }
		
		                        goto case BodyPartState.AfterSecondCarriageReturn;
		                    }
		                    else
		                    {
		                        // It's an unexpected character
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                case BodyPartState.AfterSecondDash:
		                    if (buffer[bytesConsumed] == Dash)
		                    {
		                        currentBodyPart.AppendBoundary(Dash);
		                        bytesConsumed++;
		
		                        if (currentBodyPart.IsBoundaryComplete())
		                        {
		                            Debug.Assert(currentBodyPart.IsFinal);
		
		                            // If we get in here, it means we've see the trailing '--' of the last boundary - in order to consume all of the 
		                            // remaining bytes, we don't mark the parse as complete again - wait until this method is called again with the 
		                            // empty buffer to do that.
		                            bodyPartState = BodyPartState.AfterBoundary;
		                            parseStatus = State.NeedMoreData;
		                            goto quit;
		                        }
		                        else
		                        {
		                            currentBodyPart.ResetBoundary();
		                            if (bytesConsumed == effectiveMax) goto quit;
		
		                            goto case BodyPartState.BodyPart;
		                        }
		                    }
		                    else
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                case BodyPartState.AfterSecondCarriageReturn:
		                    if (buffer[bytesConsumed] != LF)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    currentBodyPart.AppendBoundary(LF);
		                    bytesConsumed++;
		
		                    bodyPartState = BodyPartState.BodyPart;
		                    if (currentBodyPart.IsBoundaryComplete())
		                    {
		                        parseStatus = State.BodyPartCompleted;
		                        goto quit;
		                    }
		                    else
		                    {
		                        currentBodyPart.ResetBoundary();
		                        if (bytesConsumed == effectiveMax) goto quit;
		
		                        goto case BodyPartState.BodyPart;
		                    }
		            }
		
		        quit:
		            if (initialBytesParsed < bytesConsumed)
		            {
		                int boundaryLength = currentBodyPart.BoundaryDelta;
		                if (boundaryLength > 0 && parseStatus != State.BodyPartCompleted)
		                {
		                    currentBodyPart.HasPotentialBoundaryLeftOver = true;
		                }
		
		                int bodyPartEnd = bytesConsumed - initialBytesParsed - boundaryLength;
		
		                currentBodyPart.BodyPart = new ArraySegment<byte>(buffer, initialBytesParsed, bodyPartEnd);
		            }
		
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		
		        private class CurrentBodyPartStore
		        {
		            private const int InitialOffset = 2;
		
		            private readonly byte[] _boundaryStore = new byte[MaxBoundarySize];
		            private int _boundaryStoreLength;
		
		            private readonly byte[] _referenceBoundary = new byte[MaxBoundarySize];
		            private readonly int _referenceBoundaryLength;
		
		            private readonly byte[] _boundary = new byte[MaxBoundarySize];
		            private int _boundaryLength;
		
		            private ArraySegment<byte> _bodyPart = _emptyBodyPart;
		            private bool _isFinal;
		            private bool _isFirst = true;
		            private bool _releaseDiscardedBoundary;
		            private int _boundaryOffset;
		
		            public CurrentBodyPartStore(string referenceBoundary)
		            {
		                Contract.Assert(referenceBoundary != null);
		
		                _referenceBoundary[0] = CR;
		                _referenceBoundary[1] = LF;
		                _referenceBoundary[2] = Dash;
		                _referenceBoundary[3] = Dash;
		                _referenceBoundaryLength = 4 + Encoding.UTF8.GetBytes(referenceBoundary, 0, referenceBoundary.Length, _referenceBoundary, 4);
		
		                _boundary[0] = CR;
		                _boundary[1] = LF;
		                _boundaryLength = InitialOffset;
		            }
		
		            public bool HasPotentialBoundaryLeftOver { get; set; }
		
		            public int BoundaryDelta
		            {
		                get { return (_boundaryLength - _boundaryOffset > 0) ? _boundaryLength - _boundaryOffset : _boundaryLength; }
		            }
		
		            public ArraySegment<byte> BodyPart
		            {
		                get { return _bodyPart; }
		                set { _bodyPart = value; }
		            }
		
		            public bool IsFinal
		            {
		                get { return _isFinal; }
		            }
		
		            public void ResetBoundaryOffset()
		            {
		                _boundaryOffset = _boundaryLength;
		            }
		
		            public void ResetBoundary()
		            {
		                // If we had a potential boundary left over then store it so that we don't loose it
		                if (HasPotentialBoundaryLeftOver)
		                {
		                    Buffer.BlockCopy(_boundary, 0, _boundaryStore, 0, _boundaryOffset);
		                    _boundaryStoreLength = _boundaryOffset;
		                    HasPotentialBoundaryLeftOver = false;
		                    _releaseDiscardedBoundary = true;
		                }
		
		                _boundaryLength = 0;
		                _boundaryOffset = 0;
		            }
		
		            public void AppendBoundary(byte data)
		            {
		                _boundary[_boundaryLength++] = data;
		            }
		
		            public bool AppendBoundary(byte[] data, int offset, int count)
		            {
		                // Check that potential boundary is not bigger than our reference boundary. 
		                // Allow for 2 extra characters to include the final boundary which ends with 
		                // an additional "--" sequence + plus up to 4 LWS characters (which are allowed). 
		                if (_boundaryLength + count > _referenceBoundaryLength + 6) return false;
		
		                var cnt = _boundaryLength;
		                Buffer.BlockCopy(data, offset, _boundary, _boundaryLength, count);
		                _boundaryLength += count;
		
		                // Verify that boundary matches so far
		                var maxCount = Math.Min(_boundaryLength, _referenceBoundaryLength);
		                for (; cnt < maxCount; cnt++)
		                {
		                    if (_boundary[cnt] != _referenceBoundary[cnt]) return false;
		                }
		
		                return true;
		            }
		
		            public ArraySegment<byte> GetDiscardedBoundary()
		            {
		                if (_boundaryStoreLength > 0 && _releaseDiscardedBoundary)
		                {
		                    ArraySegment<byte> discarded = new ArraySegment<byte>(_boundaryStore, 0, _boundaryStoreLength);
		                    _boundaryStoreLength = 0;
		                    return discarded;
		                }
		
		                return _emptyBodyPart;
		            }
		
		            public bool IsBoundaryValid()
		            {
		                var offset = 0;
		                if (_isFirst)
		                {
		                    offset = InitialOffset;
		                }
		
		                var count = offset;
		                for (; count < _referenceBoundaryLength; count++)
		                {
		                    if (_boundary[count] != _referenceBoundary[count])
		                    {
		                        return false;
		                    }
		                }
		
		                // Check for final
		                var boundaryIsFinal = false;
		                if (_boundary[count] == Dash &&
		                    _boundary[count + 1] == Dash)
		                {
		                    boundaryIsFinal = true;
		                    count += 2;
		                }
		
		                // Rest of boundary must be ignorable whitespace in order for it to match
		                for (; count < _boundaryLength - 2; count++)
		                {
		                    if (_boundary[count] != SP && _boundary[count] != HTAB)
		                    {
		                        return false;
		                    }
		                }
		
		                // We have a valid boundary so whatever we stored in the boundary story is no longer needed
		                _isFinal = boundaryIsFinal;
		                _isFirst = false;
		
		                return true;
		            }
		
		            public bool IsBoundaryComplete()
		            {
		                if (!IsBoundaryValid()) return false;
		                if (_boundaryLength < _referenceBoundaryLength) return false;
		                if (_boundaryLength == _referenceBoundaryLength + 1 && _boundary[_referenceBoundaryLength] == Dash) return false;
		                return true;
		            }
		
		            public void ClearBodyPart()
		            {
		                BodyPart = _emptyBodyPart;
		            }
		
		            public void ClearAll()
		            {
		                _releaseDiscardedBoundary = false;
		                HasPotentialBoundaryLeftOver = false;
		                _boundaryLength = 0;
		                _boundaryOffset = 0;
		                _boundaryStoreLength = 0;
		                _isFinal = false;
		                ClearBodyPart();
		            }
		
		            private string DebuggerToString()
		            {
		                var referenceBoundary = Encoding.UTF8.GetString(_referenceBoundary, 0, _referenceBoundaryLength);
		                var boundary = Encoding.UTF8.GetString(_boundary, 0, _boundaryLength);
		
		                return String.Format(
		                    CultureInfo.InvariantCulture,
		                    "Expected: {0} *** Current: {1}",
		                    referenceBoundary,
		                    boundary);
		            }
		        }
		    }
		}
		#endregion
		
		#region MultipartContent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections;
		    using System.Collections.Generic;
		    using System.IO;
		    using System.Linq;
		    using System.Net;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Reflection;
		    using System.Text;
		    using System.Threading.Tasks;
		
		    public class MultipartContent : HttpContent, IEnumerable<HttpContent>
		    {
		        private readonly List<HttpContent> _nestedContent;
		        private readonly string _boundary;
		        private int _nextContentIndex;
		        private Stream _outputStream;
		        private TaskCompletionSource<object> _tcs;
		        //private const string crlf = "\r\n";
		
		        public MultipartContent() : this("mixed", GetDefaultBoundary()){}
		
		        public MultipartContent(string subtype) : this(subtype, GetDefaultBoundary()) {}
		
		        public MultipartContent(string subtype, string boundary)
		        {
		            if (string.IsNullOrWhiteSpace(subtype)) throw new ArgumentException("subtype");
		            ValidateBoundary(boundary);
		            _boundary = boundary;
		            var str = boundary;
		            if (!str.StartsWith("\"", StringComparison.Ordinal)) str = "\"" + str + "\"";
		            Headers.ContentType = new MediaTypeHeaderValue("multipart/" + subtype)
		            {
		                Parameters = { new NameValueHeaderValue("boundary", str) }
		            };
		            _nestedContent = new List<HttpContent>();
		        }
		
		        public virtual void Add(HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            _nestedContent.Add(content);
		        }
		
		        protected override void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                foreach (HttpContent httpContent in _nestedContent) httpContent.Dispose();
		                _nestedContent.Clear();
		            }
		            base.Dispose(disposing);
		        }
		
		        public IEnumerator<HttpContent> GetEnumerator()
		        {
		            return _nestedContent.GetEnumerator();
		        }
		
		        IEnumerator IEnumerable.GetEnumerator()
		        {
		            return _nestedContent.GetEnumerator();
		        }
		
		        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
		        {
		            TaskCompletionSource<object> completionSource = new TaskCompletionSource<object>();
		            _tcs = completionSource;
		            _outputStream = stream;
		            _nextContentIndex = 0;
		            EncodeStringToStreamAsync(_outputStream, "--" + _boundary + "\r\n").ContinueWithStandard(WriteNextContentHeadersAsync);
		            return completionSource.Task;
		        }
		
		        protected override bool TryComputeLength(out long length)
		        {
		            const long num1 = 0L;
		            var num2 = (long) GetEncodedLength("\r\n--" + _boundary + "\r\n");
		            var num3 = num1 + GetEncodedLength("--" + _boundary + "\r\n");
		            var flag = true;
		            foreach (var httpContent in _nestedContent)
		            {
		                if (flag) flag = false;
		                else num3 += num2;
		
		                foreach (var keyValuePair in httpContent.Headers) num3 += GetEncodedLength(keyValuePair.Key + ": " + string.Join(", ", keyValuePair.Value) + "\r\n");
		                num3 += "\r\n".Length;
		            
		                // ReSharper disable once ConvertToConstant.Local
		                var length1 = 0L;
		                var methodInfo = httpContent.GetType().GetMethod("TryComputeLength", BindingFlags.Instance | BindingFlags.NonPublic);
		                var result = (bool)methodInfo.Invoke(httpContent, new[] {/*out*/(object)length1}); 
		                if (result)
		                {
		                    length = 0L;
		                    return false;
		                }
		
		                num3 += length1;
		            }
		            var num4 = num3 + GetEncodedLength("\r\n--" + _boundary + "--\r\n");
		            length = num4;
		            return true;
		        }
		
		        private static void ValidateBoundary(string boundary)
		        {
		            if (string.IsNullOrWhiteSpace(boundary)) throw new ArgumentException("boundary");
		            if (boundary.Length > 70) throw new ArgumentOutOfRangeException("boundary", "boundary is longer than 70 characters");
		            if (boundary.EndsWith(" ", StringComparison.Ordinal)) throw new ArgumentException("boundary ends with blank", "boundary");
		            if (boundary.Any(ch => (48 > ch || ch > 57) && (97 > ch || ch > 122) && ((65 > ch || ch > 90) && "'()+_,-./:=? ".IndexOf(ch) < 0))) throw new ArgumentException("boundary contains invalid characters", "boundary");
		        }
		
		        private static string GetDefaultBoundary()
		        {
		            return Guid.NewGuid().ToString();
		        }
		
		        private void WriteNextContentHeadersAsync(Task task)
		        {
		            if (task.IsFaulted)
		            {
		                // ReSharper disable once PossibleNullReferenceException
		                HandleAsyncException(task.Exception.GetBaseException());
		            }
		            else
		            {
		                try
		                {
		                    if (_nextContentIndex >= _nestedContent.Count)
		                    {
		                        WriteTerminatingBoundaryAsync();
		                    }
		                    else
		                    {
		                        var str = "\r\n--" + _boundary + "\r\n";
		                        var stringBuilder = new StringBuilder();
		                        if (_nextContentIndex != 0) stringBuilder.Append(str);
		                        foreach (var keyValuePair in _nestedContent[_nextContentIndex].Headers) stringBuilder.Append(keyValuePair.Key + ": " + string.Join(", ", keyValuePair.Value) + "\r\n");
		                        stringBuilder.Append("\r\n");
		                        EncodeStringToStreamAsync(_outputStream, stringBuilder.ToString()).ContinueWithStandard(WriteNextContentAsync);
		                    }
		                }
		                catch (Exception ex)
		                {
		                  HandleAsyncException(ex);
		                }
		            }
		        }
		
		        private void WriteNextContentAsync(Task task)
		        {
		            if (task.IsFaulted)
		            {
		                // ReSharper disable once PossibleNullReferenceException
		                HandleAsyncException(task.Exception.GetBaseException());
		            }
		            else
		            {
		                try
		                {
		                    var httpContent = _nestedContent[_nextContentIndex];
		                    ++_nextContentIndex;
		                    httpContent.CopyToAsync(_outputStream).ContinueWithStandard(WriteNextContentHeadersAsync);
		                }
		                catch (Exception ex)
		                {
		                    HandleAsyncException(ex);
		                }
		            }
		        }
		
		        private void WriteTerminatingBoundaryAsync()
		        {
		            try
		            {
		                EncodeStringToStreamAsync(_outputStream, "\r\n--" + _boundary + "--\r\n").ContinueWithStandard(task =>
		                {
		                    // ReSharper disable once PossibleNullReferenceException
		                    if (task.IsFaulted) HandleAsyncException(task.Exception.GetBaseException());
		                    else CleanupAsync().TrySetResult(null);
		                });
		            }
		            catch (Exception ex)
		            {
		                HandleAsyncException(ex);
		            }
		        }
		
		        private static Task EncodeStringToStreamAsync(Stream stream, string input)
		        {
		            var bytes = Encoding.GetEncoding(28591).GetBytes(input);
		            return Task.Factory.FromAsync(stream.BeginWrite, stream.EndWrite, bytes, 0, bytes.Length, null);
		        }
		
		        private TaskCompletionSource<object> CleanupAsync()
		        {
		            var completionSource = _tcs;
		            _outputStream = null;
		            _nextContentIndex = 0;
		            _tcs = null;
		            return completionSource;
		        }
		
		        private void HandleAsyncException(Exception ex)
		        {
		            CleanupAsync().TrySetException(ex);
		        }
		
		        private static int GetEncodedLength(string input)
		        {
		            return Encoding.GetEncoding(28591).GetByteCount(input);
		        }
		    }
		}
		
		#endregion
		
		#region MultipartMemoryStreamProvider
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		
		    public class MultipartMemoryStreamProvider : MultipartStreamProvider
		    {
		        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
		        {
		            if (parent == null) throw new ArgumentNullException("parent");
		            if (headers == null) throw new ArgumentNullException("headers");
		            return new MemoryStream();
		        }
		    }
		}
		#endregion
		
		#region MultipartStreamProvider
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Collections.ObjectModel;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public abstract class MultipartStreamProvider
		    {
		        private struct AsyncVoid{}
		        
		        private readonly Collection<HttpContent> _contents = new Collection<HttpContent>();
		
		        public Collection<HttpContent> Contents
		        {
		            get { return _contents; }
		        }
		
		        public abstract Stream GetStream(HttpContent parent, HttpContentHeaders headers);
		
		        public virtual Task ExecutePostProcessingAsync()
		        {
		            return Task.FromResult(default(AsyncVoid));
		        }
		
		        public virtual Task ExecutePostProcessingAsync(CancellationToken cancellationToken)
		        {
		            // Call the other overload to maintain backward compatibility.
		            // ReSharper disable once MethodSupportsCancellation
		            return ExecutePostProcessingAsync();
		        }
		    }
		}
		#endregion
		
		#region TaskHelperExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Threading.Tasks;
		    
		    public static class TaskHelperExtensions
		    {
		        public static async Task<object> CastToObject(this Task task)
		        {
		            await task;
		            return null;
		        }
		
		        public static async Task<object> CastToObject<T>(this Task<T> task)
		        {
		            return (object)await task;
		        }
		
		        public static void ThrowIfFaulted(this Task task)
		        {
		            task.GetAwaiter().GetResult();
		        }
		
		        public static bool TryGetResult<ResultT>(this Task<ResultT> task, out ResultT result)
		        {
		            if (task.Status == TaskStatus.RanToCompletion)
		            {
		                result = task.Result;
		                return true;
		            }
		
		            result = default(ResultT);
		            return false;
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region ReflectionMapper
		
		#region Attributes
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    
		    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
		    public class NotRComparedAttribute : Attribute { }
		
		    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
		    public class NotRMappedAttribute : Attribute { }
		
		    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
		    public class NotRMappedToAttribute : Attribute { }
		
		    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
		    public class NotRMappedFromAttribute : Attribute { }
		    
		    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
		    public class RMapToAttribute : Attribute
		    {
		        public RMapToAttribute(){}
		        public RMapToAttribute(string objectPath)
		        {
		            ObjectPath = objectPath;
		        }
		        public RMapToAttribute(string objectPath, string propertyName) : this(objectPath)
		        {
		            PropertyName = propertyName;
		        }
		        public string ObjectPath { get; set; }
		        public string PropertyName { get; set; }
		    }    
		}
		#endregion
		
		#region Exceptions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		
		    public class ReflectionMapperException : Exception
		    {
		        public ReflectionMapperException() { }
		        public ReflectionMapperException(string msg) : base(msg) { }
		    }
		
		    public class ReflectionPropertyCantBeInvoked : ReflectionMapperException
		    {
		        public ReflectionPropertyCantBeInvoked(Type type, string propertyName)
		            : base(string.Format("Property '{0}' does not exist in Type '{1}'", propertyName, type.Name)) { }
		    }
		
		    public class ReflectionMethodCantBeInvoked : ReflectionMapperException
		    {
		        public ReflectionMethodCantBeInvoked(Type type, string methodName)
		            : base(string.Format("Method '{0}' does not exist in Type '{1}'", methodName, type.Name)) { }
		    }    
		    
		    public class PropertyCantBeAutomappedException : ReflectionMapperException
		    {
		        public PropertyCantBeAutomappedException(string msg) : base(msg) { }
		    }
		}
		#endregion
		
		#region IRMapperCustom
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		
		    public interface IRMapperCustom
		    {
		        //must also have a default constructor!
		        object MapFromObjectCustom(object obj, Type objType);
		        object MapToObjectCustom(object obj, Type objType);    
		    }
		}
		#endregion
		
		#region ReflectionHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.ComponentModel;
		    using System.Diagnostics;
		    using System.Linq;
		    using System.Reflection;
		    using System.Text.RegularExpressions;
		
		    public static class ReflectionHelper
		    {
		        #region Methods
		        public static string GetThrowingContext()
		        {
		            var stackFrame = new StackTrace().GetFrame(2);
		            return stackFrame.GetMethod().DeclaringType + "::" + stackFrame.GetMethod().Name + "()";
		        }
		        public static string GetCurrentContext()
		        {
		            var stackFrame = new StackTrace().GetFrame(1);
		            return stackFrame.GetMethod().DeclaringType + "::" + stackFrame.GetMethod().Name + "()";
		        }
		        public static object CreateType(Type type, params object[] args)
		        {
		            if (type.IsGenericType)
		            {
		                return CreateGenericType(type.GetGenericTypeDefinition(), type.GetGenericArguments());
		            }
		            else
		            {
		                return Activator.CreateInstance(type, args);
		            }
		        }
		        public static object CreateGenericType(Type genericType, Type[] innerTypes, params object[] args)
		        {
		            var specificType = genericType.MakeGenericType(innerTypes);
		            return Activator.CreateInstance(specificType, args);
		        }
		        public static object CreateGenericType(Type genericType, Type innerType, params object[] args)
		        {
		            return CreateGenericType(genericType, new[] {innerType}, args);
		        }
		        public static object ExecuteStaticMethod(Type typeofClassWithStaticMethod, string methodName, params object[] args)
		        {
		            var methodInfo = typeofClassWithStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.Public).Single(m => m.Name == methodName && !m.IsGenericMethod);
		            return methodInfo.Invoke(null, args);
		        }
		        
		        public static object ExecuteNonPublicStaticMethod(Type typeofClassWithStaticMethod, string methodName, params object[] args)
		        {
		            var methodInfo = typeofClassWithStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Single(m => m.Name == methodName && !m.IsGenericMethod);
		            return methodInfo.Invoke(null, args);
		        }
		        
		        public static object ExecuteStaticGenericMethod(Type typeofClassWithGenericStaticMethod, string methodName, Type[] genericArguments, params object[] args)
		        {
		            var methodInfo = typeofClassWithGenericStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.Public).Single(m => m.Name == methodName && m.IsGenericMethod);
		            var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		            return genericMethodInfo.Invoke(null, args);
		        }
		
		        public static object ExecuteNonPublicStaticGenericMethod(Type typeofClassWithGenericStaticMethod, string methodName, Type[] genericArguments, params object[] args)
		        {
		            var methodInfo = typeofClassWithGenericStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Single(m => m.Name == methodName && m.IsGenericMethod);
		            var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		            return genericMethodInfo.Invoke(null, args);
		        }
		
		        public static bool IsClassADerivedFromClassB(Type a, Type b)
		        {
		            return IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(a, b) != null;
		        }
		        public static Type IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(Type a, Type b)
		        {
		            if (a == b) return a;
		            var aBaseType = a.BaseType;
		            if (aBaseType == null) return null;
		
		            if (b.IsGenericTypeDefinition && aBaseType.IsGenericType)
		            {
		                if (aBaseType.GetGenericTypeDefinition() == b) return aBaseType;
		            }
		            else
		            {
		                if (aBaseType == b) return aBaseType;
		            }
		            return IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(aBaseType, b);
		        }
		        #endregion
		
		        #region Extnesions for Fluent interface
		        public static string InsertSpacesBetweenWords(this string str)
		        {
		            var result = Regex.Replace(str, @"(\B[A-Z][^A-Z]+)|\B(?<=[^A-Z]+)([A-Z]+)(?![^A-Z])", " $1$2");
		            return result
		                .Replace(" Or ", " or ")
		                .Replace(" And ", " and ")
		                .Replace(" Of ", " of ")
		                .Replace(" On ", " on ")
		                .Replace(" The ", " the ")
		                .Replace(" For ", " for ")
		                .Replace(" At ", " at ")
		                .Replace(" A ", " a ")
		                .Replace(" In ", " in ")
		                .Replace(" By ", " by ")
		                .Replace(" About ", " about ")
		                .Replace(" To ", " to ")
		                .Replace(" From ", " from ")
		                .Replace(" With ", " with ")
		                .Replace(" Over ", " over ")
		                .Replace(" Into ", " into ")
		                .Replace(" Without ", " without ");
		        }
		
		        public static string GetTypeDescription(this Type type)
		        {
		            var attr = Attribute.GetCustomAttribute(type, typeof(DescriptionAttribute), true);
		            return attr != null ? ((DescriptionAttribute)attr).Description : type.ToString().InsertSpacesBetweenWords();
		        }
		        public static string GetTypeFriendlyDescription(this Type type)
		        {
		            var attr = Attribute.GetCustomAttribute(type, typeof(DescriptionAttribute), true);
		            return attr != null ? ((DescriptionAttribute)attr).Description : type.Name.InsertSpacesBetweenWords();
		        }
		        public static string GetDisplayNameForProperty(this Type type, string propertyName)
		        {
		            var propertyInfo = type.GetProperty(propertyName);
		            var attr = propertyInfo.GetCustomAttribute(typeof (DisplayNameAttribute), true);
		            return attr != null ? ((DisplayNameAttribute)attr).DisplayName : propertyName.InsertSpacesBetweenWords();
		        }
		        public static object ExecuteGenericMethod(this object me, string methodName, Type[] genericArguments, params object[] args)
		        {
		            try
		            {
		                var methodInfo = me.GetType().GetMethods().Single(x => x.Name == methodName && x.IsGenericMethod);
		                var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		                return genericMethodInfo.Invoke(me, args);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		        }
		        public static object ExecuteNonPublicGenericMethod(this object me, string methodName, Type[] genericArguments, params object[] args)
		        {
		            try
		            {
		                var methodInfo = me.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Single(x => x.Name == methodName && x.IsGenericMethod);
		                var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		                return genericMethodInfo.Invoke(me, args);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		        }
		        public static object ExecuteMethod(this object me, string methodName, params object[] args)
		        {
		            var method = me.GetType().GetMethods().SingleOrDefault(x => x.Name == methodName);
		            if (method == null)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		            return method.Invoke(me, args);
		        }
		        public static object ExecuteNonPublicMethod(this object me, string methodName, params object[] args)
		        {
		            try
		            {
		                return me.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Single(x => x.Name == methodName).Invoke(me, args);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		        }
		
		        public static object PropertyGet(this object me, string propertyName, object[] index = null)
		        {
		            return me.GetPropertyInfo(propertyName).GetValue(me, index);
		        }
		        public static object PropertyGetNonPublic(this object me, string propertyName, object[] index = null)
		        {
		            return me.GetPropertyInfoNonPublic(propertyName).GetValue(me, index);
		        }
		
		        public static void PropertySet(this object me, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
		        {
		            var myProperty = me.GetPropertyInfo(propertyName);
		            var setMethod = myProperty.GetSetMethod(true);
		            if (setMethod != null)
		            {
		                setMethod.Invoke(me, new[] { newValue });
		            }
		            else
		            {
		                if (!ignoreNoSetMethod) myProperty.SetValue(me, newValue, index);
		            }
		        }
		        public static void PropertySetNonPublic(this object me, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
		        {
		            var myProperty = me.GetPropertyInfoNonPublic(propertyName);
		            if (ignoreNoSetMethod)
		            {
		                if (myProperty.GetSetMethod() != null) myProperty.SetValue(me, newValue, index);
		            }
		            else
		            {
		                myProperty.SetValue(me, newValue, index);
		            }
		        }
		
		        //public static object StaticPropertyGet(this Type myType, string propertyName, object[] index = null)
		        //{
		        //    return myType.GetStaticPropertyInfoWithType(propertyName).GetValue(null, index);
		        //}
		        //public static void StaticPropertySet(this Type myType, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
		        //{
		        //    var myProperty = myType.GetStaticPropertyInfoWithType(propertyName);
		        //    var setMethod = myProperty.GetSetMethod(true);
		        //    if (setMethod != null)
		        //    {
		        //        setMethod.Invoke(null, new[] { newValue });
		        //    }
		        //    else
		        //    {
		        //        if (!ignoreNoSetMethod) myProperty.SetValue(null, newValue, index);
		        //    }
		        //}
		
		        #endregion
		
		        #region Private Helpers
		        private static PropertyInfo GetPropertyInfoNonPublic(this object me, string propertyName)
		        {
		            return me.GetType().GetPropertyInfoNonPublicWithType(propertyName);
		        }
		        //private static PropertyInfo GetStaticPropertyInfoNonPublic(this object me, string propertyName)
		        //{
		        //    return me.GetType().GetStaticPropertyInfoNonPublicWithType(propertyName);
		        //}
		        private static PropertyInfo GetPropertyInfoNonPublicWithType(this Type myType, string propertyName)
		        {
		            try
		            {
		                return myType.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).Single(p => p.Name == propertyName);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		            }
		        }
		        //private static PropertyInfo GetStaticPropertyInfoNonPublicWithType(this Type myType, string propertyName)
		        //{
		        //    try
		        //    {
		        //        return myType.GetProperties(BindingFlags.NonPublic | BindingFlags.Static).Single(p => p.Name == propertyName);
		        //    }
		        //    catch (Exception)
		        //    {
		        //        throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		        //    }
		        //}
		
		
		        private static PropertyInfo GetPropertyInfo(this object me, string propertyName)
		        {
		            return me.GetType().GetPropertyInfoWithType(propertyName);
		        }
		        //private static PropertyInfo GetStaticPropertyInfo(this object me, string propertyName)
		        //{
		        //    return me.GetType().GetStaticPropertyInfoWithType(propertyName);
		        //}
		        private static PropertyInfo GetPropertyInfoWithType(this Type myType, string propertyName)
		        {
		            try
		            {
		                return myType.GetProperty(propertyName);
		            }
		            catch(Exception)
		            {
		                throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		            }
		        }
		        //private static PropertyInfo GetStaticPropertyInfoWithType(this Type myType, string propertyName)
		        //{
		        //    try
		        //    {
		        //        //return myType.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).Single(p => p.Name == propertyName);
		        //        var x = myType.GetProperties(BindingFlags.Public | BindingFlags.Static);
		        //        return myType.GetProperties().Single(p => p.Name == propertyName);
		        //    }
		        //    catch (Exception ex)
		        //    {
		        //        throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		        //    }
		        //}
		        #endregion
		    }
		}
		#endregion
		
		#region ReflectionMapperExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Linq;
		    using System.Reflection;
		    using System.Collections;
		    
		    public static class ReflectionMapperExtensions
		    {
		        public static MeT MapFrom<MeT, ObjT>(this MeT me, ObjT obj)
		        {
		            return (MeT)me.MapFromObject(obj);
		        }
		        public static ObjT MapTo<MeT, ObjT>(this MeT me, ObjT obj)
		        {
		            return (ObjT)me.MapToObject(obj);
		        }
		        public static object MapFromObject(this object me, object obj)
		        {
		            //first check if me implements IRMapperCustomMapper, if it does we use that. If custom mapper does not exist, we go property by property by calling the base method
		            if (me is IRMapperCustom) me = (me as IRMapperCustom).MapFromObjectCustom(obj, obj.GetType());
		            else me = me.MapFromObjectCustomBase(obj);
		            return me;
		        }
		        public static object MapToObject(this object me, object obj)
		        {
		            //first check if me implements ICustomMapper, if it does we use that. If custom mapper does not exist, we go property by property by calling the base method
		            if (me is IRMapperCustom) obj = (me as IRMapperCustom).MapToObjectCustom(obj, obj.GetType());
		            else obj = me.MapToObjectCustomBase(obj);
		            return obj;
		        }
		
		        public static object MapFromObjectCustomBase(this object me, object obj)
		        {
		            //if obj == null, we set me to null too, the caller must assign the result of this method
		            if (obj == null) return null;
		
		            //if me is null, report error
		            if (me == null) throw new ReflectionMapperException("MapFromObjectCustomBase(): me is null");
		
		            //ICollection<ICustomeMapper>
		            if (AreCompatibleCollections(me.GetType(), obj.GetType()))
		            {
		                var objIEnumerable = (IEnumerable)obj;
		                var myIEnumerableGenericArg = me.GetType().GetInterface(typeof(IEnumerable<>).Name).GetGenericArguments()[0];
		                foreach (var objItemObj in objIEnumerable)
		                {
		                    me.ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
		                }
		                return me;
		            }
		            
		            foreach (var property in me.GetType().GetProperties())
		            {
		                //If property is marked NotRMappedAttribute, don't worry about this one
		                if (Attribute.GetCustomAttribute(property, typeof(NotRMappedAttribute), true) != null || 
		                    Attribute.GetCustomAttribute(property, typeof(NotRMappedFromAttribute), true) != null) continue;
		
		                //Find matching properties
		                var myProperty = property;
		                var objPropertyMeta = GetMatchingProperty(me, myProperty, obj);
		
		                //ICustomMapper
		                if (typeof(IRMapperCustom).IsAssignableFrom(myProperty.PropertyType))
		                //if (myProperty.PropertyType.GetInterface(typeof(IRMapperCustom).Name) != null)
		                {
		                    try
		                    {
		                        //Get the property we are mapping to
		                        var objPropertyObj = objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name);
		
		                        var myPropertyObj = me.PropertyGet(myProperty.Name);
		                        if (myPropertyObj == null)
		                        {
		                            //we create blank object for the property in question
		                            myPropertyObj = !myProperty.PropertyType.IsGenericType ? ReflectionHelper.CreateType(myProperty.PropertyType) : ReflectionHelper.CreateGenericType(myProperty.PropertyType.GetGenericTypeDefinition(), myProperty.PropertyType.GetGenericArguments()[0]);
		                        }
		
		                        //then we ask that property to map itself to the matching property of the object
		                        // ReSharper disable PossibleNullReferenceException
		                        (myPropertyObj as IRMapperCustom).MapFromObjectCustom(objPropertyObj, objPropertyMeta.PropertyInfo.PropertyType);
		                        // ReSharper restore PossibleNullReferenceException
		
		                        me.PropertySet(myProperty.Name, myPropertyObj);
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' property '{3}' because ICustomMapper implementation threw an exception: {4}.", myProperty.Name, me.GetType().Name, obj.GetType().Name, objPropertyMeta.PropertyInfo.Name, ex.Message));
		                    }
		                }
		
		                //IColeection<ICustomMapper>
		                else if (AreCompatibleCollections(myProperty.PropertyType, objPropertyMeta.PropertyInfo.PropertyType))
		                {
		                    var objIEnumerable = (IEnumerable)objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name);
		                    var myIEnumerableGenericArg = myProperty.PropertyType.GetInterface(typeof(IEnumerable<>).Name).GetGenericArguments()[0];
		                    if (objIEnumerable != null)
		                    {
		                        me.PropertySet(myProperty.Name, ReflectionHelper.CreateGenericType(objIEnumerable.GetType().GetGenericTypeDefinition(), myIEnumerableGenericArg));
		                        foreach (var objItemObj in objIEnumerable)
		                        {
		                            me.PropertyGet(myProperty.Name).ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
		                        }
		                    }
		                }
		
		                //This is the default case of when the types match exactly
		                else if (myProperty.PropertyType == objPropertyMeta.PropertyInfo.PropertyType)
		                {
		                    me.PropertySet(myProperty.Name, objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name), true);
		                }
		
		                //If all fails
		                else
		                {
		                    throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' property '{3}' because their types are incompatible.", myProperty.Name, me.GetType().Name, obj.GetType().Name, objPropertyMeta.PropertyInfo.Name));
		                }
		            }
		            return me;
		        }
		        public static object MapToObjectCustomBase(this object me, object obj)
		        {
		            //if me == null, we set obj to null too, the caller must assign the result of this method
		            if (me == null) return null;
		
		            //if me is null, report error
		            if (obj == null) throw new ReflectionMapperException("MapToObjectCustomBase(): obj is null");
		
		            //ICollection<ICustomeMapper>
		            if (AreCompatibleCollections(me.GetType(), obj.GetType()))
		            {
		                var myICollection = (ICollection)me;
		                var objICollectionGenericArg = obj.GetType().GetInterface(typeof(IEnumerable<>).Name).GetGenericArguments()[0];
		                foreach (var myItemObj in myICollection)
		                {
		                    obj.ExecuteMethod("Add", myItemObj != null ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objICollectionGenericArg), objICollectionGenericArg) : null);
		                }
		                return obj;
		            }
		            
		            foreach (var property in me.GetType().GetProperties())
		            {
		                //If property is marked NotRMappedAttribute, don't worry about this one
		                if (Attribute.GetCustomAttribute(property, typeof(NotRMappedAttribute), true) != null ||
		                    Attribute.GetCustomAttribute(property, typeof(NotRMappedToAttribute), true) != null) continue;
		
		                //Find matching properties
		                var myProperty = property;
		                var objPropertyMeta = GetMatchingProperty(me, myProperty, obj);
		
		                //ICustomMapper
		                if (typeof(IRMapperCustom).IsAssignableFrom(myProperty.PropertyType))
		                //if (myProperty.PropertyType.GetInterface(typeof(IRMapperCustom).Name) != null)
		                {
		                    try
		                    {
		                        // ReSharper disable PossibleNullReferenceException
		                        var propertyValue = (me.PropertyGet(myProperty.Name) as IRMapperCustom).MapToObjectCustom(objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name), objPropertyMeta.PropertyInfo.PropertyType);
		                        // ReSharper restore PossibleNullReferenceException
		                        objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, propertyValue);
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' property '{3}' because ICsutomMapper threw an exception: {4}.", myProperty.Name, me.GetType().Name, obj.GetType().Name, objPropertyMeta.PropertyInfo.Name, ex.Message));
		                    }
		                }
		
		                //IEnumerable<ICustomMapper>
		                else if (AreCompatibleCollections(myProperty.PropertyType, objPropertyMeta.PropertyInfo.PropertyType))
		                {
		                    var myIEnumerable = (IEnumerable)me.PropertyGet(myProperty.Name);
		                    var objIEnumerableGenericArg = objPropertyMeta.PropertyInfo.PropertyType.GetInterface(typeof(IEnumerable<>).Name).GetGenericArguments()[0];
		                    if (myIEnumerable != null)
		                    {
		                        objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, ReflectionHelper.CreateGenericType(myIEnumerable.GetType().GetGenericTypeDefinition(), objIEnumerableGenericArg));
		                        foreach (var myItemObj in myIEnumerable)
		                        {
		                            objPropertyMeta.Obj.PropertyGet(myProperty.Name).ExecuteMethod("Add", (myItemObj != null) ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objIEnumerableGenericArg), objIEnumerableGenericArg) : null);
		                            //obj.PropertyGet(myProperty.Name).ExecuteMethod("Add", (myItemObj != null) ? ReflectionHelper.CreateType(objIEnumerableGenericArg).ExecuteMethod("MapToObjectCustom", myItemObj, myItemObj.GetType()) : null);
		                        }
		                    }
		                }
		
		                //This is the default case of when the types match exactly
		                else if (myProperty.PropertyType == objPropertyMeta.PropertyInfo.PropertyType)
		                {
		                    objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, me.PropertyGet(myProperty.Name), true);
		                }
		
		                //If all fails
		                else
		                {
		                    throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' property '{3}' because their types are incompatible.", myProperty.Name, me.GetType().Name, obj.GetType().Name, objPropertyMeta.PropertyInfo.Name));
		                }
		            }
		
		            return obj;
		        }
		        
		        public static bool IsEqualToObject(this object me, object obj)
		        {
		            //Handle nulls
		            if (me == null && obj == null) return true;
		            if (me == null || obj == null) return false;
		
		            //We only compare identical types or when obj is derived from me (this is needed for EF support)
		            if (!ReflectionHelper.IsClassADerivedFromClassB(obj.GetType(), me.GetType())) throw new ArgumentException(string.Format("Cannot compare incompatible types {0} and {1}", me.GetType().Name, obj.GetType().Name));
		
		            //IEnumerables
		            // ReSharper disable CanBeReplacedWithTryCastAndCheckForNull
		            if (me is IEnumerable)
		            // ReSharper restore CanBeReplacedWithTryCastAndCheckForNull
		            {
		                var objIEnumerable = (IEnumerable)obj;
		                var myIEnumerable = (IEnumerable)me;
		
		                var objIEnumerableEnumerator = objIEnumerable.GetEnumerator();
		                var myIEnumerableEnumerator = myIEnumerable.GetEnumerator();
		
		                while (true)
		                {
		                    var objEnumeratorDone = !objIEnumerableEnumerator.MoveNext();
		                    var myEnumeratorDone = !myIEnumerableEnumerator.MoveNext();
		
		                    //if both are done and we have not found a difference, return true
		                    if (objEnumeratorDone && myEnumeratorDone) return true;
		
		                    //If both are not done but one is done, enumerators are of diffrenet length, return false
		                    if (objEnumeratorDone || myEnumeratorDone) return false;
		
		                    var objItem = objIEnumerableEnumerator.Current;
		                    var myItem = myIEnumerableEnumerator.Current;
		
		                    if (!myItem.IsEqualToObject(objItem)) return false;
		                }
		            }
		
		            //Go through all properties that are both readable and writable
		            foreach (var property in me.GetType().GetProperties().Where(x => x.CanRead && x.CanWrite))
		            {
		                //If property is marked NotRComparedAttribute, don't worry about this one
		                if (Attribute.GetCustomAttribute(property, typeof(NotRComparedAttribute), true) != null) continue;
		
		                //Find matching properties
		                var myProperty = property;
		                var objProperty = obj.GetType().GetProperty(myProperty.Name);
		
		                //Get the property values
		                var objPropertyObj = obj.PropertyGet(objProperty.Name);
		                var myPropertyObj = me.PropertyGet(myProperty.Name);
		
		                //Handle nulls
		                if (myPropertyObj == null)
		                {
		                    if (objPropertyObj == null) continue;
		                    else return false;
		                }
		
		                //IComparer
		                var myPropertyObjAsComparable = myPropertyObj as IComparable;
		                if (myPropertyObjAsComparable != null)
		                {
		                    try
		                    {
		                        if (myPropertyObjAsComparable.CompareTo(objPropertyObj) != 0) return false;
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new ArgumentException(string.Format("Property '{0}' of type '{1}' can't be compared to type '{2}' property '{3}' because IComparable implementation threw an exception: {4}.", myProperty.Name, me.GetType().Name, obj.GetType().Name, objProperty.Name, ex.Message));
		                    }
		                }
		                
		                //Everything else
		                else
		                {
		                    try
		                    {
		                        if (!myPropertyObj.IsEqualToObject(objPropertyObj)) return false;
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new ArgumentException(string.Format("Property '{0}' of type '{1}' can't be compared to type '{2}' property '{3}' because IsEqualToObject() method threw an exception: {4}.", myProperty.Name, me.GetType().Name, obj.GetType().Name, objProperty.Name, ex.Message));
		                    }
		                }
		            }
		            return true;
		        }
		
		        #region Private Helper Methods
		        private static bool AreCompatibleCollections(Type activeType, Type passiveType)
		        {
		            //both types are generic
		            if (!activeType.IsGenericType) return false;
		            if (!passiveType.IsGenericType) return false;
		
		            //get generic type defintions
		            var activeTypeGenericDefinition = activeType.GetGenericTypeDefinition();
		            var passiveTypeGenericDefinition = passiveType.GetGenericTypeDefinition();
		
		            //make sure the types are the same except for the generic parameter
		            if (activeTypeGenericDefinition != passiveTypeGenericDefinition) return false;
		
		            //set up ICollection inetrafce in a variable
		            var activeICollectionInterface = activeType.GetInterface(typeof(ICollection<>).Name);
		            if (activeICollectionInterface == null && activeTypeGenericDefinition == typeof(ICollection<>)) activeICollectionInterface = activeType;
		
		            var passiveICollectionInterface = passiveType.GetInterface(typeof(ICollection<>).Name);
		            if (passiveICollectionInterface == null && passiveTypeGenericDefinition == typeof(ICollection<>)) passiveICollectionInterface = passiveType;
		            
		            //both types are ICollection
		            if (activeICollectionInterface == null) return false;
		            if (passiveICollectionInterface == null) return false;
		
		            //both are generic types (this might be generic but just in case)
		            if (!activeICollectionInterface.IsGenericType) return false;
		            if (!passiveICollectionInterface.IsGenericType) return false;
		
		            //my active generic ICollection<> type argument implements ICustomMapper
		            if (!typeof(IRMapperCustom).IsAssignableFrom(activeICollectionInterface.GetGenericArguments()[0])) return false;
		            //if (activeICollectionInterface.GetGenericArguments()[0].GetInterface(typeof(IRMapperCustom).Name) == null) return false;
		
		            return true;
		        }
		        private static ReflectionMapperPropertyMetadata GetMatchingProperty(object me, PropertyInfo myProperty, object obj)
		        {
		            // ReSharper disable PossibleMultipleEnumeration
		            // ReSharper disable AccessToForEachVariableInClosure
		
		            var myPropertyName = myProperty.Name;
		            var parentObj = obj;
		
		            var myReflectionMappedToAttribute = Attribute.GetCustomAttribute(myProperty, typeof(RMapToAttribute), true);
		            if (myReflectionMappedToAttribute != null)
		            {
		                var attrPropertyName = ((RMapToAttribute)myReflectionMappedToAttribute).PropertyName;
		                if (!string.IsNullOrEmpty(attrPropertyName)) myPropertyName = attrPropertyName;
		                
		                var attrObjectPath = ((RMapToAttribute) myReflectionMappedToAttribute).ObjectPath;
		                if (!string.IsNullOrEmpty(attrObjectPath))
		                {
		                    var mappedToNameComponents = attrObjectPath.Split('.');
		                    if (mappedToNameComponents.Length < 1) throw new ReflectionMapperException("Invalid path in ReflectionMappedTo Attribute");
		
		                    foreach (var subPropertyName in mappedToNameComponents)
		                    {
		                        var subObjProperties = parentObj.GetType().GetProperties().Where(x => x.Name == subPropertyName);
		                        if (subObjProperties.Count() != 1) throw new ReflectionMapperException("Invalid path in ReflectionMappedTo Attribute");
		                        parentObj = parentObj.PropertyGet(subObjProperties.Single().Name);
		                        if (parentObj == null) throw new ReflectionMapperException(string.Format("{0} in ReflectionMappedTo Attribute path is null", subPropertyName));
		                    }
		                }
		            }
		            
		            var objProperties = parentObj.GetType().GetProperties().Where(x => x.Name == myPropertyName);
		            if (objProperties.Count() != 1) throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' because '{3}' property does not exist in type '{2}'.", myProperty.Name, me.GetType().Name, parentObj.GetType().Name, myPropertyName));
		            var propertyInfo = objProperties.Single();
		
		            return new ReflectionMapperPropertyMetadata { Obj = parentObj, PropertyInfo = propertyInfo };
		
		            // ReSharper restore AccessToForEachVariableInClosure
		            // ReSharper restore PossibleMultipleEnumeration
		        }
		        #endregion
		    }
		}
		#endregion
		
		#region ReflectionMapperPropertyMetadata
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System.Reflection;
		    
		    public class ReflectionMapperPropertyMetadata
		    {
		        public PropertyInfo PropertyInfo { get; set; }
		        public object Obj { get; set; }
		    }
		}
		#endregion
		
	#endregion
	
	#region Repository
		
		#region DataRepo
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using System.Collections.Generic;
		    using System.Threading.Tasks;
		    using DataContext;
		    using DataContext.Core;
		    using Exceptions;
		    using Models;
		    
		    public class DataRepo<ModelT> : IDataRepo<ModelT> where ModelT : class, IModel, new()
		    {
		        #region Reads
		        public virtual Task<ModelT> GetByIdAsync(int id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetByIdAsync operation");
		            return (context as IReadableDataContext).GetByIdAsync<ModelT>(id);
		        }
		        public virtual Task<ModelT> GetByIdOrDefaultAsync(int id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetByIdOrDefaultAsync operation");
		            return (context as IReadableDataContext).GetByIdOrDefaultAsync<ModelT>(id);
		        }
		        public virtual Task<List<ModelT>> GetAllAsync(int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetAllAsync operation");
		            return (context as IReadableDataContext).GetAllAsync<ModelT>(skip, take);
		        }
		        public virtual Task<int> GetCountAllAsync(int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetCountAllAsync operation");
		            return (context as IReadableDataContext).GetCountAllAsync<ModelT>(skip, take);
		        }
		        #endregion
		
		        #region Batch Reads
		        public virtual void DelayedGetById(out DelayedModel<ModelT> model, int id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetById operation");
		            (context as IReadableDataContext).DelayedGetById(out model, id);
		        }
		        public virtual void DelayedGetByIdOrDefault(out DelayedModel<ModelT> model, int id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetByIdOrDefault operation");
		            (context as IReadableDataContext).DelayedGetByIdOrDefault(out model, id);
		        }
		        public virtual void DelayedGetAll(out DelayedModels<ModelT> models)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetAll operation");
		            (context as IReadableDataContext).DelayedGetAll(out models);
		        }
		        public virtual void DelayedGetCountAll(out DelayedCount count)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetCountAll operation");
		            (context as IReadableDataContext).DelayedGetCountAll<ModelT>(out count);
		        }
		        #endregion
		
		        #region Queries
		        public virtual Task<List<ModelT>> GetWhereAsync(object searchBy, string sortBy = null, int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetWhereAsync operation");
		            return (context as IQuerableReadableDataContext).GetWhereAsync<ModelT>(searchBy, sortBy, skip, take);
		        }
		        public virtual Task<int> GetCountWhereAsync(object searchBy)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetCountWhereAsync operation");
		            return (context as IQuerableReadableDataContext).GetCountWhereAsync<ModelT>(searchBy);
		        }
		        #endregion
		
		        #region Delayed Queries
		        public virtual void DelayedGetWhere(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetWhere operation");
		            (context as IQuerableReadableDataContext).DelayedGetWhere(out models, searchBy, sortBy, skip, take);
		        }
		        public virtual void DelayedGetCountWhere(out DelayedCount count, object searchBy)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetCountWhere operation");
		            (context as IQuerableReadableDataContext).DelayedGetCountWhere<ModelT>(out count, searchBy);
		        }
		        #endregion
		
		        #region Writes
		        public virtual void Add(ModelT model)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support Add operation");
		            (context as IWriteableDataContext).Add(model);
		        }
		        public virtual void Delete(int modelId)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support Delete operation");
		            (context as IWriteableDataContext).Delete<ModelT>(modelId);
		        }
		        public virtual void ForceUpdate(ModelT model)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support ForceUpdate operation");
		            (context as IWriteableDataContext).ForceUpdate(model);
		        }
		        #endregion
		    }
		}
		#endregion
		
		#region IDataRepo
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using System.Collections.Generic;
		    using System.Threading.Tasks;
		    using DataContext.Core;
		    using Models;
		    
		    public interface IDataRepo<ModelT> where ModelT : class, IModel, new()
		    {
		        #region Reads
		        Task<ModelT> GetByIdAsync(int id);
		        Task<ModelT> GetByIdOrDefaultAsync(int id);
		        Task<List<ModelT>> GetAllAsync(int? skip = null, int? take = null);
		        Task<int> GetCountAllAsync(int? skip = null, int? take = null);
		        #endregion
		
		        #region Batch Reads
		        void DelayedGetById(out DelayedModel<ModelT> model, int id);
		        void DelayedGetByIdOrDefault(out DelayedModel<ModelT> model, int id);
		        void DelayedGetAll(out DelayedModels<ModelT> models);
		        void DelayedGetCountAll(out DelayedCount count);
		        #endregion
		
		        #region Queries
		        Task<List<ModelT>> GetWhereAsync(object searchBy, string sortBy = null, int? skip = null, int? take = null);
		        Task<int> GetCountWhereAsync(object searchBy);
		        #endregion
		
		        #region Batch Queries
		        void DelayedGetWhere(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null);
		        void DelayedGetCountWhere(out DelayedCount count, object searchBy);
		        #endregion
		
		        #region Writes
		        void Add(ModelT model);
		        void Delete(int modelId);
		        void ForceUpdate(ModelT model);
		        #endregion
		    }
		}
		#endregion
		
		#region IRepoFactory
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using Models;
		
		    public interface IRepoFactory
		    {
		        IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new();
		    }
		}
		#endregion
		
		#region RepoFactory
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using System;
		    using DataContext;
		    using Models;
		    // ReSharper disable RedundantNameQualifier
		    using Supermodel.Mobile.ReflectionMapper;
		    // ReSharper restore RedundantNameQualifier
		    
		    public static class RepoFactory
		    {
		        public static IDataRepo<ModelT> Create<ModelT>() where ModelT : class, IModel, new()
		        {
		            return UnitOfWorkContextCore.CurrentDataContext.CreateRepo<ModelT>();
		        }
		        public static object CreateForRuntimeType(Type modelType)
		        {
		            return ReflectionHelper.ExecuteStaticGenericMethod(typeof(RepoFactory), "Create", new[] { modelType });
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region UnitOfWork
		
		#region HttpClientExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.UnitOfWork
		{
		    // ReSharper disable once RedundantUsingDirective
		    using System.Runtime.Remoting.Messaging;
		    using System.Net.Http;
		    using System.Threading.Tasks;
		    
		    public static class HttpClientExtensions
		    {
		        public static async Task<HttpResponseMessage> GetAsyncAndPreserveContext(this HttpClient httpClient, string url)
		        {
		            //This is to account for a Mono problem in Android
		            #if __ANDROID__
					var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack");
		            var migrationInProgressFlag = CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread");  
		            #endif
		
		            var dataResponse = await httpClient.GetAsync(url);
		            
		            #if __ANDROID__
		            CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", migrationInProgressFlag);
		            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
		            #endif
		
		            return dataResponse;
		        }
		
		        public static async Task<HttpResponseMessage> SendAsyncAndPreserveContext(this HttpClient httpClient, HttpRequestMessage request)
		        {
		            //This is to account for a Mono problem in Android
		            #if __ANDROID__
					var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack");
		            var migrationInProgressFlag = CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread");
		            #endif
		
		            var dataResponse = await httpClient.SendAsync(request);
		            
		            #if __ANDROID__
		            CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", migrationInProgressFlag);
		            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
		            #endif
		
		            return dataResponse;
		        }
		    }
		}
		#endregion
		
		#region ReadOnly
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.DataContext
		{
		    public enum ReadOnly
		    {
		        No,
		        Yes,
		    }
		}
		#endregion
		
		#region UnitOfWork
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.DataContext
		{
		    using System;
		    using Core;
		    using Exceptions;
		
		    public class UnitOfWork<ContextT> : IDisposable where ContextT : class, IDataContext, new()
		    {
		        #region Constructors
		        public UnitOfWork(ReadOnly readOnly = ReadOnly.No)
		        {
		            Context = new ContextT();
		            if (readOnly == ReadOnly.Yes) Context.MakeReadOnly();
		            UnitOfWorkContext<ContextT>.PushDbContext(Context);
		        }
		        #endregion
		
		        #region IDisposable implemetation
		        public void Dispose()
		        {
		            Context.Dispose();
		
		            var context = UnitOfWorkContext<ContextT>.PopDbContext();
		
		            // ReSharper disable PossibleUnintendedReferenceComparison
		            if (context != Context) throw new SupermodelException(string.Format("POP on Dispose popped mismatched Data Context."));
		            // ReSharper restore PossibleUnintendedReferenceComparison
		        }
		        #endregion
		
		        #region Properties
		        public ContextT Context { get; private set; }
		        #endregion
		    }
		}
		#endregion
		
		#region UnitOfWorkContext
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.DataContext
		{
		    using System;
		    using System.Collections.Concurrent;
		    using System.Runtime.Remoting.Messaging;
		    using Core;
		    using Exceptions;
		    using Encryptor;
		    using System.Threading.Tasks;
		    using Models;
		
		    //Shortcuts for the most often used Context methods
		    public static class UnitOfWorkContext
		    {
		        #region Methods and Properties
		        public static Task SaveChangesAsync()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("SaveChangesAsync() is only valid for IWriteableDataContext");
		            return ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).SaveChangesAsync();
		        }
		        public static Task FinalSaveChangesAsync()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("FinalSaveChangesAsync() is only valid for IWriteableDataContext");
		            return ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).FinalSaveChangesAsync();
		        }
		        public static void DetectUpdates()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("DetectUpdates() is only valid for IWriteableDataContext");
		            ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).DetectUpdates();
		        }
		        public static bool CommitOnDispose
		        {
		            get { return UnitOfWorkContextCore.CurrentDataContext.CommitOnDispose; }
		            set { UnitOfWorkContextCore.CurrentDataContext.CommitOnDispose = value; }
		        }
		        public static AuthHeader AuthHeader
		        {
		            get
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthHeader is only accesable for IWebApiAuthorizationContext");
		                return ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).AuthHeader;
		            }
		            set
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthorizationHeader is only accesable for IWebApiAuthorizationContext");
		                ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).AuthHeader = value;
		            }
		        }
		        public static Task<bool> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthorizationHeader is only accesable for IWebApiAuthorizationContext");
		            return ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).ValidateLoginAsync<ModelT>();
		        }
		        public static int CacheAgeToleranceInSeconds
		        {
		            get
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
		                return ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).CacheAgeToleranceInSeconds;
		            }
		            set
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
		                ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).CacheAgeToleranceInSeconds = value;
		            }
		        }
		        public static Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null)
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
		            return ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).PurgeCacheAsync(cacheExpirationAgeInSeconds, modelType);
		        }
		        #endregion
		    }
		    
		    public static class UnitOfWorkContext<ContextT> where ContextT : IDataContext
		    {
		        #region Methods and Properties
		        static public ContextT PopDbContext()
		        {
		            return (ContextT)UnitOfWorkContextCore.PopDbContext();
		        }
		        static public void PushDbContext(ContextT context)
		        {
		            UnitOfWorkContextCore.PushDbContext(context);
		        }
		        public static uint StackCount
		        {
		            get { return UnitOfWorkContextCore.StackCount; }
		        }
		        static public ContextT CurrentDataContext
		        {
		            get { return (ContextT)UnitOfWorkContextCore.CurrentDataContext; }
		        }
		        public static bool HasDbContext()
		        {
		            return StackCount > 0;
		        }
		        #endregion
		    }
		
		    public static class UnitOfWorkContextCore
		    {
		        #region Methods and Properties
		        static public void ReplaceContextStackWithNew()
		        {
		            lock (_contextStackLock)
		            {
		                CallContext.LogicalSetData("SupermodelDataContextStack", new ConcurrentStack<IDataContext>());
		            }
		        }
		        static public IDataContext PopDbContext()
		        {
		            IDataContext context;
		            if (!_contextStackThreadSafe.TryPop(out context)) throw new InvalidOperationException("Stack is empty");
		            return context;
		        }
		        static public void PushDbContext(IDataContext context)
		        {
		            _contextStackThreadSafe.Push(context);
		        }
		        public static uint StackCount
		        {
		            get { return (uint)_contextStackThreadSafe.Count; }
		        }
		        public static IDataContext CurrentDataContext
		        {
		            get
		            {
		                IDataContext context;
		                if (!_contextStackThreadSafe.TryPeek(out context)) throw new SupermodelException("Current UnitOfWork does not exist. All database access oprations must be wrapped in 'using(new UnitOfWork())'");
		                return context;
		            }
		        }
		        #endregion
		
		        #region Private variables
		        // ReSharper disable InconsistentNaming
		        private static readonly Object _contextStackLock = new Object();
		        private static ConcurrentStack<IDataContext> _contextStackThreadSafe
		        {
		            get
		            {
		                var contextStack = (ConcurrentStack<IDataContext>)CallContext.LogicalGetData("SupermodelDataContextStack");
		                if (contextStack == null)
		                {
		                    lock (_contextStackLock)
		                    {
		                        //We read again, now inside a lock to make sure nobody created one since we last checked
		                        contextStack = (ConcurrentStack<IDataContext>)CallContext.LogicalGetData("SupermodelDataContextStack");
		                        if (contextStack == null)
		                        {
		                            contextStack = new ConcurrentStack<IDataContext>();
		                            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
		                        }
		                    }
		                }
		                return contextStack;
		            }
		        }
		        // ReSharper restore InconsistentNaming
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Utils
		
		#region ObservableCollectionExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System;
		    using System.Collections.ObjectModel;
		    using System.Linq;
		    
		    public static class ObservableCollectionExtensions
		    {
		        public static int RemoveAll<T>(this ObservableCollection<T> coll, Func<T, bool> condition)
		        {
		            var itemsToRemove = coll.Where(condition).ToList();
		            foreach (var itemToRemove in itemsToRemove) coll.Remove(itemToRemove);
		            return itemsToRemove.Count;
		        }
		    }
		}
		#endregion
		
		#region SearchByHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System.Linq;
		
		    public static class SearchByHelper
		    {
		        public static object PropertyGetWithNullIfNoProperty(this object me, string propertyName)
		        {
		            var propertyInfo = me.GetType().GetProperties().SingleOrDefault(x => x.Name == propertyName);
		            return propertyInfo == null ? null : propertyInfo.GetValue(me);
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region Validation
		
		#region ValidationResultList
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Validation
		{
		    using System;
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		    using System.Linq.Expressions;
		    using System.Reflection;
		    using Exceptions;
		    
		    public class ValidationResultList : List<ValidationResult>
		    {
		        public ValidationResult AddValidationResult<ModelT, ValueT>(ModelT model, Expression<Func<ModelT, ValueT>> expression, string message)
		        {
		            var validationResult = CreateValidationResult(model, expression, message);
		            Add(validationResult);
		            return validationResult;
		        }
		        public ValidationResult AddValidationResult<ModelT, Value1T, Value2T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, string message)
		        {
		            var validationResult = CreateValidationResult(model, expression1, expression2, message);
		            Add(validationResult);
		            return validationResult;
		        }
		        public ValidationResult AddValidationResult<ModelT, Value1T, Value2T, Value3T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, Expression<Func<ModelT, Value3T>> expression3, string message)
		        {
		            var validationResult = CreateValidationResult(model, expression1, expression2, expression2, message);
		            Add(validationResult);
		            return validationResult;
		        }
		        public ValidationResultList AddValidationResultList(ValidationResultList vrl)
		        {
		            foreach (var vr in vrl) Add(vr);
		            return this;
		        }
		
		        public static ValidationResult CreateValidationResult<ModelT, ValueT>(ModelT model, Expression<Func<ModelT, ValueT>> expression, string message)
		        {
		            return new ValidationResult(message, new[] { GetPropertyName(model, expression) });
		        }
		        public static ValidationResult CreateValidationResult<ModelT, Value1T, Value2T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, string message)
		        {
		            return new ValidationResult(message, new[] { GetPropertyName(model, expression1), GetPropertyName(model, expression2) });
		        }
		        public static ValidationResult CreateValidationResult<ModelT, Value1T, Value2T, Value3T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, Expression<Func<ModelT, Value3T>> expression3, string message)
		        {
		            return new ValidationResult(message, new[] { GetPropertyName(model, expression1), GetPropertyName(model, expression2), GetPropertyName(model, expression3) });
		        }
		
		        public static string GetPropertyName<ModelT, ValueT>(ModelT model, Expression<Func<ModelT, ValueT>> expression)
		        {
		            if (expression.Body.NodeType != ExpressionType.MemberAccess) throw new SupermodelException("Expression must describe a property");
		            var memberExpression = (MemberExpression)expression.Body;
		            var propertyName = memberExpression.Member is PropertyInfo ? memberExpression.Member.Name : null;
		            return GetExpressionName(memberExpression.Expression) + propertyName;
		        }
		
		        public static string GetExpressionName(Expression expression)
		        {
		            if (expression.NodeType == ExpressionType.Parameter) return "";
		
		            if (expression.NodeType == ExpressionType.MemberAccess)
		            {
		                var memberExpression = (MemberExpression)expression;
		                return GetExpressionName(memberExpression.Expression) + memberExpression.Member.Name + ".";
		            }
		
		            throw new Exception("Invalid Expression '" + expression + "'");
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region XForms
		
		#region Views
			
			#region ViewWithActivityIndicator
			#if __ANDROID__ || __IOS__
			
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.Views
			{
			    using Xamarin.Forms;
			    
			    public class ViewWithActivityIndicator<ContentViewT> : AbsoluteLayout where ContentViewT : View
			    {
			        #region Constructors
			        public ViewWithActivityIndicator(ContentViewT contentView)
			        {
			            ContentView = contentView;
			            ActivityIndicator = new ActivityIndicator();
			            GrayOutOverlay = new BoxView { Color = new Color (0, 0, 0, 0.4) };
			
			            HorizontalOptions = LayoutOptions.FillAndExpand;
			            VerticalOptions = LayoutOptions.FillAndExpand;
			
			            SetLayoutFlags(ContentView, AbsoluteLayoutFlags.All);
			            SetLayoutBounds(ContentView, new Rectangle(0, 0, 1f, 1f));
			
			            SetLayoutFlags(GrayOutOverlay, AbsoluteLayoutFlags.All);
			            SetLayoutBounds(GrayOutOverlay, new Rectangle(0, 0, 1f, 1f));
			
			            SetLayoutFlags(ActivityIndicator, AbsoluteLayoutFlags.PositionProportional);
			            SetLayoutBounds(ActivityIndicator, new Rectangle(0.5, 0.5, AutoSize, AutoSize));
			            
			            Children.Add(ContentView);
			            Children.Add(GrayOutOverlay);
			            Children.Add(ActivityIndicator);
			            ActivityIndicatorOn = false;
			        }
			        #endregion
			
			        #region Methods
			        public bool ActivityIndicatorOn
			        {
			            set
			            {
			                _activityIndicatorOn = value;
			                ActivityIndicator.IsEnabled = ActivityIndicator.IsRunning = ActivityIndicator.IsVisible = GrayOutOverlay.IsEnabled = GrayOutOverlay.IsVisible = value;
			            }
			            get
			            {
			                return _activityIndicatorOn;
			            }
			        }
			        #endregion
			
			        #region Properties
			        public BoxView GrayOutOverlay { get; set; }
			        public ActivityIndicator ActivityIndicator { get; set; }
			        public ContentViewT ContentView { get; set; }
			        private bool _activityIndicatorOn;
			        #endregion
			    }
			}
			#endif
			#endregion
			
		#endregion
		
	#endregion
	
#endregion

