﻿using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.WebApi;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;

namespace TestDomain.Supermodel
{
    public abstract class TestApiCRUDController<EntityT, ApiModelT> : SyncApiCRUDController<EntityT, ApiModelT, TestDbContext>
        where EntityT : class, IEntity, new()
        where ApiModelT : ApiModelForEntity<EntityT>, new()
    {}

    public abstract class TestEnhancedApiCRUDController<EntityT, ApiModelT, SearchApiModelT> : SyncEnhancedApiCRUDController<EntityT, ApiModelT, SearchApiModelT, TestDbContext>
        where EntityT : class, IEntity, new()
        where ApiModelT : ApiModelForEntity<EntityT>, new()
        where SearchApiModelT : SearchApiModel, new()
    {}

    //public abstract class TestPageableSortableCRUDController<EntityT, MvcModelT> : SyncEnhancedMvcCRUDController<EntityT, MvcModelT, TestDbContext>
    //    where EntityT : class, IEntity, new()
    //    where MvcModelT : MvcModelForEntity<EntityT>, new()
    //{}

    public abstract class TestPageableSortableSearchableCRUDController<EntityT, MvcModelT, SearchMvcModelT> : SyncEnhancedMvcCRUDController<EntityT, MvcModelT, SearchMvcModelT, TestDbContext>
        where EntityT : class, IEntity, new()
        where MvcModelT : MvcModelForEntity<EntityT>, new()
        where SearchMvcModelT : MvcModel, new()
    {}

    public abstract class TestChildCRUDController<EntityT, ParentEntityT, MvcModelT> : SyncMvcChildCRUDController<EntityT, ParentEntityT, MvcModelT, TestDbContext>
        where EntityT : class, IEntity, new()
        where ParentEntityT : class, IEntity, new()
        where MvcModelT : ChildMvcModelForEntity<EntityT, ParentEntityT>, new()
    {}

    public abstract class TestCRUDController<EntityT, MvcModelT> : SyncMvcCRUDController<EntityT, MvcModelT, TestDbContext>
        where EntityT : class, IEntity, new()
        where MvcModelT : MvcModelForEntity<EntityT>, new()
    {}
}
