﻿using System;
using Supermodel.MvcAndWebApi.Auth.WebApi;

namespace TestDomain.Supermodel
{
    public class TestAuthenticateAttribute : AuthenticateAttribute
    {
        protected override string AuthenticateBasicAndGetIdentityName(string username, string password)
        {
            return "1";
        }
        protected override string Realm { get { return "SuperModelTest"; } }

        #region Custom ecrypted stuff we did nit implement in this example
        protected override byte[] EncryptionKey { get { throw new NotImplementedException(); } }
        protected override string AuthenticateEncryptedAndGetIdentityName(string[] args, out string password)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
