﻿using System.Web.Mvc;

namespace TestWeb.Controllers
{
    public class StartController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
