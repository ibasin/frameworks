using System.Linq;
using System.Web.Http;
using TestDomain.Entities;
using TestDomain.Supermodel;

namespace TestWeb.ApiControllers
{
    [Authorize]
    public class DemoPersonController : TestEnhancedApiCRUDController<TestPerson, DemoPersonApiModel, DemoPersonSearchApiModel>
    {
        protected override IQueryable<TestPerson> ApplySearchBy(IQueryable<TestPerson> items, DemoPersonSearchApiModel searchBy)
        {
            if (!string.IsNullOrEmpty(searchBy.Name)) items = items.Where(x => x.Name.Contains(searchBy.Name));
            if (searchBy.FromHeight != null) items = items.Where(x => x.Height >= searchBy.FromHeight);
            if (searchBy.ToHeight != null) items = items.Where(x => x.Height <= searchBy.ToHeight);
            return items;
        }
    }
}