 // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.WebApi
{
    public class LoginResult
    {
        #region Contructors
        public LoginResult(bool loginSuccessful, string userLabel)
        {
            LoginSuccessful = loginSuccessful;
            UserLabel = loginSuccessful ? userLabel : null;
        }
        #endregion

        #region Properties
        public bool LoginSuccessful { get; set; }
        public string UserLabel { get; set; }
        #endregion
    }
}