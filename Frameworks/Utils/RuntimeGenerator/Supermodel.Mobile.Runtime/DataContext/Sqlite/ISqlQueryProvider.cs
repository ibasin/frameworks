﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Sqlite
{
    using Models;
    
    public interface ISqlQueryProvider
    {
        object GetIndex(int idxNum0To29, IModel model);
        string GetWhereClause(object searchBy, int? skip, int? take, string sortBy);
    }
}
