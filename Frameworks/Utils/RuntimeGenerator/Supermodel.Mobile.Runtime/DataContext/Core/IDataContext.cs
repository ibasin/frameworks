﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using System;
    using Models;
    using Repository;
    using System.Collections.Generic;
    
    public interface IDataContext : IDisposable
    {
        #region Configuration
        bool CommitOnDispose { get; set; }
        bool IsReadOnly { get; }
        void MakeReadOnly();
        #endregion

        #region Context RepoFactory
        IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new();
        #endregion

        #region CustomValues
        Dictionary<string, object> CustomValues { get; } 
        #endregion
    }
}

