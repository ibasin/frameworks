﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using System.Threading.Tasks;
    using Models;
    
    public interface IWriteableDataContext : IDataContext
    {
        #region Writes
        void Add<ModelT>(ModelT model) where ModelT : class, IModel, new();
        void Delete<ModelT>(ModelT model) where ModelT : class, IModel, new();
        void ForceUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new();
        void DetectAllUpdates();
        #endregion

        #region Save Changes
        Task SaveChangesAsync();
        Task FinalSaveChangesAsync();
        #endregion
    }
}