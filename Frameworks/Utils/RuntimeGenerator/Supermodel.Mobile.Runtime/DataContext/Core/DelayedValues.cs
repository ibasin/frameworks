﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using System.Collections.Generic;
    using Models;
    
    public abstract class DelayedValue
    {
        public abstract void SetValue(object value);
        public abstract object GetValue();
    }
    
    public class DelayedModel<ModelT> : DelayedValue where ModelT : class, IModel, new() 
    {
        #region Overrides
        public override void SetValue(object value)
        {
            Value = (ModelT)value;
        }

        public override object GetValue()
        {
            return Value;
        }
        #endregion

        #region Properties
        public ModelT Value { get; set; }
        #endregion
    }

    public class DelayedModels<ModelT> : DelayedValue where ModelT : class, IModel, new()
    {
        #region Overrides
        public override void SetValue(object value)
        {
            Values = (List<ModelT>)value;
        }
        public override object GetValue()
        {
            return Values;
        }
        #endregion

        #region Properties
        public List<ModelT> Values { get; set; }
        #endregion
    }

    public class DelayedCount : DelayedValue
    {
        #region Overrides
        public override void SetValue(object value)
        {
            Value = (long?) value;
        }
        public override object GetValue()
        {
            return Value;
        }
        #endregion

        #region Properties
        public long? Value { get; set; }
        #endregion
    }
}
