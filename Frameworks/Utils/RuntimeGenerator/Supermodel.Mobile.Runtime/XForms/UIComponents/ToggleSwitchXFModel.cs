﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using System;
    using ReflectionMapper;
    using Xamarin.Forms;
    using Exceptions;
    using Base;

    public class ToggleSwitchXFModel : SingleCellWritableUIComponentXFModel
    {
        #region Constructors
		public ToggleSwitchXFModel()
		{
			Switch = new Switch { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center };
			Switch.SetBinding(Switch.IsToggledProperty, "IsToggled");
            StackLayoutView.Children.Add(Switch);
            Tapped += (sender, args) => Switch.Focus();
		}
        #endregion

        #region ICustomMapper implemtation
        public override object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(bool) && objType != typeof(bool?))
            {
                throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1}", GetType().Name, objType.Name));
            }

            if (obj is bool) IsToggled = (bool)obj;
            else IsToggled = (bool?)obj ?? false;

            return this;
        }
        public override object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(bool) && objType != typeof(bool?))
            {
                throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1}", GetType().Name, objType.Name));
            }
            
            return IsToggled;
        }
        #endregion

        #region Properties
        public bool IsToggled
        {
            get { return Switch.IsToggled; }
            set
            {
                if (value == Switch.IsToggled) return;
                Switch.IsToggled = value;
                OnPropertyChanged();
            }
        }
        public Switch Switch { get; set; }

        public override object WrappedValue
        {
            get { return Switch.IsToggled; } 
        }
        public override TextAlignment TextAlignmentIfApplies
        {
            get
            {
                switch(Switch.HorizontalOptions.Alignment)
                {
                    case LayoutAlignment.Start: return TextAlignment.Start;
                    case LayoutAlignment.Center: return TextAlignment.Center;
                    case LayoutAlignment.End: return TextAlignment.End;
                    default: throw new SupermodelException("Invalid value for Switch.HorizontalOptions.Alignment. This should never happen");
                }
            }
            set
            {
                switch(value)
                {
                    case TextAlignment.Start: 
                        Switch.HorizontalOptions = LayoutOptions.StartAndExpand;
                        break;
                    case TextAlignment.Center: 
                        Switch.HorizontalOptions = LayoutOptions.CenterAndExpand;
                        break;
                    case TextAlignment.End: 
                        Switch.HorizontalOptions = LayoutOptions.EndAndExpand;
                        break;
                    default: throw new SupermodelException("Invalid value for TextAlignmentIfApplies. This should never happen");
                }
            }
        }
        #endregion
    }
}
