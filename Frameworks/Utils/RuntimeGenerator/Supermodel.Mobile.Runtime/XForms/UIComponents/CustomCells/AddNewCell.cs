﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.CustomCells
{
    using System;
    using Xamarin.Forms;
    
    public class AddNewCell : ViewCell
    {
        public AddNewCell()
        {
            View = new StackLayout
            {
                Padding = new Thickness(8, 0, 8, 0), 
                Orientation = StackOrientation.Horizontal, 
                VerticalOptions = LayoutOptions.CenterAndExpand, 
                HorizontalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 40,
            };
			
            AddNewLabel = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, Text = "Add New" }; 
            if (Device.OS == TargetPlatform.iOS) AddNewLabel.TextColor = Color.FromHex("#007AFF");
            StackLayoutView.Children.Add(AddNewLabel);

            ValidationErrorIndicator = new Button{ Text = "!", TextColor = Color.Red, HorizontalOptions = LayoutOptions.EndAndExpand };
            ValidationErrorIndicator.Clicked += ValidationIndicatorClicked;

            RequiredFieldIndicator = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, TextColor = Color.Red, Text = "*" };
        }

        #region Event Handlers
        public async void ValidationIndicatorClicked(object sender, EventArgs args)
        {
            if (ParentPage != null) await ParentPage.DisplayAlert("", ErrorMessage, "Ok");
        }
        #endregion

        #region Properties
        public Page ParentPage { get; set; }
        
        public StackLayout StackLayoutView
        {
            get
            {
                return (StackLayout)View;
            } 
            set
            {
                View = value;
            }
        }

        public string Text
        {
            get { return AddNewLabel.Text; }
            set { AddNewLabel.Text = value; }
        }
        public Label AddNewLabel { get; set; }

        public Button ValidationErrorIndicator { get; set; }
        public Label RequiredFieldIndicator { get; set; }
        public bool Required
        {
            get { return _required; }
            set
            {
                if (!_required && value) StackLayoutView.Children.Insert(0, RequiredFieldIndicator);
                if (_required && !value) StackLayoutView.Children.Remove(RequiredFieldIndicator);
                _required = value;
            }
        }
        private bool _required;
        
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                if (_errorMessage == null & value != null) StackLayoutView.Children.Add(ValidationErrorIndicator);
                if (_errorMessage != null & value == null) StackLayoutView.Children.Remove(ValidationErrorIndicator);
                _errorMessage = value;
            }
        }
        private string _errorMessage;
        #endregion
    }
}
