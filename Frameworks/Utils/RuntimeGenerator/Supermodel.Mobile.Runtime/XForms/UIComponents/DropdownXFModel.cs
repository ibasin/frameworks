﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
	using Xamarin.Forms;    
	using CustomControls;
	using System.Collections.ObjectModel;
	using System.Collections.Specialized;
	using System.Linq;
	using System.Collections.Generic;
	using Base;
			    
	public class DropdownXFModel : SingleCellWritableUIComponentWithoutBackingXFModel
	{
		#region Option nested class
		public class Option
		{
			public Option(string value, string label, bool isDisabled = false)
			{
			    Value = value;
			    Label = label;
			    IsDisabled = isDisabled;
			}
			public string Value { get; private set; }
			public string Label { get; private set; }
			public bool IsDisabled { get; private set; }
		}
		public ObservableCollection<Option> Options = new ObservableCollection<Option>();
		public List<Option> DisplayedOptions = new List<Option>();
		#endregion
			        
		#region Constructors
		public DropdownXFModel()
		{
			Picker = new ExtPicker
			{
				HorizontalOptions = LayoutOptions.FillAndExpand, 
                VerticalOptions = LayoutOptions.FillAndExpand, 
                Border = false, 
                TextAlignment = TextAlignment.End,
                TextColor =  Device.OnPlatform(Color.Black, Color.White, Color.Black)
			};
			StackLayoutView.Children.Add(Picker);
			Tapped += (sender, args) => Picker.Focus();
			Options.CollectionChanged += OptionsChangedHandler;
			SelectedValue = "";

		    Picker.SelectedIndexChanged += (source, args) => { UpdateTextColor(); };
		}
		#endregion
			
		#region Event Handlers
        protected virtual void OptionsChangedHandler(object sender, NotifyCollectionChangedEventArgs args)
		{
			ResetList(SelectedValue, true);
		}
		protected virtual void ResetList(string selectedValue, bool setValue)
		{
			DisplayedOptions = new List<Option> { new Option("", BlankOptionLabel) };
			foreach (var option in Options)
			{
			    if (option.IsDisabled)
			    {
			        if (option.Value == selectedValue) DisplayedOptions.Add(option);
			    }
			    else
			    {
			        DisplayedOptions.Add(option);
			    }
			}
			            
			Picker.Items.Clear();
			foreach (var option in DisplayedOptions)
			{
				if (option.IsDisabled) Picker.Items.Add(option.Label + " [DISABLED]");
			    else Picker.Items.Add(option.Label);
			}
			if (setValue) SelectedValue = selectedValue;
		}
		#endregion
			
		#region Properties
		public string BlankOptionLabel { get; set; }
		public string SelectedValue
		{
			get
			{
			    if (SelectedIndex == null) return "";
			    var selectedOption = DisplayedOptions[SelectedIndex.Value];
			    return selectedOption.Value;
			} 
			set
			{
			    if (value == null)
			    {
			        SelectedIndex = null;
			    }
			    else
			    {
			        var originalItemDisabled = false;
			        if (SelectedIndex != null) originalItemDisabled = DisplayedOptions[SelectedIndex.Value].IsDisabled;
			                    
			        var selectedOption = DisplayedOptions.FirstOrDefault(x => x.Value == value);
					if (selectedOption == null)
					{
						SelectedIndex = null;
					}
					else
					{
			            //SelectedIndex = Options.Where(x => !x.IsDisabled || x.Value == value).ToList().IndexOf(selectedOption);
						if (selectedOption.IsDisabled || originalItemDisabled) ResetList(selectedOption.Value, false);
			            SelectedIndex = DisplayedOptions.IndexOf(selectedOption);
					}
			    }
			}
		}
		public string SelectedLabel
		{
			get
			{
			    if (SelectedIndex == null) return "";
			    var selectedOption = Options[SelectedIndex.Value];
			    return selectedOption.Label;
			}
		}
		public bool IsEmpty => string.IsNullOrEmpty(SelectedValue);

	    protected int? SelectedIndex
		{
			get { return Picker.SelectedIndex == -1 ? (int?)null : Picker.SelectedIndex; }
			set
			{
			    if (value == null) Picker.SelectedIndex = -1;
			    else Picker.SelectedIndex = value.Value;

                UpdateTextColor();
			}
		}

	    public void UpdateTextColor()
	    {
            if (SelectedIndex == 0) Picker.TextColor = Color.Gray;
            else Picker.TextColor = Device.OnPlatform(Color.Black, Color.White, Color.Black);
	    }

		public ExtPicker Picker { get; set; }
			
		public override object WrappedValue => SelectedValue;

	    public override TextAlignment TextAlignmentIfApplies
		{
			get { return Picker.TextAlignment; }
			set { Picker.TextAlignment = value; }
		}
		#endregion
	}
}