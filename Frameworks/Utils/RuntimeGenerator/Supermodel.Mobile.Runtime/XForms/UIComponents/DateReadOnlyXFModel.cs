﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using System;
    using Base;
    using Xamarin.Forms;    
    using ReflectionMapper;
    
    public class DateReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
    {
        #region Constructors
        public DateReadOnlyXFModel()
        {
            TextLabel = new Label { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, LineBreakMode = LineBreakMode.TailTruncation};
            StackLayoutView.Children.Add(TextLabel);
        }
        #endregion

        #region ICstomMapper implementations
        public override object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1}", GetType().Name, objType.Name));
            
            if (obj == null) Value = null;
            else Value = (DateTime)obj; 

            return this;
        }
        // ReSharper disable once RedundantAssignment
        public override object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1}", GetType().Name, objType.Name));
            if (objType == typeof(DateTime) && Value == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
            obj = Value; //This assignment does not do anyhting but we still do it for consistency
            return obj;
        }
        #endregion

        #region Properties
        public DateTime? Value
        {
            get
            {
                DateTime result;
                if (DateTime.TryParse(Text, out result)) return result;
                else return null;
            }
            set
            {
                Text = value == null ? "" : value.Value.ToString("d");//.ToShortDateString();
            }
        }
        public override string Text
        {
            get { return TextLabel.Text; }
            set { TextLabel.Text = value; }
        }
        public Label TextLabel { get; set; }
        public override TextAlignment TextAlignmentIfApplies
        {
            get { return TextLabel.HorizontalTextAlignment; }
            set { TextLabel.HorizontalTextAlignment = value; }
        }
        #endregion
    }
}
