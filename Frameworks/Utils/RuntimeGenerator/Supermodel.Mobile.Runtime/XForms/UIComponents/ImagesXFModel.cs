﻿//ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using System.Linq;
    using ViewModels;
    using System.IO;
    using System;
    using Xamarin.Forms;
    using System.Collections.Generic;
    using Base;
    using ReflectionMapper;
    using Views;
    using CustomCells;
    using Utils;
    using Plugin.Media;
    using Plugin.Media.Abstractions;

    public class ImagesXFModel : BinaryFilesWritableXFModel
    {
        #region Constructors
        public ImagesXFModel()
        {
            DeleteIconFileName = null;
            AddNewCell = new AddNewCell { Text = "Add New Image" };
        }
        #endregion

        #region ISupermodelMobileDetailTemplate implementation
        public override List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue)
        {
            ParentPage = parentPage;
            Cells.Clear();
            var imageIndex = 0;
            foreach (var imageFile in ModelsWithBinaryFileXFModels)
            {
                var file = imageFile;
                var index = imageIndex;

                var cell = new ImageCellWithEditableText
                {
                    ImageSource = ImageSource.FromStream(() => new MemoryStream(file.BinaryFile.BinaryContent)),
                    Text = file.Title,
                    Placeholder = "Enter Image Title",
                };
                cell.TextEntry.TextChanged += (sender, args) => { file.Title = cell.Text; };

                var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
                deleteAction.Clicked += (sender, e) => { ImageDeletedHandler(file, cell); };
                cell.ContextActions.Add(deleteAction);

                //This we do for Andorid -- otherwise the tap is not recognized
                //This is instead of cell.Tapped += (sender, args) => { ImageTappedHandler(index); };
                var tapGestureRecognizer = new TapGestureRecognizer();
                tapGestureRecognizer.Tapped += (s, e) => { ImageTappedHandler(index); };
                cell.Image.GestureRecognizers.Add(tapGestureRecognizer);

                Cells.Add(cell);
                imageIndex++;
            }
            AddNewCell.ParentPage = parentPage;
            AddNewCell.Tapped += (sender, args) => { AddNewTapped(); };
            Cells.Add(AddNewCell);
            return Cells;
        }
        public async void AddNewTapped()
        {
            await CrossMedia.Current.Initialize();

            var options = new List<string>();
            if (CrossMedia.Current.IsPickPhotoSupported) options.Add("Photo Library");
            if (CrossMedia.Current.IsTakePhotoSupported) options.Add("Take Photo");
            var action = await ParentPage.DisplayActionSheet(null, "Cancel", null, options.ToArray());
            try
            {
                MediaFile mediaFile;
                switch (action)
                {
                    case "Photo Library":
                    {
                        mediaFile = await CrossMedia.Current.PickPhotoAsync();
                        break;
                    }
                    case "Take Photo":
                    {
                        var storeCameraMediaOptions = new StoreCameraMediaOptions
                        {
                            DefaultCamera = CameraDevice.Rear,
                            SaveToAlbum = false,
                            Directory = "Media",
                            Name = "pic.jpg"
                        };
                        mediaFile = await CrossMedia.Current.TakePhotoAsync(storeCameraMediaOptions);
                        break;
                    }
                    default: throw new Exception("Invalid photo option. This should never happen");
                }
                if (mediaFile != null)
                {
                    var binaryContent = ReadFully(mediaFile.GetStream());
                    binaryContent = await ImageResizer.ResizeImageAsync(binaryContent, 1024, 1024);

                    //Insert image into ModelsWithBinaryFileXFModels list
                    var file = new ModelWithBinaryFileXFModel { Id = 0, Title = "", BinaryFile = new BinaryFileXFModel { BinaryContent = binaryContent, Name = "image.png" } };
                    ModelsWithBinaryFileXFModels.Add(file);

                    //Insert image cell
                    var cell = new ImageCellWithEditableText
                    {
                        ImageSource = ImageSource.FromStream(() => new MemoryStream(file.BinaryFile.BinaryContent)),
                        Text = file.Title,
                        Placeholder = "Enter Image Title",
                    };
                    cell.TextEntry.TextChanged += (sender, args) => { file.Title = cell.Text; };

                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
                    deleteAction.Clicked += (sender, e) => { ImageDeletedHandler(file, cell); };
                    cell.ContextActions.Add(deleteAction);

                    var tableView = (ViewWithActivityIndicator<TableView>)ParentPage.PropertyGet("DetailView");
                    var section = tableView.ContentView.Root.Single(x => x.Contains(AddNewCell));
                    var index = section.IndexOf(AddNewCell);

                    //This we do for Andorid -- otherwise the tap is not recognized
                    //This is instead of cell.Tapped += (sender, args) => { ImageTappedHandler(index); };
                    var tapGestureRecognizer = new TapGestureRecognizer();
                    tapGestureRecognizer.Tapped += (s, e) => { ImageTappedHandler(index); };
                    cell.Image.GestureRecognizers.Add(tapGestureRecognizer);

                    section.Insert(index, cell);
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception) { }
        }
        public void ImageDeletedHandler(ModelWithBinaryFileXFModel file, Cell cell)
        {
            ModelsWithBinaryFileXFModels.Remove(file);

            var tableView = (ViewWithActivityIndicator<TableView>)ParentPage.PropertyGet("DetailView");
            foreach (var section in tableView.ContentView.Root) section.Remove(cell);
        }
        public async void ImageTappedHandler(int imageIndex)
        {
            var imagesCarouselPage = new CarouselPage();
            foreach (var imageFile in ModelsWithBinaryFileXFModels)
            {
                // ReSharper disable once AccessToForEachVariableInClosure
                var imagePage = new ContentPage
                {
                    Content = new Image
                    {
                        Source = ImageSource.FromStream(() => new MemoryStream(imageFile.BinaryFile.BinaryContent)),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    }
                };
                imagesCarouselPage.Children.Add(imagePage);
            }
            imagesCarouselPage.ToolbarItems.Add(new ToolbarItem("Delete", DeleteIconFileName, () =>
            {
                var deletedImageIndex = imagesCarouselPage.Children.IndexOf(imagesCarouselPage.CurrentPage);
                imagesCarouselPage.Children.RemoveAt(deletedImageIndex);
                ModelsWithBinaryFileXFModels.RemoveAt(deletedImageIndex);

                var tableView = (ViewWithActivityIndicator<TableView>)ParentPage.PropertyGet("DetailView");
                foreach (var section in tableView.ContentView.Root) section.Remove(Cells[deletedImageIndex]);
                Cells.RemoveAt(deletedImageIndex);
            }));
            imagesCarouselPage.CurrentPage = imagesCarouselPage.Children[imageIndex];
            await ParentPage.Navigation.PushAsync(imagesCarouselPage);
        }
        #endregion

        #region Private Helper Methods
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) ms.Write(buffer, 0, read);
                return ms.ToArray();
            }
        }
        #endregion

        #region Properties
        public string DeleteIconFileName { get; set; }

        public override bool ShowDisplayNameIfApplies { get; set; }
        public override string DisplayNameIfApplies { get; set; }
        public override TextAlignment TextAlignmentIfApplies { get; set; }

        protected Page ParentPage { get; set; }
        protected AddNewCell AddNewCell { get; set; }

        public override string ErrorMessage
        {
            get { return AddNewCell.ErrorMessage; }
            set { AddNewCell.ErrorMessage = value; }
        }
        public override bool Required
        {
            get { return AddNewCell.Required; }
            set { AddNewCell.Required = value; }
        }

        public List<Cell> Cells = new List<Cell>();
        #endregion
    }
}
