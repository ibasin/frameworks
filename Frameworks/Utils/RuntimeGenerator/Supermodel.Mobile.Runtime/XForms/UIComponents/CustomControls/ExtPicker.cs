﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
{
    using Xamarin.Forms;
    
    public class ExtPicker : Picker
    {
        #region Contructors
        public ExtPicker()
        {
            Border = true;
        }
        #endregion
        
        #region Properties
        public TextAlignment TextAlignment
        {
            get
            {
                return _textAlign;
            }
            set
            {
                if ( value == _textAlign) return;
                _textAlign = value;
                OnPropertyChanged();
            }
        }
        private TextAlignment _textAlign;

        //public Color TextColor
        //{
        //    get
        //    {
        //        return _textColor;
        //    }
        //    set
        //    {
        //        if ( value == _textColor) return;
        //        _textColor = value;
        //        OnPropertyChanged();
        //    }
        //}
        //private Color _textColor;
        
        public bool Border
        {
            get
            {
                return _border;
            }
            set
            {
                if ( value == _border) return;
                _border = value;
                OnPropertyChanged();
            }
        }
        private bool _border;
        #endregion
    }
}
