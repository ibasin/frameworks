﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using Xamarin.Forms;
    using Base;

    public class MultiLineTextBoxXFModel : SingleCellWritableUIComponentForTextXFModel
    {
        #region Constructors
		public MultiLineTextBoxXFModel()
		{
		    Editor = new Editor { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };
		    Editor.SetBinding(Entry.TextProperty, "Text");
            StackLayoutView.Orientation = StackOrientation.Vertical;
            StackLayoutView.Padding = Device.OnPlatform(new Thickness(8, 10), new Thickness(8, 0), new Thickness(8, 10));
            StackLayoutView.Children.Add(Editor);
            SetHeight(120);
            Tapped += (sender, args) => Editor.Focus();
		}
        #endregion

        #region Properties
        public void SetHeight(double newHeight)
        {
            Height = newHeight;
            Editor.HeightRequest = ShowDisplayNameIfApplies ? newHeight - 50 : newHeight - 20;
        }
        public override string Text
        {
            get { return Editor.Text; }
            set
            {
                if (value == Editor.Text) return;
                Editor.Text = value;
                OnPropertyChanged();
            }
        }
        public Editor Editor { get; set; }
        public override TextAlignment TextAlignmentIfApplies { get; set; }
        #endregion
    }
}
