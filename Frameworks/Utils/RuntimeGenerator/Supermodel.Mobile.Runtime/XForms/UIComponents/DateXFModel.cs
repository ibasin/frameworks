﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using CustomControls;
    using System;
    using Base;
    using Xamarin.Forms;    
    using ReflectionMapper;
    
    public class DateXFModel : SingleCellWritableUIComponentWithoutBackingXFModel, IRMapperCustom
    {
        #region Constructors
		public DateXFModel()
		{
			DatePicker = new ExtDatePicker{ HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Border = false, TextAlignment = TextAlignment.End };
            StackLayoutView.Children.Add(DatePicker);
            Tapped += (sender, args) => DatePicker.Focus();
		}
        #endregion

        #region ICstomMapper implementations
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1}", GetType().Name, objType.Name));
            
            if (obj == null) Value = null;
            else Value = (DateTime)obj; 

            return this;
        }
        // ReSharper disable once RedundantAssignment
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1}", GetType().Name, objType.Name));
            if (objType == typeof(DateTime) && Value == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
            obj = Value; //This assignment does not do anyhting but we still do it for consistency
            return obj;
        }
        #endregion

        #region Properties
        public DateTime? Value
        {
            get
            {
                return DatePicker.Date;
            } 
            set
            {
                if (value == null) DatePicker.Date = DateTime.Today;
                else DatePicker.Date = value.Value;
            }
        }

        public ExtDatePicker DatePicker { get; set; }

        public override object WrappedValue
        {
            get { return Value; }
        }
        public override TextAlignment TextAlignmentIfApplies
        {
            get { return DatePicker.TextAlignment; }
            set { DatePicker.TextAlignment = value; }
        }
        #endregion
    }
}
