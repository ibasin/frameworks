﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using Xamarin.Forms;
    using Base;

    public class MultiLineTextBoxReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
    {
        #region Constructors
		public MultiLineTextBoxReadOnlyXFModel()
		{
            TextLabel = new Label();
            ScrollView = new ScrollView{ Content = TextLabel, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };
            
		    TextLabel.SetBinding(Entry.TextProperty, "Text");
            StackLayoutView.Orientation = StackOrientation.Vertical;
            StackLayoutView.Padding = Device.OnPlatform(8, new Thickness(8, 10), 8);
            StackLayoutView.Children.Add(ScrollView);
            SetHeight(120);
		}
        #endregion

        #region Properties
        public void SetHeight(double newHeight)
        {
            Height = newHeight;
            ScrollView.HeightRequest = ShowDisplayNameIfApplies ? newHeight - 50 : newHeight - 20;
        }
        public override string Text
        {
            get { return TextLabel.Text; }
            set
            {
                if (value == TextLabel.Text) return;
                TextLabel.Text = value;
                OnPropertyChanged();
            }
        }
        public ScrollView ScrollView { get; set; }
        public Label TextLabel { get; set; }
        public override TextAlignment TextAlignmentIfApplies { get; set; }
        #endregion

    }
}
