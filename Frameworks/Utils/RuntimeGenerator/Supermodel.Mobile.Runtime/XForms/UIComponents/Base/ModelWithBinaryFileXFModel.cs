// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using ReflectionMapper;
    using ViewModels;
    using System;
    using Models;
    
    public class ModelWithBinaryFileXFModel : IRMapperCustom
    {
        #region IRMapperCustom implementation
        public object MapFromObjectCustom(object obj, Type objType)
        {
            var modelWithBinaryFile = (IModelWithBinaryFile)obj;
            Title = modelWithBinaryFile.GetTitle();
            BinaryFile = new BinaryFileXFModel { Name = modelWithBinaryFile.GetBinaryFile().Name, BinaryContent = modelWithBinaryFile.GetBinaryFile().BinaryContent };
            return this.MapFromObjectCustomBase(obj);
        }
        public object MapToObjectCustom(object obj, Type objType)
        {
            var modelWithBinaryFile = (IModelWithBinaryFile)obj;
            modelWithBinaryFile.SetTitle(Title);
            modelWithBinaryFile.SetBinaryFile(new BinaryFile { Name = BinaryFile.Name, BinaryContent = BinaryFile.BinaryContent });
            return this.MapToObjectCustomBase(obj);
        }
        #endregion

        #region Properties
        public long Id { get; set; }
        [NotRMapped] public string Title { get; set; }
        [NotRMapped] public BinaryFileXFModel BinaryFile { get; set; }
        #endregion
    }
}