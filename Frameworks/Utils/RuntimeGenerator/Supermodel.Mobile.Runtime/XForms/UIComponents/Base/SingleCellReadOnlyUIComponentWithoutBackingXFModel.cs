 // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using System;
    using System.Collections.Generic;
    using Xamarin.Forms;    
    
    public abstract class SingleCellReadOnlyUIComponentWithoutBackingXFModel : ViewCell, IReadOnlyUIComponentXFModel
    {
        #region Constructors
        protected SingleCellReadOnlyUIComponentWithoutBackingXFModel()
        {
            View = new StackLayout
            {
                Padding = new Thickness(8, 0, 8, 0), 
                Orientation = StackOrientation.Horizontal, 
                VerticalOptions = LayoutOptions.CenterAndExpand, 
                HorizontalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 40,
            };
			
            DisplayNameLabel = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, TextColor = Color.Gray, LineBreakMode = LineBreakMode.NoWrap }; 
            if (ShowDisplayNameIfApplies) StackLayoutView.Children.Add(DisplayNameLabel);
        }
        #endregion

        #region ISupermodelMobileDetailTemplate implemetation
        public List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue)
        {
            ParentPage = parentPage;
            return new List<Cell> { this };
        }
        #endregion

        #region Properties
        public StackLayout StackLayoutView
        {
            get { return (StackLayout)View; } 
            set { View = value; }
        }

        public bool ShowDisplayNameIfApplies
        {
            get
            {
                return _showDisplayNameIfApplies;
            }
            set
            {
                if (_showDisplayNameIfApplies == value) return;
                if (value) StackLayoutView.Children.Insert(0, DisplayNameLabel);
                else StackLayoutView.Children.RemoveAt(StackLayoutView.Children.IndexOf(DisplayNameLabel));
                _showDisplayNameIfApplies = value;
            }
        }
        private bool _showDisplayNameIfApplies = true;

        public string DisplayNameIfApplies
        {
            get { return DisplayNameLabel.Text; }
            set { DisplayNameLabel.Text = value; }
        }
        public Label DisplayNameLabel { get; set; }

        public Page ParentPage { get; set; }

        public abstract TextAlignment TextAlignmentIfApplies { get; set; }
        #endregion    
    }
}