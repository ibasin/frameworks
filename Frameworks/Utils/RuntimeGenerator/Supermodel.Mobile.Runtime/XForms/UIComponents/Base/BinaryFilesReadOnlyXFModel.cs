﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Exceptions;
    using ReflectionMapper;
    using Xamarin.Forms;
    using ViewModels;

    public abstract class BinaryFilesReadOnlyXFModel : IReadOnlyUIComponentXFModel, IRMapperCustom
    {
        #region Constructors
        protected BinaryFilesReadOnlyXFModel()
        {
            ModelsWithBinaryFileXFModels = new ObservableCollection<ModelWithBinaryFileXFModel>();
        }
        #endregion

        #region Custom Mapper implementation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (!(obj is IEnumerable<IModelWithBinaryFile>)) throw new SupermodelException(GetType().Name + " can only map to Lists of type that implements IModelWithBinaryFile");
            var modelsWithImage = (IEnumerable<IModelWithBinaryFile>)obj;
            
            ModelsWithBinaryFileXFModels = ModelsWithBinaryFileXFModels ?? new ObservableCollection<ModelWithBinaryFileXFModel>();
            ModelsWithBinaryFileXFModels.Clear();
            foreach (var modelWithImage in modelsWithImage) ModelsWithBinaryFileXFModels.Add(new ModelWithBinaryFileXFModel().MapFrom(modelWithImage));
            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            //its read-only, so we do nothing
            return obj; 
        }
        #endregion

        #region Methods
        public abstract List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue);
        #endregion

        #region Properties
        public ObservableCollection<ModelWithBinaryFileXFModel> ModelsWithBinaryFileXFModels { get; private set; }

        public abstract bool ShowDisplayNameIfApplies { get; set; }
        public abstract string DisplayNameIfApplies { get; set; }
        public abstract TextAlignment TextAlignmentIfApplies { get; set; }
        #endregion
    }
}