﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using Xamarin.Forms; 
    using CustomControls; 
    using Base;  
    
    public class TextBoxXFModel : SingleCellWritableUIComponentForTextXFModel
    {
        #region Constructors
		public TextBoxXFModel()
		{
			TextEntry = new ExtEntry { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Border = false, TextAlignment = TextAlignment.End, WidthRequest = 1 };
            TextEntry.SetBinding(Entry.TextProperty, "Text");
            StackLayoutView.Children.Add(TextEntry);
            Tapped += (sender, args) => TextEntry.Focus();
		}
        #endregion

        #region Properties
        public override string Text
        {
            get { return TextEntry.Text; }
            set
            {
                if (value == TextEntry.Text) return;
                TextEntry.Text = value;
                OnPropertyChanged();
            }
        }
        public ExtEntry TextEntry { get; set; }
        public override TextAlignment TextAlignmentIfApplies
        {
            get { return TextEntry.TextAlignment; }
            set { TextEntry.TextAlignment = value; }
        }
        #endregion
    }
}
