﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Views
{
    using Xamarin.Forms;

    public class ViewWithActivityIndicator<ContentViewT> : AbsoluteLayout, IHaveActivityIndicator where ContentViewT : View
    {
        #region Constructors
        public ViewWithActivityIndicator(ContentViewT contentView)
        {
            ContentView = contentView;
            ActivityIndicator = new ActivityIndicator();
            MessageLabel = new Label { TextColor = Device.OnPlatform(Color.Black, Color.White, Color.Black), Text = Message };
            ActivityIndicatorAndMessageStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            ActivityIndicatorAndMessageStackLayout.Children.Add(ActivityIndicator);
            ActivityIndicatorAndMessageStackLayout.Children.Add(MessageLabel);

            GrayOutOverlay = new BoxView { Color = new Color (0, 0, 0, 0.4) };

            HorizontalOptions = LayoutOptions.FillAndExpand;
            VerticalOptions = LayoutOptions.FillAndExpand;

            SetLayoutFlags(ContentView, AbsoluteLayoutFlags.All);
            SetLayoutBounds(ContentView, new Rectangle(0, 0, 1f, 1f));

            SetLayoutFlags(GrayOutOverlay, AbsoluteLayoutFlags.All);
            SetLayoutBounds(GrayOutOverlay, new Rectangle(0, 0, 1f, 1f));

            SetLayoutFlags(ActivityIndicatorAndMessageStackLayout, AbsoluteLayoutFlags.PositionProportional);
            SetLayoutBounds(ActivityIndicatorAndMessageStackLayout, new Rectangle(0.5, 0.5, AutoSize, AutoSize));
            
            Children.Add(ContentView);
            Children.Add(GrayOutOverlay);
            Children.Add(ActivityIndicatorAndMessageStackLayout);
            ActivityIndicatorOn = false;
        }
        #endregion

        #region IHaveActivityIndicator implementation
        public string Message 
        {
            get { return _message;  }
            set { _message = MessageLabel.Text = value; }
        }
        private string _message = null;
        public bool ActivityIndicatorOn
        {
            get
            {
                return _activityIndicatorOn;
            }
            set
            {
                _activityIndicatorOn = value;
                ActivityIndicator.IsEnabled = ActivityIndicator.IsRunning = ActivityIndicator.IsVisible = GrayOutOverlay.IsEnabled = GrayOutOverlay.IsVisible = value;
                MessageLabel.IsVisible = value;
            }
        }
        private bool _activityIndicatorOn;
        #endregion

        #region Properties
        public BoxView GrayOutOverlay { get; set; }
        public StackLayout ActivityIndicatorAndMessageStackLayout { get; set; }
        public ActivityIndicator ActivityIndicator { get; set; }
        public Label MessageLabel { get; set; }
        
        public ContentViewT ContentView { get; set; }
        #endregion
    }
}