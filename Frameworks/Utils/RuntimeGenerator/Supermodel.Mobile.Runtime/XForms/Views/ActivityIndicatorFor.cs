﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Views
{
    using System;
    
    public class ActivityIndicatorFor : IDisposable
    {
        public ActivityIndicatorFor(IHaveActivityIndicator view, string message = null)
        {
            View = view;
            View.ActivityIndicatorOn = true;
            View.Message = message;
        }
        
        public void Dispose()
        {
             View.ActivityIndicatorOn = false;
        }

        // ReSharper disable once InconsistentNaming
        public IHaveActivityIndicator View { get; set; }
    }
}
