﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Views
{
    public interface IHaveActivityIndicator
    {
        bool ActivityIndicatorOn { get; set; }
        string Message { get; set; }
    }
}