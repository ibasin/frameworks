﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.App
{
    using Xamarin.Forms;

    public class SupermodelXamarinFormsApp : Application
    {
        public SupermodelXamarinFormsApp()
        {
            FormsApplication.SetRunningApp(this); 
        }
    }
}
