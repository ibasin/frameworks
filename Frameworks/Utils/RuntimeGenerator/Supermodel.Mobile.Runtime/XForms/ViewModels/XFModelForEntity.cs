// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Models;
    using ReflectionMapper;    
    
    public abstract class XFModelForEntity<ModelT> : XFModel, IObjectWithIdentity, IRMapperCustom where ModelT : class, IModel, new()
    {
        #region IModelWithIdentity implementation
        [ScaffoldColumn(false)] public virtual string Identity => Id.ToString();
        [ScaffoldColumn(false)] public virtual bool IsNew => Id == 0;
        #endregion
        
        #region ICustom mapper implementation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            return this.MapFromObjectCustomBase(obj);
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            return this.MapToObjectCustomBase(obj);
        }
        #endregion

        #region Validation
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
            var tempEntityForValidation = CreateTempValidationEntity();
            Validator.TryValidateObject(tempEntityForValidation, new ValidationContext(tempEntityForValidation), vr); 
            return vr;
        }
        #endregion

        #region Private Helper Methods
        protected virtual ModelT CreateTempValidationEntity()
        {
            return (ModelT)this.MapToObject(new ModelT());
        }
        #endregion

        #region Standard Properties
        [ScaffoldColumn(false), NotRMappedTo] public virtual long Id { get; set; }
        #endregion
    }
}