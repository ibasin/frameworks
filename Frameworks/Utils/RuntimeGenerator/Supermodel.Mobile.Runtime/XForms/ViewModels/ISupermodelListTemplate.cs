// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using Xamarin.Forms;
    using System.ComponentModel;
    using System;

    public interface ISupermodelListTemplate : INotifyPropertyChanged
    {
        DataTemplate GetListCellDataTemplate(EventHandler deleteHandler);
        void OnPropertyChanged(string propertyName);
    }
}