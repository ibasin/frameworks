﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.Threading.Tasks;
    using Encryptor;    
    
    public interface IAuthHeaderGenerator
    {
        AuthHeader CreateAuthHeader();
    }
}