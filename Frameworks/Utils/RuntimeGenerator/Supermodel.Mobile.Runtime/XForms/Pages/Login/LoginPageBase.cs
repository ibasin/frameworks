﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.Threading.Tasks;
    using DataContext.WebApi; 
    using Models;
    using UnitOfWork;

    public abstract class LoginPageBase<LoginViewModelT, LoginViewT, LoginValidationModelT, WebApiDataContextT> : LoginPageCore<LoginViewModelT, LoginViewT>
        where LoginViewModelT: ILoginViewModel, new()
        where LoginViewT : LoginViewBase<LoginViewModelT>, new()
        where LoginValidationModelT : class, IModel
        where WebApiDataContextT : WebApiDataContext, new()
    {
        #region Overrides
        public override async Task<LoginResult> TryLoginAsync()
        {
            using (new UnitOfWork<WebApiDataContextT>(ReadOnly.Yes))
            {
                UnitOfWorkContext.AuthHeader = AppContextAuthHeaderGenerator.CreateAuthHeader();
                return await UnitOfWorkContext.ValidateLoginAsync<LoginValidationModelT>();
            }
        }
        #endregion
    }
}
