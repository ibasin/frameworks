﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.ComponentModel;
    using System.Threading.Tasks;

    public interface ILoginViewModel : INotifyPropertyChanged, IAuthHeaderGenerator
    {
        string GetValidationError();
        
        bool LoadFromAppProperties();
        Task SaveToAppPropertiesAsync();

        void Clear();
        Task ClearAndSaveToPropertiesAsync();
        
        byte[] GetEncryptionKey();

        string UserLabel { get; set; }
    }
}
