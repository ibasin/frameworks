// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using DataContext.WebApi;
    using Exceptions;
    using Views;
    using Xamarin.Forms;    
    
    public abstract class LoginPageCore<LoginViewModelT, LoginViewT> : ContentPage, IHaveActivityIndicator
        where LoginViewModelT: ILoginViewModel, new()
        where LoginViewT : LoginViewBase<LoginViewModelT>, new()
    {
        #region Contructors
        protected LoginPageCore()
        {
            Content = LoginView = new ViewWithActivityIndicator<LoginViewT>(new LoginViewT());
            AutologinIfConnectionLost = false;
            LoginView.ContentView.SetUpSignInClickedHandler(SignInClicked);
        }
        #endregion

        #region Methods
        public abstract Task<LoginResult> TryLoginAsync();
        public async Task AutologinIfPossibleAsync()
        {
            var loginViewModel = new LoginViewModelT();
            if (loginViewModel.LoadFromAppProperties())
            {
                bool connectionLost;
                do
                {
                    connectionLost = false;
                    try
                    {
                        using(var activityIndicator = new ActivityIndicatorFor(LoginView, "Logging you in..."))
                        {
                            AppContextAuthHeaderGenerator = loginViewModel;
                            var loginResult = await TryLoginAsync();
                            if (loginResult.LoginSuccessful)
                            {
                                if (!string.IsNullOrEmpty(loginResult.UserLabel)) activityIndicator.View.Message = "Welcome, " + loginResult.UserLabel + "!";
                                loginViewModel.UserLabel = loginResult.UserLabel;
                                if (await DoLoginAsync(true, false)) await loginViewModel.SaveToAppPropertiesAsync();                                
                            }
                            else
                            {
                                await loginViewModel.ClearAndSaveToPropertiesAsync();
                                AppContextAuthHeaderGenerator = null;
                            }
                        }
                    }
                    catch (SupermodelWebApiException) { connectionLost = true; }
                    catch (WebException) { connectionLost = true; }
                    catch (Exception) { connectionLost = true; }
                    if (connectionLost)
                    {
                        if (AutologinIfConnectionLost)
                        {
                            await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Work Offline");
                            using (new ActivityIndicatorFor(LoginView))
                            {
                                await DoLoginAsync(true, false);
                            }
                            return;
                        }
                        
                        var result = await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Cancel", "Try again");
                        if (result) return;
                    }
                }
                while(connectionLost);            
            }
        }
        #endregion

        #region Overrides
        protected virtual void OnDisappearingBase()
        {
            // ReSharper disable once RedundantBaseQualifier
            base.OnDisappearing();
        }
        protected virtual void OnAppearingBase()
        {
            // ReSharper disable once RedundantBaseQualifier
            base.OnAppearing();
        }
        protected override async void OnAppearing()
        {
            if (_trueTitle != null)
            {
                Title = _trueTitle;
                _trueTitle = null;
            }
            
            //If Sign Out is clicked
            if (_loggedIn)
            {
                var loginViewModel = LoginView.ContentView.ViewModel;

                var answer = await DisplayAlert("Alert", "Are you sure you want to sign out?", "Yes", "No");
                if (answer)
                {
                    await loginViewModel.ClearAndSaveToPropertiesAsync();
                    AppContextAuthHeaderGenerator = null;
                    _loggedIn = false;
                }     
                else 
                {
                    await DoLoginAsync(true, true);
                }
            }
            OnAppearingBase();
        }
        protected virtual async Task<bool> DoLoginAsync(bool autologin, bool isJumpBack)
        {
            var result = await OnSuccessfulLoginAsync(autologin, isJumpBack);
            if (result)
            {
                _loggedIn = true;
                _trueTitle = Title;
                Title = "Sign Out";
            }
            return result;
        }
        public abstract Task<bool> OnSuccessfulLoginAsync(bool autologin, bool isJumpBack);
        public abstract IAuthHeaderGenerator AppContextAuthHeaderGenerator { get; set; }
        #endregion

        #region IHaveActivityIndicator implementation
        public bool ActivityIndicatorOn
        {
            get { return LoginView.ActivityIndicatorOn; }
            set { LoginView.ActivityIndicatorOn = value; }
        }
        public string Message
        {
            get { return LoginView.Message; }
            set { LoginView.Message = value; }
        }
        #endregion

        #region Event Handlers
        public virtual async void SignInClicked(object sender, EventArgs args)
        {
            var loginViewModel = LoginView.ContentView.ViewModel;
            var validationError = loginViewModel.GetValidationError();
            if (validationError != null)
            {
                AppContextAuthHeaderGenerator = null;
                await DisplayAlert("Login", validationError, "Ok");
                return;
            }

            bool connectionLost;
            do
            {
                connectionLost = false;
                try
                {
                    using(var activityIndicator = new ActivityIndicatorFor(LoginView, "Logging you in..."))
                    {
                        AppContextAuthHeaderGenerator = loginViewModel;
                        var loginResult = await TryLoginAsync();
                        if (loginResult.LoginSuccessful)
                        {
                            if (!string.IsNullOrEmpty(loginResult.UserLabel)) activityIndicator.View.Message = "Welcome, " + loginResult.UserLabel + "!";
                            loginViewModel.UserLabel = loginResult.UserLabel;
                            if (await DoLoginAsync(false, false)) await loginViewModel.SaveToAppPropertiesAsync();
                        }
                        else
                        {
                            AppContextAuthHeaderGenerator = null;
                            await DisplayAlert("Unable to sign in", "Username and password combination provided is invalid. Please try again.", "Ok");
                        }
                    }
                }
                catch (SupermodelWebApiException) { connectionLost = true; }
                catch (WebException) { connectionLost = true; }
                catch (Exception) { connectionLost = true; }
                if (connectionLost)
                {
                    var result = await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Cancel", "Try again");
                    if (result) return;
                }
            }
            while(connectionLost);
        }
        #endregion

        #region Properties
        public ViewWithActivityIndicator<LoginViewT> LoginView { get; set; }
        public bool AutologinIfConnectionLost { get; set; }
        
        // ReSharper disable InconsistentNaming
        protected bool _loggedIn = false;
        protected string _trueTitle;
        // ReSharper restore InconsistentNaming
        #endregion        
    }
}