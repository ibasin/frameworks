﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Encryptor;
    using Exceptions;
    using Xamarin.Forms;    
    
    public class UsernameAndPasswordLoginViewModel : ILoginViewModel	
    {
        #region InotifyPropertyChanged implementation
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        public virtual AuthHeader CreateAuthHeader()
        {
            return HttpAuthAgent.CreateBasicAuthHeader(Username, Password);
        }
        public virtual void Clear()
        {
            Username = Password = "";
        }
        public virtual async Task ClearAndSaveToPropertiesAsync()
        {
            Clear();
            Application.Current.Properties["smUsername"] = null;
            Application.Current.Properties["smPasswordCode"] = null;
            Application.Current.Properties["smPasswordIV"] = null;
            await Application.Current.SavePropertiesAsync();
        }
        public virtual string GetValidationError()
        {
            if  (string.IsNullOrWhiteSpace(Username) || string.IsNullOrWhiteSpace(Password)) return "Please enter valid login credentials.";
            else return null;
        }
        public virtual async Task SaveToAppPropertiesAsync()
        {
            var vr = GetValidationError();
            if (vr != null) throw new SupermodelException(vr);
            
            byte[] passwordIV;
            var passwordCode = EncryptorAgent.Lock(GetEncryptionKey(), Password, out passwordIV);

            Application.Current.Properties["smUsername"] = Username;
            Application.Current.Properties["smPasswordCode"] = passwordCode;
            Application.Current.Properties["smPasswordIV"] = passwordIV;
            Application.Current.Properties["smUserLabel"] = UserLabel;
            await Application.Current.SavePropertiesAsync();
        }
        public virtual bool LoadFromAppProperties()
        {
            if (Application.Current.Properties.ContainsKey("smUsername") && 
                Application.Current.Properties.ContainsKey("smPasswordCode") && 
                Application.Current.Properties.ContainsKey("smPasswordIV") && 
                Application.Current.Properties.ContainsKey("smUserLabel"))
            {
                var username = Application.Current.Properties["smUsername"] as string;
                if (username == null) return false;

                var passwordCode = Application.Current.Properties["smPasswordCode"] as byte[];
                if (passwordCode == null) return false;

                var passwordIV = Application.Current.Properties["smPasswordIV"] as byte[];
                if (passwordIV == null) return false;

                //User label can be null
                var userLabel = Application.Current.Properties["smUserLabel"] as string;

                Password = EncryptorAgent.Unlock(GetEncryptionKey(), passwordCode, passwordIV);
                Username = username;
                UserLabel = userLabel;
                return true;
            }
            else
            {
                return false;
            }
        }
        public virtual byte[] GetEncryptionKey()
        {
            return _key;
        }
        private readonly static byte[] _key = { 0xB6, 0x46, 0x12, 0xA1, 0xEA, 0x16, 0x52, 0xA0, 0xB2, 0x41, 0x2A, 0x5C, 0x23, 0x8C, 0xF0, 0xAD };
        #endregion

        #region Properties
        public string Username
        {
            get { return _username; }
            set
            {
                if (value == _username) return;
                _username = value;
                OnPropertyChanged();
            }
        }
        private string _username;

        public string Password
        {
            get { return _password; }
            set
            {
                if (value == _password) return;
                _password = value;
                OnPropertyChanged();
            }
        }
        private string _password;

        public string UserLabel
        {
            get { return _userLabel; }
            set
            {
                if (value == _userLabel) return;
                _userLabel = value;
                OnPropertyChanged();
            }
        }
        private string _userLabel;
        #endregion
    }
}
