﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using System.Collections.ObjectModel;
    using Xamarin.Forms;
    using ViewModels;
    using ReflectionMapper;
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Exceptions;
    using Encryptor;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Views;
    using Utils;
    using Models;
    using Newtonsoft.Json;
    using System.Reflection;

    public abstract class CRUDDetailPage<ModelT, XFModelT> : ContentPage, IHaveActivityIndicator
        where ModelT : class, ISupermodelListTemplate, IObjectWithIdentity, new()
        where XFModelT : XFModel, new()
    {
        #region Initializers
        public virtual CRUDDetailPage<ModelT, XFModelT> Init(ObservableCollection<ModelT> models, string title, ModelT model)
        {
            Title = title;

            Models = models;
            Model = model;
            XFModel = new XFModelT().MapFrom(model);
            OriginalXFModel = new XFModelT().MapFrom(model);

            Content = DetailView = new CRUDDetailView<ModelT, XFModelT>(this);

            return this;
        }
        public virtual CRUDDetailPage<ModelT, XFModelT> Init(ObservableCollection<ModelT> models, string title, ModelT model, XFModelT xfModel, XFModelT originalXFModel)
        {
            Title = title;

            Models = models;
            Model = model;
            XFModel = xfModel;
            OriginalXFModel = originalXFModel;

            Content = DetailView = new CRUDDetailView<ModelT, XFModelT>(this);

            return this;
        }
        #endregion

        #region Overrides
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (XFModel.ContainsValidationErrros()) await DisplayAlert("Validation Errors", "Please correct problems with fields marked with '!'", "Ok");
        }
        protected override async void OnDisappearing()
        {
            await OnDisappearingInternalAsync();
        }
        protected virtual async Task<bool> OnDisappearingInternalAsync()
        {
            var goingBack = (Navigation.NavigationStack.Last() == this);
            var parentPage = Navigation.NavigationStack[Navigation.NavigationStack.Count - (goingBack ? 2 : 3)];
            var childPage = goingBack ? null : Navigation.NavigationStack.Last();
            var pageShowing = goingBack ? parentPage : childPage;
            
            //First, finish disapearing :)
            base.OnDisappearing();

            //Map to Model
            var originalModelHash = ComputeModelHash(Model);
            ValidationResultList mappingVr = null;
            try
            {
                Model = XFModel.MapTo(Model);
            }
            catch (ValidationResultException ex)
            {
                mappingVr = ex.ValidationResultList;
            }
            if (mappingVr != null && mappingVr.Any())
            {
				//if we had mapping validation errors
				Model = OriginalXFModel.MapTo(Model); 
				XFModel.ShowValidationErrors(mappingVr);
				if (goingBack)
				{
                    var page = (CRUDDetailPage<ModelT, XFModelT>)ReflectionHelper.CreateType(GetType());
				    await parentPage.Navigation.PushAsync(page.Init(Models, Title, Model, XFModel, OriginalXFModel));
				}
                else
				{
				    await parentPage.Navigation.PopAsync();     
				}
				return false;
            }

            //Try to validate locally. We validate even if the hash did not chnage since we only calculate hash to perisitant fields
            var localVr = new ValidationResultList();
            if (!Validator.TryValidateObject(XFModel, new ValidationContext(XFModel), localVr))
            {
                //if we had local validation errors
                Model = OriginalXFModel.MapTo(Model); 
                XFModel.ShowValidationErrors(localVr);
                if (goingBack)
                {
                    var page = (CRUDDetailPage<ModelT, XFModelT>)ReflectionHelper.CreateType(GetType());
                    await parentPage.Navigation.PushAsync(page.Init(Models, Title, Model, XFModel, OriginalXFModel));
                }
                else
                {
				    await parentPage.Navigation.PopAsync();     
                }
                return false;
            }

			//if Model did not change, we don't need to save it
			if (ComputeModelHash(Model) == originalModelHash) 
			{
				//This is in case we had vaication errors in the XFModel to begin with
                XFModel.ClearValidationErrors();
                return true;
			}

            //Try to save to DataContext
            bool connectionLost;
            ValidationResultList serverVr = null;
            do
            {
                connectionLost = false;
                var itemNotFound = false;
                try
                {
                    var pageShowingActivityIndicator = pageShowing as IHaveActivityIndicator;
                    if (pageShowingActivityIndicator != null)
                    {
                        using(new ActivityIndicatorFor(pageShowingActivityIndicator))
                        {
                            await SaveItemInternalAsync(Model);
                        }
                    }
                    else
                    {
                        await SaveItemInternalAsync(Model);
                    }
				    if (!Model.IsNew && Models.All(x => x.Identity != Model.Identity)) Models.Add(Model);
                }
                catch (SupermodelWebApiException ex)
                {
                    if (ex.StatusCode == HttpStatusCode.Unauthorized) UnauthorizedHandler();
                    else if (ex.StatusCode == HttpStatusCode.NotFound) itemNotFound = true;
                    else connectionLost = true;
                }
                catch (SupermodelDataContextValidationException ex)
                {
                    var vrl = ex.GetListOfValidationResultLists();
                    if (vrl.Count != 1) throw new SupermodelException("CRUDDetailPage: vrl.Count != 1. This should never happen!");
                    serverVr = vrl[0];
                }
                catch (WebException) { connectionLost = true; }
                catch (Exception) { connectionLost = true; }
                if (connectionLost) await pageShowing.DisplayAlert("Connection Lost", "Connection to teh cloud cannot be established.", "Try again");
                if (itemNotFound)
                {
                    Models.RemoveAll(x => x == Model);
                    await pageShowing.DisplayAlert("Not Found", "Item you are trying to update no longer exists.", "Ok");
                }
            } 
            while (connectionLost);  

            if (serverVr != null && serverVr.Any())
            {
                //if we had any validation errors while trying to save to DataContext
                Model = OriginalXFModel.MapTo(Model); 
                XFModel.ShowValidationErrors(serverVr);

                if (goingBack)
                {
                    var page = (CRUDDetailPage<ModelT, XFModelT>)ReflectionHelper.CreateType(GetType());
                    await parentPage.Navigation.PushAsync(page.Init(Models, Title, Model, XFModel, OriginalXFModel));
                }
                else
                {
				    await parentPage.Navigation.PopAsync();   
                }
                return false;
            }
            //If no validation issues, mark all properteis as changed. This way the list will always update
            foreach (var property in Model.GetType().GetTypeInfo().DeclaredProperties) Model.OnPropertyChanged(property.Name);
            
            //This is in case we had vaication errors in the XFModel to begin with
            XFModel.ClearValidationErrors();
            
            return true;
        }
        protected abstract Task SaveItemInternalAsync(ModelT model);
        protected abstract void UnauthorizedHandler();
        protected virtual string ComputeModelHash(ModelT model)
        {
            //We hash Json to ingore changes that do not get persisted
            return JsonConvert.SerializeObject(Model).GetMD5Hash();
        }
        #endregion

        #region Methods
        public virtual void InitView(CRUDDetailView<ModelT, XFModelT> view)
        {
            var section = new TableSection();
            view.ContentView.Root.Add(section);
            
            var cells = XFModel.RenderDetail(this);
            foreach (var cell in cells) section.Add(cell);
        }
        #endregion

        #region IHaveActivityIndicator implementation
        public bool ActivityIndicatorOn
        {
            get { return DetailView.ActivityIndicatorOn; }
            set { DetailView.ActivityIndicatorOn = value; }
        }
        public string Message
        {
            get { return DetailView.Message; }
            set { DetailView.Message = value; }
        }
        #endregion
        
        #region Properties
        public CRUDDetailView<ModelT, XFModelT> DetailView { get; set; }
        public ObservableCollection<ModelT> Models { get; set; } 
        public ModelT Model { get; set; }
        public XFModelT XFModel { get; set; }
        public XFModelT OriginalXFModel { get; set; }
        #endregion
    }
}
