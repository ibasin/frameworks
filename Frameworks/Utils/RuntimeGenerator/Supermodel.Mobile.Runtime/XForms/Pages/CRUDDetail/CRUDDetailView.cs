﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using ViewModels;
    using Xamarin.Forms;
    using Views;
    using Models;
    
    public class CRUDDetailView<ModelT, XFModelT> : ViewWithActivityIndicator<TableView>
        where ModelT : class, ISupermodelListTemplate, IObjectWithIdentity, new()
        where XFModelT : XFModel, new()
    {
        public CRUDDetailView(CRUDDetailPage<ModelT, XFModelT> parentPage) : base(new TableView
        {
            Intent = TableIntent.Form,
            HasUnevenRows = true,
            Root = new TableRoot()
        })
        {
            parentPage.InitView(this);
        }
    }
}
