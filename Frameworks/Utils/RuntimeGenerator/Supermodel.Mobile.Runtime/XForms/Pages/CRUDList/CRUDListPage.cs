﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDList
{
    using System.Linq;
    using System;
    using System.Collections.ObjectModel;
    using System.Net;
    using System.Threading.Tasks;
    using Exceptions;
    using Xamarin.Forms;
    using ViewModels;
    using Views;
    using System.Collections.Generic;
    using Models;

    public abstract class CRUDListPage<ModelT> : CRUDListPageCore<ModelT> where ModelT : class, ISupermodelListTemplate, IObjectWithIdentity, new()
    {
        #region Initializers
        public override CRUDListPageCore<ModelT> Init(string title, int take = 25, bool readOnly = false)
        {
            if (readOnly) Content = ListView = new CRUDListView<ModelT>(null, false);
            else Content = ListView = new CRUDListView<ModelT>(DeleteItemHandler, false);
            base.Init(title, take, readOnly);
            ListView.ListPanel.ContentView.Refreshing += RefreshingHandler;
            return this;
        }
        #endregion
        
        #region Event Handlers
        public override async void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args)
        {
            if (LoadedAll || LoadingInProgress) return;
            if (Models.Last() == args.Item) await LoadListContentAsync(Models.Count, Take);
        }
        public virtual async void RefreshingHandler(object sender, EventArgs args)
        {
            Models = null;
            await LoadListContentAsync(showActivityIndicator: false);
            ListView.ListPanel.ContentView.IsRefreshing = false;
        }
        #endregion

        #region Overrides
        protected abstract Task<List<ModelT>> GetItemsInternalAsync(int skip, int? take);
        #endregion

        #region Methods
        public virtual async Task LoadListContentAsync(int skip = 0, int? take = -1, bool showActivityIndicator = true)
        {
            if (take < 0) take = Take;
            bool connectionLost;
            do
            {
                connectionLost = false;
                try
                {
                    LoadingInProgress = true;
                    LoadedAll = false;
                    
                    if (Models == null) Models = new ObservableCollection<ModelT>();
                    if (skip == 0) Models.Clear();
                    
                    if (showActivityIndicator)
                    {
                        using(new ActivityIndicatorFor(ListView.ListPanel))
                        {
                            var models = await GetItemsInternalAsync(skip, take);
                            if (take == null || models.Count < take) LoadedAll = true;

                            foreach (var model in models)
                            {
                                if (Models.All(x => x.Identity != model.Identity)) Models.Add(model);
                            }
                            ListView.ListPanel.ContentView.ItemsSource = Models;
                        }
                    }
                    else
                    {
                        var models = await GetItemsInternalAsync(skip, take);
                        if (take == null || models.Count < take) LoadedAll = true;

                        foreach (var model in models)
                        {
                            if (Models.All(x => x.Identity != model.Identity)) Models.Add(model);
                        }
                        ListView.ListPanel.ContentView.ItemsSource = Models;
                    }
                }
                catch (SupermodelWebApiException ex)
                {
                    if (ex.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        UnauthorizedHandler();
                        return;
                    }
                    connectionLost = true;
                }
                catch (WebException) { connectionLost = true; }
                catch (Exception) { connectionLost = true; }
                finally { LoadingInProgress = false; }
                if (connectionLost) await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
            } 
            while (connectionLost);
        }
        #endregion
    }
}
