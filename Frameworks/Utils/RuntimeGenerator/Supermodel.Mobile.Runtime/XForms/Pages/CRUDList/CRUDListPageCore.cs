﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDList
{
    using ViewModels;
    using Xamarin.Forms;
    using System.Threading.Tasks;
    using System;
    using System.Net;
    using Exceptions;
    using Views;
    using System.Collections.ObjectModel;
    using Utils;
    using Models;

    public abstract class CRUDListPageCore<ModelT> : ContentPage, IHaveActivityIndicator where ModelT : class, ISupermodelListTemplate, IObjectWithIdentity, new()
    {
        #region Initializers
        public virtual CRUDListPageCore<ModelT> Init(string title, int take = 25, bool readOnly = false)
		{
			Title = title;
			Take = take;
            
            ListView.ListPanel.ContentView.ItemSelected += ItemSelectedHandler;
            ListView.ListPanel.ContentView.ItemAppearing += ItemAppearingHandler;

            if (!readOnly) ToolbarItems.Add(new ToolbarItem("New", NewBtnIconFilename, NewBtnClickedHandler));

            return this;
        }
        #endregion

        #region Event Handlers
        public abstract void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args);
        public virtual async void ItemSelectedHandler(object sender, SelectedItemChangedEventArgs args)
        {
            if (args.SelectedItem == null) return;
            var model = (ModelT)args.SelectedItem;
            await OpenDetailInternalAsync(model);
            ListView.ListPanel.ContentView.SelectedItem = null; //deselect row
        }
        public virtual async void DeleteItemHandler(object sender, EventArgs args)
        {
            bool connectionLost;
            var model = (ModelT)((MenuItem)sender).CommandParameter;

            do
            {
                connectionLost = false;
                var itemNotFound = false;
                try
                {
                    using(new ActivityIndicatorFor(ListView.ListPanel))
                    {
                        await DeleteItemInternalAsync(model);
                        Models.RemoveAll(x => x.Identity == model.Identity);
                    }
                }
                catch (SupermodelWebApiException ex)
                {
                    if (ex.StatusCode == HttpStatusCode.Unauthorized) UnauthorizedHandler();
                    else if (ex.StatusCode == HttpStatusCode.NotFound) itemNotFound = true;
                    else connectionLost = true;
                }
                catch (WebException) { connectionLost = true; }
                catch (Exception) { connectionLost = true; }
                if (connectionLost) await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
                if (itemNotFound)
                {
                    Models.RemoveAll(x => x.Identity == model.Identity);
                    await DisplayAlert("Not Found", "Item you are trying to delete no longer exists.", "Ok");
                }
            } 
            while (connectionLost);            
        }
        public virtual async void NewBtnClickedHandler()
        {
            var blankModel = new ModelT();
            await OpenDetailInternalAsync(blankModel);
        }
        #endregion

        #region IHaveActivityIndicator implementation
        public bool ActivityIndicatorOn
        {
            get { return ListView.ListPanel.ActivityIndicatorOn; }
            set { ListView.ListPanel.ActivityIndicatorOn = value; }
        }
        public string Message
        {
            get { return ListView.ListPanel.Message; }
            set { ListView.ListPanel.Message = value; }
        }
        #endregion

        #region Overrides
        protected abstract Task OpenDetailInternalAsync(ModelT model);
        protected abstract Task DeleteItemInternalAsync(ModelT model);
        protected abstract void UnauthorizedHandler();
        public virtual string NewBtnIconFilename { get { return null; } }
        #endregion

        #region Properties
        public CRUDListView<ModelT> ListView { get; set; }
        public ObservableCollection<ModelT> Models { get; set; }
        public int? Take { get; set; }

        protected bool LoadingInProgress { get; set; }
        protected bool LoadedAll { get; set; }
        #endregion
    }
}
