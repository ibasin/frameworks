﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDList
{
    using ViewModels;
    using Xamarin.Forms;    
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using System.Linq;
    using System;
    using System.Net;
    using Exceptions;
    using Views;
    using System.Collections.Generic;
    using Models;

    public abstract class EnhancedCRUDListPage<ModelT> : CRUDListPageCore<ModelT> where ModelT : class, ISupermodelListTemplate, IObjectWithIdentity, new()
    {
        #region Initializers
        public override CRUDListPageCore<ModelT> Init(string title, int take = 25, bool readOnly = false)
        {
            if (readOnly) Content = ListView = new CRUDListView<ModelT>(null, true);
            else Content = ListView = new CRUDListView<ModelT>(DeleteItemHandler, true);
            base.Init(title, take, readOnly);
            ListView.SearchBar.TextChanged += RunSearchHandler;
            ListView.ListPanel.ContentView.Refreshing += RefreshingHandler;
            return this;
        }
        #endregion

        #region Event Handlers
        public virtual async void RunSearchHandler(object sender, TextChangedEventArgs args)
        {
            var searchTerm = ListView.SearchBar.Text;
            await Task.Delay(750);
            if (searchTerm != ListView.SearchBar.Text) return; //we must still be typing, let's wait for it to finish

            await LoadListContentAsync(0, Take, searchTerm);
        }
        public override async void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args)
        {
            if (LoadedAll || LoadingInProgress) return;
            var searchTerm = ListView.SearchBar.Text;
            if (Models.Last() == args.Item) await LoadListContentAsync(Models.Count, Take, searchTerm);
        }
        public virtual async void RefreshingHandler(object sender, EventArgs args)
        {
            Models = null;
            await LoadListContentAsync(searchTerm: ListView.SearchBar.Text, showActivityIndicator: false);
            ListView.ListPanel.ContentView.IsRefreshing = false;
        }
        #endregion

        #region Overrides
        protected abstract Task<List<ModelT>> GetItemsInternalAsync(int skip, int? take, string searchTerm);
        #endregion

        #region Methods
        public virtual async Task LoadListContentAsync(int skip = 0, int? take = -1, string searchTerm = null, bool showActivityIndicator = true)
        {
            if (take < 0) take = Take;
            if (searchTerm == null) searchTerm = "";
            
            bool connectionLost;
            do
            {
                connectionLost = false;
                try
                {
                    LoadingInProgress = true;
                    LoadedAll = false;
                    
                    if (Models == null) Models = new ObservableCollection<ModelT>();
                    if (skip == 0) Models.Clear();
                    
                    if (showActivityIndicator)
                    {
                        using(new ActivityIndicatorFor(ListView.ListPanel))
                        {
                            var models = await GetItemsInternalAsync(skip, take, searchTerm);
                            if (take == null || models.Count < take) LoadedAll = true;

                            foreach (var model in models)
                            {
                                if (Models.All(x => x.Identity != model.Identity)) Models.Add(model);
                            }
                            ListView.ListPanel.ContentView.ItemsSource = Models;
                        }
                    }
                    else
                    {
                        var models = await GetItemsInternalAsync(skip, take, searchTerm);
                        if (take == null || models.Count < take) LoadedAll = true;

                        foreach (var model in models)
                        {
                            if (Models.All(x => x.Identity != model.Identity)) Models.Add(model);
                        }
                        ListView.ListPanel.ContentView.ItemsSource = Models;
                    }
                }
                catch (SupermodelWebApiException ex)
                {
                    if (ex.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        UnauthorizedHandler();
                        return;
                    }
                    connectionLost = true;
                }
                catch (WebException) { connectionLost = true; }
                catch (Exception) { connectionLost = true; }
                finally { LoadingInProgress = false; }
                if (connectionLost) await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
            } 
            while (connectionLost);
        }
        #endregion
    }
}
