﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Exceptions
{
    using System;
    
    public class SupermodelException : Exception
    {
        public SupermodelException() : this("N/A") { }
        public SupermodelException(string msg) : base(/*ReflectionHelper.GetThrowingContext() + ": " + */msg) { }
    }
}
