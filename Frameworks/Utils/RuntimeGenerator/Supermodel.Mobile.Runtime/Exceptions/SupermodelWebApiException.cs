﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Exceptions
{
    using System.Net;
    
    public class SupermodelWebApiException : SupermodelException
    {
        public SupermodelWebApiException(HttpStatusCode statusCode, string content) : base((int)statusCode + ":" + statusCode + ". Content: " + content)
        {
            StatusCode = statusCode;
            Content = content;
        }

		public HttpStatusCode StatusCode { get; set; }
		public string Content { get; set; }
    }
}
