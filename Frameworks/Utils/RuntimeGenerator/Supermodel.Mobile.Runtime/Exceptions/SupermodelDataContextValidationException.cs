﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Exceptions
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Newtonsoft.Json;
    using DataContext.Core;
    using ReflectionMapper;
    
    public class SupermodelDataContextValidationException : SupermodelException
    {
        #region Embedded Classes
        public class ValidationError : List<ValidationError.Error>
        {
            #region Constructors
            public ValidationError() { }
            public ValidationError(IEnumerable<ValidationResult> validationResults, PendingAction failedAction, string message)
            {
                foreach (var validationResult in validationResults)
                {
                    foreach (var memberName in validationResult.MemberNames)
                    {
                        var existingError = this.SingleOrDefault(x => x.Name == memberName);
                        if (existingError != null) existingError.ErrorMessages.Add(validationResult.ErrorMessage);
                        else Add(new Error (memberName, validationResult.ErrorMessage) );
                    }
                }
                FailedAction = failedAction;
                Message = message;
            }
            #endregion

            #region Embedded Classes
            public class Error
            {
                #region Constructors
                public Error(string name, List<string> errorMessages)
                {
                    Name = name;
                    ErrorMessages = errorMessages;
                }
                public Error(string name, string errorMessage)
                {
                    Name = name;
                    ErrorMessages = new List<string>{ errorMessage };
                }
                public Error()
                {
                    ErrorMessages = new List<string>();
                }
                #endregion

                #region Properties
                public string Name { get; set; }
                public List<string> ErrorMessages { get; set; }
                #endregion
            }
            #endregion

            #region Properties
            [JsonIgnore] public PendingAction FailedAction { get; set; }
            public string Message { get; set; }
            #endregion
        }
        #endregion

        #region Methods
        public List<ValidationResultList> GetListOfValidationResultLists()
        {
			var vrl = new List<ValidationResultList>();    
            foreach (var validationError in ValidationErrors)
			{
                var vr = new ValidationResultList();
                foreach (var error in validationError)
				{
				    if (error.Name == "id" || error.Name.StartsWith("apiModelItem.")) continue;
				    foreach (var errorMessage in error.ErrorMessages)
				    {
                        vr.Add(new ValidationResult(errorMessage, new[] { error.Name }));
				    }
				}
                vrl.Add(vr);
            }
            return vrl;
        }
        #endregion

        #region Constructors
        public SupermodelDataContextValidationException(ValidationError validationError)
        {
            ValidationErrors = new List<ValidationError>{validationError};
        }
        public SupermodelDataContextValidationException(List<ValidationError> validationErrors)
        {
            ValidationErrors = validationErrors;
        }
        #endregion

        #region Properties
        public List<ValidationError> ValidationErrors { get; set; }
        #endregion
    }
}
