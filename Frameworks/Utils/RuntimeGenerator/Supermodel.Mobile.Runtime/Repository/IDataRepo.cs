﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataContext.Core;
    using Models;
    
    public interface IDataRepo<ModelT> where ModelT : class, IModel, new()
    {
        #region Reads
        Task<ModelT> GetByIdAsync(long id);
        Task<ModelT> GetByIdOrDefaultAsync(long id);
        Task<List<ModelT>> GetAllAsync(int? skip = null, int? take = null);
        Task<long> GetCountAllAsync(int? skip = null, int? take = null);
        #endregion

        #region Batch Reads
        void DelayedGetById(out DelayedModel<ModelT> model, long id);
        void DelayedGetByIdOrDefault(out DelayedModel<ModelT> model, long id);
        void DelayedGetAll(out DelayedModels<ModelT> models);
        void DelayedGetCountAll(out DelayedCount count);
        #endregion

        #region Queries
        Task<List<ModelT>> GetWhereAsync(object searchBy, string sortBy = null, int? skip = null, int? take = null);
        Task<long> GetCountWhereAsync(object searchBy);
        #endregion

        #region Batch Queries
        void DelayedGetWhere(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null);
        void DelayedGetCountWhere(out DelayedCount count, object searchBy);
        #endregion

        #region Writes
        void Add(ModelT model);
        void Delete(ModelT model);
        void ForceUpdate(ModelT model);
        #endregion
    }
}
