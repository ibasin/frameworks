﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Repository
{
    using Models;

    public interface IRepoFactory
    {
        IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new();
    }
}
