﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    public interface IObjectWithIdentity
    {
        string Identity { get; }
        bool IsNew { get; }
    }
}