﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;
    using Repository;
    using DataContext.Core;
    using ReflectionMapper;
    using XForms.ViewModels;
    using System.ComponentModel;
    using Xamarin.Forms;
   
    public abstract class Model : IModel, ISupermodelListTemplate
    {
        #region Methods
        public virtual void Add()
        {
            CreateRepo().ExecuteMethod("Add", this);
        }
        public virtual void Delete()
        {
            CreateRepo().ExecuteMethod("Delete", this);
        }
        public virtual void Update()
        {
            CreateRepo().ExecuteMethod("ForceUpdate", this);
        }

        public virtual void BeforeSave(PendingAction.OperationEnum operation)
        {
            //default is doing nothing
        }

        public virtual object CreateRepo()
        {
            return RepoFactory.CreateForRuntimeType(GetType());
        }
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new ValidationResultList();
        }
        #endregion

        #region IObjectWithIdentity implemetation
        [JsonIgnore] public virtual string Identity => Id.ToString();
        [JsonIgnore] public virtual bool IsNew => Id == 0;
        #endregion

        #region ISupermodelListTemplate implemetation
        public virtual DataTemplate GetListCellDataTemplate(EventHandler deleteItemHandler)
        {
            var dataTemplate = new DataTemplate(() =>
            {
                var cell = ReturnACell();
                if (deleteItemHandler != null)
                {
                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
                    deleteAction.SetBinding(MenuItem.CommandParameterProperty, new Binding("."));
                    cell.ContextActions.Add(deleteAction);
                    deleteAction.Clicked += deleteItemHandler;
                }
                return cell;
            });
            SetUpBindings(dataTemplate);
            return dataTemplate;
        }
        public virtual Cell ReturnACell()
        {
            var msg = string.Format("In order to use '{0}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!", GetType().Name);
            throw new NotImplementedException(msg);
        }
        public virtual void SetUpBindings(DataTemplate dataTemplate)
        {
            var msg = string.Format("In order to use '{0}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!", GetType().Name);
            throw new NotImplementedException(msg);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Properties
        public long Id { get; set; }

        [NotRMapped] public DateTime? BroughtFromMasterDbOnUtc { get; set; }
        public bool ShouldSerializeBroughtFromMasterDbOnUtc() { return !SerializingForMasterDb; }
        public IModel PerpareForSerializingForMasterDb() { SerializingForMasterDb = true; return this; }
        public IModel PerpareForSerializingForLocalDb() { SerializingForMasterDb = false; return this; }
        [JsonIgnore] protected bool SerializingForMasterDb { get; set; }
        #endregion
    }
}
