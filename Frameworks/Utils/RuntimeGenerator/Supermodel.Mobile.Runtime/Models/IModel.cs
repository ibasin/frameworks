﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using DataContext.Core;

    public interface IModel : IObjectWithIdentity, IValidatableObject
    {
        long Id { get; set; }
        
        DateTime? BroughtFromMasterDbOnUtc { get; set; }
        
        IModel PerpareForSerializingForMasterDb();
        IModel PerpareForSerializingForLocalDb();

        void Add();
        void Delete();
        void Update();

        void BeforeSave(PendingAction.OperationEnum operation);
    }
}

