// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    public class TableSectionDefinition
    {
        public TableSectionDefinition() { }
        public TableSectionDefinition(string title, int screenOrderFrom, int screenOrderTo)
        {
            Title = title;
            ScreenOrderFrom = screenOrderFrom;
            ScreenOrderTo = screenOrderTo;
        }


        public string Title { get; set; }
        public int ScreenOrderFrom { get; set; }
        public int ScreenOrderTo { get; set; }
    }
}