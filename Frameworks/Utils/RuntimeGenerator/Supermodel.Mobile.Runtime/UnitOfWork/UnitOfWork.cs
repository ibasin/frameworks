﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.UnitOfWork
{
    using System;
    using DataContext;
    using DataContext.Core;
    using Exceptions;
    using Runtime.Async;

    public class UnitOfWork<ContextT> : IDisposable where ContextT : class, IDataContext, new()
    {
        #region Constructors
        public UnitOfWork(ReadOnly readOnly = ReadOnly.No)
        {
            Releaser = null;
            Context = new ContextT();
            if (readOnly == ReadOnly.Yes) Context.MakeReadOnly();
            UnitOfWorkContext<ContextT>.PushDbContext(Context);
        }
        #endregion

        #region IDisposable implemetation
        public virtual void Dispose()
        {
            Context.Dispose();

            var context = UnitOfWorkContext<ContextT>.PopDbContext();

            // ReSharper disable PossibleUnintendedReferenceComparison
            if (context != Context) throw new SupermodelException(string.Format("POP on Dispose popped mismatched Data Context."));
            // ReSharper restore PossibleUnintendedReferenceComparison

            if (Releaser != null) Releaser.Value.Dispose();
        }
        #endregion

        #region Properties
        public ContextT Context { get; private set; }
        public AsyncLock.Releaser? Releaser { get; set; }
        #endregion
    }
}
