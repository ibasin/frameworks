﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using System.Threading.Tasks;

    // Usings je Platform

    #if __IOS__
    using System.Drawing;
    using UIKit;
    using CoreGraphics;
    #endif


    #if __ANDROID__
    using Android.Graphics;
    #endif

    #if WINDOWS_UWP
    using Windows.Graphics.Imaging;
    using Windows.Storage.Streams;
    using System.Runtime.InteropServices.WindowsRuntime;
    #endif    
    
    #pragma warning disable 1998
    public static class ImageResizer
    {
        public static async Task<byte[]> ResizeImageAsync(byte[] imageData, float maxWidth, float maxHeight)
        {
            byte[] result = null;
            #if __IOS__
            result = await ResizeImageIOSAsync(imageData, maxWidth, maxHeight);
            #endif
            #if __ANDROID__
            result = await ResizeImageAndroidAsync(imageData, maxWidth, maxHeight);
            #endif
            #if WINDOWS_UWP
            result = await ResizeImageWinPhoneAsync(imageData, maxWidth, maxHeight);
            #endif
            // ReSharper disable once ExpressionIsAlwaysNull
            return result;
        }
        
        #if __IOS__
        public static async Task<byte[]> ResizeImageIOSAsync(byte[] imageData, float width, float height)
        {
            // Load the bitmap
            UIImage originalImage = ImageFromByteArray(imageData);
            //
            var Hoehe = originalImage.Size.Height;
            var Breite = originalImage.Size.Width;
            //
            nfloat ZielHoehe = 0;
            nfloat ZielBreite = 0;
            //

            if (Hoehe > Breite) // Höhe (71 für Avatar) ist Master
            {
                ZielHoehe = height;
                nfloat teiler = Hoehe / height;
                ZielBreite = Breite / teiler;
            }
            else // Breite (61 for Avatar) ist Master
            {
                ZielBreite = width;
                nfloat teiler = Breite / width;
                ZielHoehe = Hoehe / teiler;
            }
            //
            width = (float)ZielBreite;
            height = (float)ZielHoehe;
            //
            UIGraphics.BeginImageContext(new SizeF(width, height));
            originalImage.Draw(new RectangleF(0, 0, width, height));
            var resizedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            //
            var bytesImagen = resizedImage.AsJPEG().ToArray();
            resizedImage.Dispose();
            return bytesImagen;
        }
        public static UIKit.UIImage ImageFromByteArray(byte[] data)
        {
            if (data == null)
            {
                return null;
            }
            //
            UIKit.UIImage image;
            try
            {
                image = new UIKit.UIImage(Foundation.NSData.FromArray(data));
            }
            catch (Exception e)
            {
                Console.WriteLine("Image load failed: " + e.Message);
                return null;
            }
            return image;
        }
        #endif
        #if __ANDROID__
        public static async Task<byte[]> ResizeImageAndroidAsync(byte[] imageData, float width, float height)
        {
            // Load the bitmap 
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
            //
            float ZielHoehe = 0;
            float ZielBreite = 0;
            //
            var Hoehe = originalImage.Height;
            var Breite = originalImage.Width;
            //
            if (Hoehe > Breite) // Höhe (71 für Avatar) ist Master
            {
                ZielHoehe = height;
                float teiler = Hoehe / height;
                ZielBreite = Breite / teiler;
            }
            else // Breite (61 für Avatar) ist Master
            {
                ZielBreite = width;
                float teiler = Breite / width;
                ZielHoehe = Hoehe / teiler;
            }
            //
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)ZielBreite, (int)ZielHoehe, false);
            // 
            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
                return ms.ToArray();
            }
        }
        #endif
        #if WINDOWS_UWP
		public static async Task<byte[]> ResizeImageWinPhoneAsync(byte[] imageData, float width, float height)
		{
		    using (var streamIn = new MemoryStream(imageData))
		    {
                using (var imageStream = streamIn.AsRandomAccessStream())
                {
                    var decoder = await BitmapDecoder.CreateAsync(imageStream);
                            
                    var resizedStream = new InMemoryRandomAccessStream();
                    var encoder = await BitmapEncoder.CreateForTranscodingAsync(resizedStream, decoder);

		            double targetHeight, targetWidth;
		            if (decoder.PixelHeight > decoder.PixelWidth)
		            {
		                targetHeight = height;
		                double scaleRatio = decoder.PixelHeight / height;
		                targetWidth = decoder.PixelWidth / scaleRatio;
		            }
		            else 
		            {
		                targetWidth = width;
		                double scaleRatio = decoder.PixelWidth / width;
		                targetHeight = decoder.PixelHeight / scaleRatio;
		            }

                    encoder.BitmapTransform.InterpolationMode = BitmapInterpolationMode.Linear;
                    encoder.BitmapTransform.ScaledHeight = (uint)targetHeight;
                    encoder.BitmapTransform.ScaledWidth = (uint)targetWidth;

                    await encoder.FlushAsync();
                    resizedStream.Seek(0);
                    var resizedData = new byte[resizedStream.Size];
                    await resizedStream.ReadAsync(resizedData.AsBuffer(), (uint)resizedStream.Size, InputStreamOptions.None);
                    return resizedData;
                }
		    }
		}        
        #endif
    }
    #pragma warning restore 1998
}
