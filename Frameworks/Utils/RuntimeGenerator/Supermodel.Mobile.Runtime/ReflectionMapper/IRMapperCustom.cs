﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.ReflectionMapper
{
    using System;

    public interface IRMapperCustom
    {
        //must also have a default constructor!
        object MapFromObjectCustom(object obj, Type objType);
        object MapToObjectCustom(object obj, Type objType);    
    }
}
