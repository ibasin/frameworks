﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.ReflectionMapper
{
    using System.Reflection;
    
    public class ReflectionMapperPropertyMetadata
    {
        public PropertyInfo PropertyInfo { get; set; }
        public object Obj { get; set; }
    }
}
